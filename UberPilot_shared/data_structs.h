#ifndef DATA_STRUCTS_H_
#define DATA_STRUCTS_H_


// Type defs
#ifdef _C28_

typedef unsigned int 	uint16_t;
typedef int 			int16_t;
typedef unsigned long 	uint32_t;
typedef long 			int32_t;
typedef char 			int8_t; // warning, this is actually 16 bits on C28!!!
typedef unsigned char 	uint8_t; // warning, this is actually 16 bits on C28!!!

#elif defined _M3_ // M3 and C28 have different data types

typedef unsigned char		uint8_t;
typedef unsigned char 		BYTE;
typedef unsigned short int 	uint16_t;
typedef short int 			int16_t;
typedef unsigned int 		uint32_t;
typedef int 				int32_t;
#ifndef _OFFBOARDFILT_
typedef char 				int8_t; // 127 -> -128
#endif

#endif

//## communications defines
#define POS_SCALE	100000.0 // convert position float to int32_t
#define VEL_SCALE	1000000.0 // convert position float to int32_t
#define QUAT_SCALE	10000.0
#define ROT_SCALE	100.0

//## SDcard sector starting bytes
#define SD_XA	0xDE
#define SD_XB	0xAD
#define SD_XC	0xBE
#define SD_XD	0xEF

enum flightStates {
	NOT_FLYING,
	ATTITUDE_AND_THRUST,
	ATTITUDE_AND_Z,
	POSITION
};


//*********************************************************************
// UART Comm Structs
//*********************************************************************
#define PACKETID_SENSORCAL 0x01
#ifdef _C28_ // Only M3 supports packed attribute
typedef struct sSensorCalPacket {
#else
typedef struct __attribute__((packed)) sSensorCalPacket {
#endif
	int16_t gyroXScale;
	int16_t gyroYScale;
	int16_t gyroZScale;
	int16_t accelXScale;
	int16_t accelYScale;
	int16_t accelZScale;
	int16_t accelXZero;
	int16_t accelYZero;
	int16_t accelZZero;
	int16_t magXZero;
	int16_t magYZero;
	int16_t magZZero;
	int16_t magComp00;
	int16_t magComp01;
	int16_t magComp02;
	int16_t magComp10;
	int16_t magComp11;
	int16_t magComp12;
	int16_t magComp20;
	int16_t magComp21;
	int16_t magComp22;
	int16_t kAttFilter;
	int16_t kGyroBias;
} tSensorCalPacket;

#define PACKETID_AHRS 0x02
#ifdef _C28_ // Only M3 supports packed attribute
typedef struct sAHRSpacket {
#else
typedef struct __attribute__((packed)) sAHRSpacket {
#endif
	int16_t p;
	int16_t q;
	int16_t r;
	int16_t qo_est;
	int16_t qx_est;
	int16_t qy_est;
	int16_t qz_est;
	int16_t qo_meas;
	int16_t qx_meas;
	int16_t qy_meas;
	int16_t qz_meas;
} tAHRSpacket;

#define PACKETID_ATT_CMD 0x03
#ifdef _C28_ // Only M3 supports packed attribute
typedef struct sAttCmdPacket {
#else
typedef struct __attribute__((packed)) sAttCmdPacket {
#endif
	int16_t p_cmd;
	int16_t q_cmd;
	int16_t r_cmd;
	int16_t qo_cmd;
	int16_t qx_cmd;
	int16_t qy_cmd;
	int16_t qz_cmd;
	int16_t qo_meas;
	int16_t qx_meas;
	int16_t qy_meas;
	int16_t qz_meas;
	int16_t throttle;
	uint16_t AttCmd;
} tAttCmdPacket;

#define PACKETID_GAINS 0x04
#ifdef _C28_ // Only M3 supports packed attribute
typedef struct sGainsPacket {
#else
typedef struct __attribute__((packed)) sGainsPacket {
#endif
	int16_t Kp_roll;
	int16_t Kd_roll;
	int16_t Ki_roll;
	int16_t Kp_pitch;
	int16_t Kd_pitch;
	int16_t Ki_pitch;
	int16_t Kp_yaw;
	int16_t Ki_yaw;
	int16_t Kd_yaw;
	int16_t Servo1_trim;
	int16_t Servo2_trim;
	int16_t Servo3_trim;
	int16_t Servo4_trim;
	int16_t maxang;
	int16_t motorFF;
	uint16_t lowBatt;
	uint16_t stream_data;
} tGainsPacket;

#define PACKETID_VOLTAGE 0x05
#ifdef _C28_ // Only M3 supports packed attribute
typedef struct sVoltagePacket {
#else
typedef struct __attribute__((packed)) sVoltagePacket {
#endif
	int16_t voltage;
} tVoltagePacket;

#define PACKETID_LOG 0x06
#ifdef _C28_ // Only M3 supports packed attribute
typedef struct sLogPacket {
#else
typedef struct __attribute__((packed)) sLogPacket {
#endif
	int16_t p_est;
	int16_t q_est;
	int16_t r_est;
	int16_t qo_est;
	int16_t qx_est;
	int16_t qy_est;
	int16_t qz_est;
	int16_t p_cmd;
	int16_t q_cmd;
	int16_t r_cmd;
	int16_t qo_cmd;
	int16_t qx_cmd;
	int16_t qy_cmd;
	int16_t qz_cmd;
	uint16_t throttle;
	int16_t collective;
} tLogPacket;

#define PACKETID_HEALTH 0x07
#ifdef _C28_ // Only M3 supports packed attribute
typedef struct sHealthPacket {
#else
typedef struct __attribute__((packed)) sHealthPacket {
#endif
	int8_t current1;
	int8_t temp1;
	int8_t current2;
	int8_t temp2;
	int8_t current3;
	int8_t temp3;
	int8_t current4;
	int8_t temp4;
} tHealthPacket;

#define PACKETID_SENSORS 0x08
#ifdef _C28_ // Only M3 supports packed attribute
typedef struct sSensorsPacket {
#else
typedef struct __attribute__((packed)) sSensorsPacket {
#endif
	int16_t gyroX;
	int16_t gyroY;
	int16_t gyroZ;
	int16_t accelX;
	int16_t accelY;
	int16_t accelZ;
	int16_t magX;
	int16_t magY;
	int16_t magZ;
	int16_t sonarZ;
	int32_t pressure;
} tSensorsPacket;

#define PACKETID_ALTITUDE 0x09
#ifdef _C28_ // Only M3 supports packed attribute
typedef struct sAltitudePacket {
#else
typedef struct __attribute__((packed)) sAltitudePacket {
#endif
	int16_t sonarZ;
	int32_t pressure;
	int32_t temperature;
	int16_t accelX;
	int16_t accelY;
	int16_t accelZ;
} tAltitudePacket;

#define PACKETID_STATE 0x0A
#ifdef _C28_ // Only M3 supports packed attribute
typedef struct sStatePacket {
#else
typedef struct __attribute__((packed)) sStatePacket {
#endif
	int16_t x;
	int16_t y;
	int16_t z;
	int16_t dx;
	int16_t dy;
	int16_t dz;
	int16_t ddx;
	int16_t ddy;
	int16_t ddz;
	int16_t ddx_bias;
	int16_t ddy_bias;
	int16_t ddz_bias;
	int16_t qw;
	int16_t qx;
	int16_t qy;
	int16_t qz;
	int16_t p;
	int16_t q;
	int16_t r;
	int16_t x_cmd;
	int16_t y_cmd;
	int16_t z_cmd;
	int16_t dx_cmd;
	int16_t dy_cmd;
	int16_t dz_cmd;
	int16_t qw_cmd;
	int16_t qx_cmd;
	int16_t qy_cmd;
	int16_t qz_cmd;
	int16_t p_cmd;
	int16_t q_cmd;
	int16_t r_cmd;
	int16_t magx;
	int16_t magy;
	int16_t magz;
	uint16_t dt;
} tStatePacket;

#define PACKETID_POSVEL 0x0B
#ifdef _C28_ // Only M3 supports packed attribute
typedef struct sPosVelPacket {
#else
typedef struct __attribute__((packed)) sPosVelPacket {
#endif
	int32_t x;
	int32_t y;
	int32_t z;
	int32_t dx;
	int32_t dy;
	int32_t dz;
} tPosVelPacket;

#define PACKETID_WII 0x0C
#ifdef _C28_ // Only M3 supports packed attribute
typedef struct sWiiPacket {
#else
typedef struct __attribute__((packed)) sWiiPacket {
#endif
    uint16_t x1;
    uint16_t y1;
    uint8_t size1;
    uint16_t x2;
    uint16_t y2;
    uint8_t size2;
    uint16_t x3;
    uint16_t y3;
    uint8_t size3;
    uint16_t x4;
    uint16_t y4;
    uint8_t size4;
    uint8_t blobcount;
}
tWiiPacket;

#define PACKETID_ATT_Z_CMD 0x0D
#ifdef _C28_ // Only M3 supports packed attribute
typedef struct sAttZCmdPacket {
#else
typedef struct __attribute__((packed)) sAttZCmdPacket {
#endif
	int16_t p_cmd;
	int16_t q_cmd;
	int16_t r_cmd;
	int16_t qo_cmd;
	int16_t qx_cmd;
	int16_t qy_cmd;
	int16_t qz_cmd;
	int16_t z;
	uint16_t AttCmd;
} tAttZCmdPacket;

#define PACKETID_POS 0x0E
#ifdef _C28_ // Only M3 supports packed attribute
typedef struct sPosPacket {
#else
typedef struct __attribute__((packed)) sPosPacket {
#endif
	int32_t x;
	int32_t y;
	int32_t z;
} tPosPacket;


#define PACKETID_POS_YAW_CMD 0x0F
#ifdef _C28_ // Only M3 supports packed attribute
typedef struct sPosYawCmdPacket {
#else
typedef struct __attribute__((packed)) sPosYawCmdPacket {
#endif
	int16_t x_cmd;
	int16_t y_cmd;
	int16_t z_cmd;
	int16_t dx_cmd;
	int16_t dy_cmd;
	int16_t dz_cmd;
	int16_t yaw;
	uint16_t AttCmd;
} tPosYawCmdPacket;

#define PACKETID_VICON_STATE 0x10
#ifdef _C28_ // Only M3 supports packed attribute
typedef struct sViconStatePacket {
#else
typedef struct __attribute__((packed)) sViconStatePacket {
#endif
	int16_t x;
	int16_t y;
	int16_t z;
	int16_t dx;
	int16_t dy;
	int16_t dz;
	int16_t qw;
	int16_t qx;
	int16_t qy;
	int16_t qz;
} tViconStatePacket;

//*********************************************************************
// End UART Comm Structs
//*********************************************************************

//*********************************************************************
// IPC Comm Structs
//*********************************************************************

#define PACKETID_IMU 0x30
#ifdef _C28_ // Only M3 supports packed attribute
typedef struct sIMUPacket {
#else
typedef struct __attribute__((packed)) sIMUPacket {
#endif
	int16_t gyroX;
	int16_t gyroY;
	int16_t gyroZ;
	int16_t accX;
	int16_t accY;
	int16_t accZ;
} tIMUPacket;

#define PACKETID_ALT 0x31
#ifdef _C28_ // Only M3 supports packed attribute
typedef struct sAltPacket {
#else
typedef struct __attribute__((packed)) sAltPacket {
#endif
	int32_t z;
} tAltPacket;

#define PACKETID_WII_IPC 0x32
#ifdef _C28_ // Only M3 supports packed attribute
typedef struct sWiiIPCPacket {
#else
typedef struct __attribute__((packed)) sWiiIPCPacket {
#endif
	int16_t x[3];
    int16_t y[3];
} tWiiIPCPacket;

#define PACKETID_MAG 0x33
#ifdef _C28_ // Only M3 supports packet attribute
  typedef struct sMagPacket {
#else
    typedef struct __attribute__((packed)) sMagPacket {
#endif
      int16_t magx;
      int16_t magy;
      int16_t magz;
    } tMagPacket;


#define PACKETID_SONAR 0xFA // arbitrarily chosen
//*********************************************************************
// End IPC Comm Structs
//*********************************************************************

// Structs
typedef struct sBlob
{
	int16_t X;
	int16_t Y;
	int16_t Size;
}tBlob;


typedef struct sSensorData {

	int16_t gyroX;
	int16_t gyroY;
	int16_t gyroZ;

	int16_t accX;
	int16_t accY;
	int16_t accZ;

	int16_t magX;
	int16_t magY;
	int16_t magZ;

	int16_t sonarZ;

    int32_t pressure;
    int32_t temperature;

    //filter buffers for pressure and temperature -- should be multiples of four
    int32_t temperature_buff[4];
    uint8_t temperature_index;
    int32_t temperature_filt;
    int32_t temperature_buff_sum;
    int32_t pressure_buff[128];
    uint8_t pressure_index;
    int32_t pressure_filt;
    int32_t pressure_buff_sum;

    tBlob blob1;
    tBlob blob2;
    tBlob blob3;
    tBlob blob4;
    uint8_t blobcount;

//    int8_t current1;
//    int8_t maxPWM1;
//    int8_t temp1;
//
//    int8_t current2;
//    int8_t maxPWM2;
//    int8_t temp2;
//
//    int8_t current3;
//    int8_t maxPWM3;
//    int8_t temp3;
//
//    int8_t current4;
//    int8_t maxPWM4;
//    int8_t temp4;

} tSensorData;

typedef struct sM3SensorCal{

    int axBias;
    int ayBias;
    int azBias;
    int calibration_cnts;
    int blankReads;

    // see data sheet for BMP180;
    int16_t AC1;
    int16_t AC2;
    int16_t AC3;
    uint16_t AC4;
    uint16_t AC5;
    uint16_t AC6;
    int16_t B1;
    int16_t B2;
    int16_t MB;
    int16_t MC;
    int16_t MD; // end calibration data

    int32_t B3;
    uint32_t B4;
    int32_t B5;
    int32_t B6;
    uint32_t B7;
    int32_t X1;
    int32_t X2;
    int32_t X3;
    int32_t UT;
    int32_t UP;
    uint8_t oss;

    uint8_t pressureConversionTime;
    uint8_t temperatureConversionTime;
    uint8_t conversionTime;

}tM3SensorCal;

#endif /*DATA_STRUCTS_H_*/
