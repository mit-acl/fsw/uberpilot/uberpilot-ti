/*
 * wii.c
 *
 *  Created on: Jan 16, 2013
 *      Author: mark
 */


#include "wii.h"

extern volatile tWiiIPCPacket WiiPacket;
extern volatile tPosPacket PosPacket;

// coefficients found using the "plotDataLog.m" matlab function (fit is in log-log space)
float aWii = -0.002;
float bWii = -0.9083;
float cWii = 4.7739;

void processWiiData(void)
{
	tPix xpix[3];
	tPix ypix[3];
	int i=0;
	for (i=0;i<3;i++){
		xpix[i].x = WiiPacket.x[i];
		xpix[i].y = WiiPacket.y[i];
		ypix[i].x = WiiPacket.x[i];
		ypix[i].y = WiiPacket.y[i];
	}

	// sort x pixel values
	qsort (xpix, 3, sizeof(tPix), comparex);
	qsort (ypix, 3, sizeof(tPix), comparey);

	// TODO: account for roll and pitch here
	// calculate d1 and d2
	float d1 = fabs(xpix[1].x - xpix[0].x);
	float d2 = fabs(xpix[2].x - xpix[1].x);

	float bearing_est = -atan((d1/(d1+d2) - cos(PI/3))/sin(PI/3));

	int short_light = 0; //short
	int middle_light = 0;// middle
	int tall_light = 0;// tall
	while (ypix[0].x != xpix[short_light].x){
		short_light++;
	}
	while (ypix[1].x != xpix[middle_light].x){
		middle_light++;
	}
	while (ypix[2].x != xpix[tall_light].x){
		tall_light++;
	}

	if (short_light == 0 && middle_light == 1 && tall_light == 2){  // 123 order
		//bearing_est += 0;
	} else if (short_light == 0 && middle_light == 2 && tall_light == 1){  // 132 order
		bearing_est = -bearing_est + PI/3;
	} else if (short_light == 1 && middle_light == 2 && tall_light == 0){  // 312 order
		bearing_est += 2*PI/3;
	} else if (short_light == 2 && middle_light == 1 && tall_light == 0){  // 321 order
		bearing_est = -bearing_est + PI;
	} else if (short_light == 2 && middle_light == 0 && tall_light == 1){  // 231 order
		bearing_est += 4*PI/3;
	} else if (short_light == 1 && middle_light == 0 && tall_light == 2){  // 213 order
		bearing_est = -bearing_est + 5*PI/3;
	}

	wrap(&bearing_est);

	float disparity = (d1+d2);///cos(bearing_est);

	float range_est = aWii*log(disparity)*log(disparity) + bWii*log(disparity) + cWii;
	range_est = exp(range_est);

	PosPacket.x = (int32_t) (range_est*cos(bearing_est)*POS_SCALE);
	PosPacket.y = (int32_t) (range_est*sin(bearing_est)*POS_SCALE);

}


int comparex (const void * a, const void * b)
{
	tPix* x1 = (tPix*)a;
	tPix* x2 = (tPix*)b;
	return ( x1->x - x2->x );
}

// sort other direction
int comparey (const void * a, const void * b)
{
	tPix* y1 = (tPix*)a;
	tPix* y2 = (tPix*)b;
	return ( y2->y - y1->y );
}

//## Enforces +pi <--> -pi
void wrap(float* ang)
{
    while( *ang > PI )
        *ang -= 2*PI;
    while( *ang <= -PI )
        *ang += 2*PI;
}
