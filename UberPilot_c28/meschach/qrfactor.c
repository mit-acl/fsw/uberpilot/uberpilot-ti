/**************************************************************************
 **
 ** Copyright (C) 1993 David E. Steward & Zbigniew Leyk, all rights reserved.
 **
 **			     Meschach Library
 **
 ** This Meschach Library is provided "as is" without any express
 ** or implied warranty of any kind with respect to this software.
 ** In particular the authors shall not be liable for any direct,
 ** indirect, special, incidental or consequential damages arising
 ** in any way from use of the software.
 **
 ** Everyone is granted permission to copy, modify and redistribute this
 ** Meschach Library, provided:
 **  1.  All copies contain this copyright notice.
 **  2.  All modified copies shall carry a notice stating who
 **      made the last modification and the date of such modification.
 **  3.  No charge is made for this software or works derived from it.
 **      This clause shall not be construed as constraining other software
 **      distributed on the same medium as this software, nor is a
 **      distribution fee considered a charge.
 **
 ***************************************************************************/

/*
 This file contains the routines needed to perform QR factorisation
 of matrices, as well as Householder transformations.
 The internal "factored form" of a matrix A is not quite standard.
 The diagonal of A is replaced by the diagonal of R -- not by the 1st non-zero
 entries of the Householder vectors. The 1st non-zero entries are held in
 the diag parameter of QRfactor(). The reason for this non-standard
 representation is that it enables direct use of the Usolve() function
 rather than requiring that  a seperate function be written just for this case.
 See, e.g., QRsolve() below for more details.

 */

//static char rcsid[] = "$Id: qrfactor.c,v 1.5 1994/01/13 05:35:07 des Exp $";

#include	<stdio.h>
#include	<math.h>
#include        "matrix2.h"

#define		sign(x)	((x) > 0.0 ? 1 : ((x) < 0.0 ? -1 : 0 ))

/* Note: The usual representation of a Householder transformation is taken
 to be:
 P = I - beta.u.uT
 where beta = 2/(uT.u) and u is called the Householder vector
 */

/* QRfactor -- forms the QR factorisation of A -- factorisation stored in
 compact form as described above ( not quite standard format ) */
#ifndef ANSI_C
MAT *QRfactor(A,diag)
MAT *A;
VEC *diag;
#else
MAT *QRfactor(MAT *A, VEC *diag)
#endif
{
	unsigned int k, limit;
	Real beta;
	STATIC VEC *hh = VNULL, *w = VNULL;

//	if (!A || !diag)
//		error(E_NULL, "QRfactor");
	limit = min(A->m,A->n);
//	if (diag->dim < limit)
//		error(E_SIZES, "QRfactor");

	hh = v_resize(hh, A->m);
	w = v_resize(w, A->n);
	//MEM_STAT_REG(hh, TYPE_VEC);
	//MEM_STAT_REG(w, TYPE_VEC);

	for (k = 0; k < limit; k++) {
		/* get H/holder vector for the k-th column */
		get_col(A, k, hh);
		/* hhvec(hh,k,&beta->ve[k],hh,&A->me[k][k]); */
		hhvec(hh, k, &beta, hh, &A->me[k][k]);
		diag->ve[k] = hh->ve[k];

		/* apply H/holder vector to remaining columns */
		/* hhtrcols(A,k,k+1,hh,beta->ve[k]); */
		_hhtrcols(A, k, k + 1, hh, beta, w);
	}

#ifdef	THREADSAFE
	V_FREE(hh); V_FREE(w);
#endif

	return (A);
}

/* Qsolve -- solves Qx = b, Q is an orthogonal matrix stored in compact
 form a la QRfactor() -- may be in-situ */
#ifndef ANSI_C
VEC *_Qsolve(QR,diag,b,x,tmp)
MAT *QR;
VEC *diag, *b, *x, *tmp;
#else
VEC *_Qsolve(const MAT *QR, const VEC *diag, const VEC *b, VEC *x, VEC *tmp)
#endif
{
	unsigned int dynamic;
	int k, limit;
	Real beta, r_ii, tmp_val;

	limit = min(QR->m,QR->n);
	dynamic = FALSE;
//	if (!QR || !diag || !b)
//		error(E_NULL, "_Qsolve");
//	if (diag->dim < limit || b->dim != QR->m)
//		error(E_SIZES, "_Qsolve");
	x = v_resize(x, QR->m);
	if (tmp == VNULL )
		dynamic = TRUE;
	tmp = v_resize(tmp, QR->m);

	/* apply H/holder transforms in normal order */
	x = v_copy(b,x);
	for (k = 0; k < limit; k++) {
		get_col(QR, k, tmp);
		r_ii = fabs(tmp->ve[k]);
		tmp->ve[k] = diag->ve[k];
		tmp_val = (r_ii * fabs(diag->ve[k]));
		beta = (tmp_val == 0.0) ? 0.0 : 1.0 / tmp_val;
		/* hhtrvec(tmp,beta->ve[k],k,x,x); */
		hhtrvec(tmp, beta, k, x, x);
	}

	if (dynamic)
		V_FREE(tmp);

	return (x);
}


/* QRsolve -- solves the system Q.R.x=b where Q & R are stored in compact form
 -- returns x, which is created if necessary */
#ifndef ANSI_C
VEC *QRsolve(QR,diag,b,x)
MAT *QR;
VEC *diag /* , *beta */, *b, *x;
#else
VEC *QRsolve(const MAT *QR, const VEC *diag, const VEC *b, VEC *x)
#endif
{
	int limit;
	STATIC VEC *tmp = VNULL;

//	if (!QR || !diag || !b)
//		error(E_NULL, "QRsolve");
	limit = min(QR->m,QR->n);
//	if (diag->dim < limit || b->dim != QR->m)
//		error(E_SIZES, "QRsolve");
	tmp = v_resize(tmp, limit);
	//MEM_STAT_REG(tmp, TYPE_VEC);

	x = v_resize(x, QR->n);
	_Qsolve(QR, diag, b, x, tmp);
	x = Usolve(QR, x, x, 0.0);
	v_resize(x, QR->n);

#ifdef	THREADSAFE
	V_FREE(tmp);
#endif

	return x;
}






