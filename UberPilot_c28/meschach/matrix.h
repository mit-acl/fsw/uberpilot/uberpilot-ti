/**************************************************************************
 **
 ** Copyright (C) 1993 David E. Steward & Zbigniew Leyk, all rights reserved.
 **
 **			     Meschach Library
 **
 ** This Meschach Library is provided "as is" without any express
 ** or implied warranty of any kind with respect to this software.
 ** In particular the authors shall not be liable for any direct,
 ** indirect, special, incidental or consequential damages arising
 ** in any way from use of the software.
 **
 ** Everyone is granted permission to copy, modify and redistribute this
 ** Meschach Library, provided:
 **  1.  All copies contain this copyright notice.
 **  2.  All modified copies shall carry a notice stating who
 **      made the last modification and the date of such modification.
 **  3.  No charge is made for this software or works derived from it.
 **      This clause shall not be construed as constraining other software
 **      distributed on the same medium as this software, nor is a
 **      distribution fee considered a charge.
 **
 ***************************************************************************/

/*
 Type definitions for general purpose maths package
 */

#ifndef	MATRIXH

/* RCS id: $Id: matrix.h,v 1.18 1994/04/16 00:33:37 des Exp $ */

#define	MATRIXH	

#include	"machine.h"
//#include        "err.h"
#include 	"meminfo.h"

/* unsigned integer type */
/************************************************************
 #ifndef U_INT_DEF
 typedef	unsigned int	u_int;
 #define U_INT_DEF
 #endif
 ************************************************************/

/* vector definition */
typedef struct {
	unsigned int dim, max_dim;
	Real *ve;
} VEC;

/* matrix definition */
typedef struct {
	unsigned int m, n;
	unsigned int max_m, max_n, max_size;
	Real **me, *base; /* base is base of alloc'd mem */
} MAT;


//#ifndef MALLOCDECL
//#ifndef ANSI_C
//extern char *malloc(), *calloc(), *realloc();
//#else
//extern void *malloc( size_t), *calloc( size_t, size_t),
//		*realloc(void *, size_t);
//#endif
//#endif /* MALLOCDECL */


#ifdef THREADSAFE
#define	STATIC
#else
#define	STATIC	static
#endif /* THREADSAFE */

#ifndef ANSI_C
extern void m_version();
#else
void m_version(void);
#endif

#ifndef ANSI_C


#else
/* allocate one object of given type */
#define	NEW(type)	((type *)calloc((size_t)1,(size_t)sizeof(type)))

/* allocate num objects of given type */
#define	NEW_A(num,type)	((type *)calloc((size_t)(num),(size_t)sizeof(type)))

/* re-allocate arry to have num objects of the given type */
#define	RENEW(var,num,type) \
    ((var)=(type *)((var) ? \
		    realloc((char *)(var),(size_t)((num)*sizeof(type))) : \
		    calloc((size_t)(num),(size_t)sizeof(type))))

#define	MEMCOPY(from,to,n_items,type) \
 MEM_COPY((char *)(from),(char *)(to),(unsigned)(n_items)*sizeof(type))

#endif /* ANSI_C */

/* type independent min and max operations */
#ifndef max
#define	max(a,b)	((a) > (b) ? (a) : (b))
#endif /* max */
#ifndef min
#define	min(a,b)	((a) > (b) ? (b) : (a))
#endif /* min */

#undef TRUE
#define	TRUE	1
#undef FALSE
#define	FALSE	0

/* for input routines */
#define MAXLINE 81

/* Dynamic memory allocation */

/* Should use M_FREE/V_FREE/PX_FREE in programs instead of m/v/px_free()
 as this is considerably safer -- also provides a simple type check ! */

#ifndef ANSI_C

extern VEC *v_get(), *v_resize();
extern MAT *m_get(), *m_resize();
extern int m_free(),v_free();

#else

/* get/resize vector to given dimension */
extern VEC *v_get(int), *v_resize(VEC *, int);
/* get/resize matrix to be m x n */
extern MAT *m_get(int, int), *m_resize(MAT *, int, int);

/* free (de-allocate) (band) matrices, vectors, permutations and
 integer vectors */
extern m_free(MAT *), v_free(VEC *);

#endif /* ANSI_C */

/* MACROS */

/* macros that also check types and sets pointers to NULL */
#define	M_FREE(mat)	( m_free(mat),	(mat)=(MAT *)NULL )
#define V_FREE(vec)	( v_free(vec),	(vec)=(VEC *)NULL )

#define MAXDIM  	10000001

/* Entry level access to data structures */
/* routines to check indexes */
#define	m_chk_idx(A,i,j)	((i)>=0 && (i)<(A)->m && (j)>=0 && (j)<=(A)->n)
#define	v_chk_idx(x,i)		((i)>=0 && (i)<(x)->dim)

#define	m_entry(A,i,j)		m_get_val(A,i,j)
#define	v_entry(x,i)		v_get_val(x,i)
#define	bd_entry(A,i,j)		bd_get_val(A,i,j)
#ifdef DEBUG

#else /* no DEBUG */
#define	m_set_val(A,i,j,val)	((A)->me[(i)][(j)] = (val))
#define	m_add_val(A,i,j,val)	((A)->me[(i)][(j)] += (val))
#define	m_sub_val(A,i,j,val)	((A)->me[(i)][(j)] -= (val))
#define	m_get_val(A,i,j)	((A)->me[(i)][(j)])
#define	v_set_val(x,i,val)	((x)->ve[(i)] = (val))
#define	v_add_val(x,i,val)	((x)->ve[(i)] += (val))
#define	v_sub_val(x,i,val)	((x)->ve[(i)] -= (val))
#define	v_get_val(x,i)		((x)->ve[(i)])
#endif /* DEBUG */

/* MACROS */

/* macros to use stdout and stdin instead of explicit fp */
#define	v_output(vec)	v_foutput(stdout,vec)
#define	v_input(vec)	v_finput(stdin,vec)
#define	m_output(mat)	m_foutput(stdout,mat)
#define	m_input(mat)	m_finput(stdin,mat)

/* special purpose access routines */

/* Copying routines */
#ifndef ANSI_C
extern MAT *_m_copy(), *m_move(), *vm_move();
extern VEC *_v_copy(), *v_move(), *mv_move();
extern PERM *px_copy();
extern IVEC *iv_copy(), *iv_move();
extern BAND *bd_copy();

#else

/* copy in to out starting at out[i0][j0] */
extern MAT *_m_copy(const MAT *in, MAT *out, unsigned int i0, unsigned int j0),
		* m_move(const MAT *in, int, int, int, int, MAT *out, int, int),
		*vm_move(const VEC *in, int, MAT *out, int, int, int, int);
/* copy in to out starting at out[i0] */
extern VEC *_v_copy(const VEC *in, VEC *out, unsigned int i0), * v_move(
		const VEC *in, int, int, VEC *out, int), *mv_move(const MAT *in, int,
		int, int, int, VEC *out, int);

#endif /* ANSI_C */

/* MACROS */
#define	m_copy(in,out)	_m_copy(in,out,0,0)
#define	v_copy(in,out)	_v_copy(in,out,0)

/* Initialisation routines -- to be zero, ones, random or identity */
#ifndef ANSI_C

#else
extern VEC *v_zero(VEC *), *v_rand(VEC *), *v_ones(VEC *);
extern MAT *m_zero(MAT *), *m_ident(MAT *), *m_rand(MAT *), *m_ones(MAT *);
#endif /* ANSI_C */

/* Basic vector operations */
#ifndef ANSI_C

#else

extern VEC *sv_mlt(double s, const VEC *x, VEC *out), /* out <- s.x */
*mv_mlt(const MAT *A, const VEC *s, VEC *out), /* out <- A.x */
*vm_mlt(const MAT *A, const VEC *x, VEC *out), /* out^T <- x^T.A */
*v_add(const VEC *x, const VEC *y, VEC *out), /* out <- x + y */
*v_sub(const VEC *x, const VEC *y, VEC *out), /* out <- x - y */
*v_mltadd(const VEC *x, const VEC *y, double s, VEC *out),

/* out <- s1.x1 + s2.x2 + ... */


/* returns sum_i x[i] */
v_sum(const VEC *);



/* returns inner product starting at component i0 */
extern double _in_prod(const VEC *x, const VEC *y, unsigned int i0),
/* returns sum_{i=0}^{len-1} x[i].y[i] */
__ip__(const Real *, const Real *, int);

/* see v_mltadd(), v_add(), v_sub() and v_zero() */
extern void __mltadd__(Real *, const Real *, double, int), __add__(const Real *,
		const Real *, Real *, int), __sub__(const Real *, const Real *, Real *,
		int), __smlt__(const Real *, double, Real *, int),
		__zero__(Real *, int);

#endif /* ANSI_C */

/* MACRO */
/* usual way of computing the inner product */
#define	in_prod(a,b)	_in_prod(a,b,0)

/* Norms */
/* scaled vector norms -- scale == NULL implies unscaled */
#ifndef ANSI_C


#else
/* returns sum_i |x[i]/scale[i]| */
extern double
/* returns (scaled) Euclidean norm */
_v_norm2(const VEC *x, const VEC *scale);


#endif /* ANSI_C */

/* MACROS */
/* unscaled vector norms */
#define	v_norm2(x)	_v_norm2(x,VNULL)

/* Basic matrix operations */
#ifndef ANSI_C



#else

extern MAT *sm_mlt(double s, const MAT *A, MAT *out), /* out <- s.A */
*m_mlt(const MAT *A, const MAT *B, MAT *out), /* out <- A.B */
*mmtr_mlt(const MAT *A, const MAT *B, MAT *out), /* out <- A.B^T */
*mtrm_mlt(const MAT *A, const MAT *B, MAT *out), /* out <- A^T.B */
*m_add(const MAT *A, const MAT *B, MAT *out), /* out <- A + B */
*m_sub(const MAT *A, const MAT *B, MAT *out), /* out <- A - B */
*sub_mat(const MAT *A, unsigned int, unsigned int, unsigned int, unsigned int,
		MAT *out), *m_transp(const MAT *A, MAT *out), /* out <- A^T */
/* out <- A + s.B */
*ms_mltadd(const MAT *A, const MAT *B, double s, MAT *out);


extern MAT
*_set_col(MAT *A, unsigned int i, const VEC *col, unsigned int j0),
/* A[i][j] <- out[i], i >= i0 */
*_set_row(MAT *A, unsigned int j, const VEC *row, unsigned int i0);

extern VEC *get_row(const MAT *, unsigned int, VEC *), *get_col(const MAT *,
		unsigned int, VEC *), *sub_vec(const VEC *, int, int, VEC *),
/* mv_mltadd: out <- x + s.A.y */
*mv_mltadd(const VEC *x, const VEC *y, const MAT *A, double s, VEC *out),
/* vm_mltadd: out^T <- x^T + s.y^T.A */
*vm_mltadd(const VEC *x, const VEC *y, const MAT *A, double s, VEC *out);
#endif /* ANSI_C */

/* MACROS */
/* row i of A <- vec */
#define	set_row(mat,row,vec)	_set_row(mat,row,vec,0) 
/* col j of A <- vec */
#define	set_col(mat,col,vec)	_set_col(mat,col,vec,0)


/* miscellaneous functions */

#ifndef ANSI_C

extern double square(), cube(), mrand();
extern void smrand(), mrandlist();
extern void m_dump(), px_dump(), v_dump(), iv_dump();
extern MAT *band2mat();
extern BAND *mat2band();

#else

double square(double x), /* returns x^2 */
cube(double x), /* returns x^3 */
mrand(void); /* returns random # in [0,1) */

void smrand(int seed), /* seeds mrand() */
mrandlist(Real *x, int len); /* generates len random numbers */

#endif /* ANSI_C */

/* miscellaneous constants */
#define	VNULL	((VEC *)NULL)
#define	MNULL	((MAT *)NULL)
#define	PNULL	((PERM *)NULL)
#define	IVNULL	((IVEC *)NULL)
#define BDNULL  ((BAND *)NULL)

/* varying number of arguments */

#ifdef ANSI_C
#include <stdarg.h>

/* prototypes */

int v_get_vars(int dim, ...);
int m_get_vars(int m, int n, ...);

int v_resize_vars(int new_dim, ...);
int m_resize_vars(int m, int n, ...);

int v_free_vars(VEC **, ...);
int m_free_vars(MAT **, ...);

#elif VARARGS

#endif /* ANSI_C */

#endif /* MATRIXH */

