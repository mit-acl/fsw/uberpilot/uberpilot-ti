################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Each subdirectory must supply rules for building sources it contributes
AHRS.obj: ../AHRS.c $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv5/tools/compiler/c2000_6.1.3/bin/cl2000" -v28 -ml -mt --float_support=fpu32 --vcu_support=vcu0 -O4 -ms -g --include_path="C:/ti/ccsv5/tools/compiler/c2000_6.1.3/include" --include_path="C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_common/include" --include_path="C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_headers/include" --include_path="C:/ti/xdais_7_21_01_07/packages/ti/xdais" --diag_warning=225 --display_error_number --preproc_with_compile --preproc_dependency="AHRS.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

AttitudeControl.obj: ../AttitudeControl.c $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv5/tools/compiler/c2000_6.1.3/bin/cl2000" -v28 -ml -mt --float_support=fpu32 --vcu_support=vcu0 -O4 -ms -g --include_path="C:/ti/ccsv5/tools/compiler/c2000_6.1.3/include" --include_path="C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_common/include" --include_path="C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_headers/include" --include_path="C:/ti/xdais_7_21_01_07/packages/ti/xdais" --diag_warning=225 --display_error_number --preproc_with_compile --preproc_dependency="AttitudeControl.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

F28M35x_Adc.obj: C:/ti/controlSUITE/device_support/f28m35x/v160/F28M35x_common/source/F28M35x_Adc.c $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv5/tools/compiler/c2000_6.1.3/bin/cl2000" -v28 -ml -mt --float_support=fpu32 --vcu_support=vcu0 -O4 -ms -g --include_path="C:/ti/ccsv5/tools/compiler/c2000_6.1.3/include" --include_path="C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_common/include" --include_path="C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_headers/include" --include_path="C:/ti/xdais_7_21_01_07/packages/ti/xdais" --diag_warning=225 --display_error_number --preproc_with_compile --preproc_dependency="F28M35x_Adc.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

F28M35x_CodeStartBranch.obj: C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_common/source/F28M35x_CodeStartBranch.asm $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv5/tools/compiler/c2000_6.1.3/bin/cl2000" -v28 -ml -mt --float_support=fpu32 --vcu_support=vcu0 -O4 -ms -g --include_path="C:/ti/ccsv5/tools/compiler/c2000_6.1.3/include" --include_path="C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_common/include" --include_path="C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_headers/include" --include_path="C:/ti/xdais_7_21_01_07/packages/ti/xdais" --diag_warning=225 --display_error_number --preproc_with_compile --preproc_dependency="F28M35x_CodeStartBranch.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

F28M35x_DefaultIsr.obj: C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_common/source/F28M35x_DefaultIsr.c $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv5/tools/compiler/c2000_6.1.3/bin/cl2000" -v28 -ml -mt --float_support=fpu32 --vcu_support=vcu0 -O4 -ms -g --include_path="C:/ti/ccsv5/tools/compiler/c2000_6.1.3/include" --include_path="C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_common/include" --include_path="C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_headers/include" --include_path="C:/ti/xdais_7_21_01_07/packages/ti/xdais" --diag_warning=225 --display_error_number --preproc_with_compile --preproc_dependency="F28M35x_DefaultIsr.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

F28M35x_EPwm.obj: C:/ti/controlSUITE/device_support/f28m35x/v160/F28M35x_common/source/F28M35x_EPwm.c $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv5/tools/compiler/c2000_6.1.3/bin/cl2000" -v28 -ml -mt --float_support=fpu32 --vcu_support=vcu0 -O4 -ms -g --include_path="C:/ti/ccsv5/tools/compiler/c2000_6.1.3/include" --include_path="C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_common/include" --include_path="C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_headers/include" --include_path="C:/ti/xdais_7_21_01_07/packages/ti/xdais" --diag_warning=225 --display_error_number --preproc_with_compile --preproc_dependency="F28M35x_EPwm.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

F28M35x_GlobalVariableDefs.obj: C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_headers/source/F28M35x_GlobalVariableDefs.c $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv5/tools/compiler/c2000_6.1.3/bin/cl2000" -v28 -ml -mt --float_support=fpu32 --vcu_support=vcu0 -O4 -ms -g --include_path="C:/ti/ccsv5/tools/compiler/c2000_6.1.3/include" --include_path="C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_common/include" --include_path="C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_headers/include" --include_path="C:/ti/xdais_7_21_01_07/packages/ti/xdais" --diag_warning=225 --display_error_number --preproc_with_compile --preproc_dependency="F28M35x_GlobalVariableDefs.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

F28M35x_Ipc_Util.obj: C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_common/source/F28M35x_Ipc_Util.c $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv5/tools/compiler/c2000_6.1.3/bin/cl2000" -v28 -ml -mt --float_support=fpu32 --vcu_support=vcu0 -O4 -ms -g --include_path="C:/ti/ccsv5/tools/compiler/c2000_6.1.3/include" --include_path="C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_common/include" --include_path="C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_headers/include" --include_path="C:/ti/xdais_7_21_01_07/packages/ti/xdais" --diag_warning=225 --display_error_number --preproc_with_compile --preproc_dependency="F28M35x_Ipc_Util.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

F28M35x_PieCtrl.obj: C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_common/source/F28M35x_PieCtrl.c $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv5/tools/compiler/c2000_6.1.3/bin/cl2000" -v28 -ml -mt --float_support=fpu32 --vcu_support=vcu0 -O4 -ms -g --include_path="C:/ti/ccsv5/tools/compiler/c2000_6.1.3/include" --include_path="C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_common/include" --include_path="C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_headers/include" --include_path="C:/ti/xdais_7_21_01_07/packages/ti/xdais" --diag_warning=225 --display_error_number --preproc_with_compile --preproc_dependency="F28M35x_PieCtrl.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

F28M35x_PieVect.obj: C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_common/source/F28M35x_PieVect.c $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv5/tools/compiler/c2000_6.1.3/bin/cl2000" -v28 -ml -mt --float_support=fpu32 --vcu_support=vcu0 -O4 -ms -g --include_path="C:/ti/ccsv5/tools/compiler/c2000_6.1.3/include" --include_path="C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_common/include" --include_path="C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_headers/include" --include_path="C:/ti/xdais_7_21_01_07/packages/ti/xdais" --diag_warning=225 --display_error_number --preproc_with_compile --preproc_dependency="F28M35x_PieVect.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

F28M35x_SysCtrl.obj: C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_common/source/F28M35x_SysCtrl.c $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv5/tools/compiler/c2000_6.1.3/bin/cl2000" -v28 -ml -mt --float_support=fpu32 --vcu_support=vcu0 -O4 -ms -g --include_path="C:/ti/ccsv5/tools/compiler/c2000_6.1.3/include" --include_path="C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_common/include" --include_path="C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_headers/include" --include_path="C:/ti/xdais_7_21_01_07/packages/ti/xdais" --diag_warning=225 --display_error_number --preproc_with_compile --preproc_dependency="F28M35x_SysCtrl.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

F28M35x_usDelay.obj: C:/ti/controlSUITE/device_support/f28m35x/v160/F28M35x_common/source/F28M35x_usDelay.asm $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv5/tools/compiler/c2000_6.1.3/bin/cl2000" -v28 -ml -mt --float_support=fpu32 --vcu_support=vcu0 -O4 -ms -g --include_path="C:/ti/ccsv5/tools/compiler/c2000_6.1.3/include" --include_path="C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_common/include" --include_path="C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_headers/include" --include_path="C:/ti/xdais_7_21_01_07/packages/ti/xdais" --diag_warning=225 --display_error_number --preproc_with_compile --preproc_dependency="F28M35x_usDelay.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

PositionControl.obj: ../PositionControl.c $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv5/tools/compiler/c2000_6.1.3/bin/cl2000" -v28 -ml -mt --float_support=fpu32 --vcu_support=vcu0 -O4 -ms -g --include_path="C:/ti/ccsv5/tools/compiler/c2000_6.1.3/include" --include_path="C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_common/include" --include_path="C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_headers/include" --include_path="C:/ti/xdais_7_21_01_07/packages/ti/xdais" --diag_warning=225 --display_error_number --preproc_with_compile --preproc_dependency="PositionControl.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

main_c28.obj: ../main_c28.c $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv5/tools/compiler/c2000_6.1.3/bin/cl2000" -v28 -ml -mt --float_support=fpu32 --vcu_support=vcu0 -O4 -ms -g --include_path="C:/ti/ccsv5/tools/compiler/c2000_6.1.3/include" --include_path="C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_common/include" --include_path="C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_headers/include" --include_path="C:/ti/xdais_7_21_01_07/packages/ti/xdais" --diag_warning=225 --display_error_number --preproc_with_compile --preproc_dependency="main_c28.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

quaternion.obj: ../quaternion.c $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv5/tools/compiler/c2000_6.1.3/bin/cl2000" -v28 -ml -mt --float_support=fpu32 --vcu_support=vcu0 -O4 -ms -g --include_path="C:/ti/ccsv5/tools/compiler/c2000_6.1.3/include" --include_path="C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_common/include" --include_path="C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_headers/include" --include_path="C:/ti/xdais_7_21_01_07/packages/ti/xdais" --diag_warning=225 --display_error_number --preproc_with_compile --preproc_dependency="quaternion.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

setup_c28.obj: ../setup_c28.c $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv5/tools/compiler/c2000_6.1.3/bin/cl2000" -v28 -ml -mt --float_support=fpu32 --vcu_support=vcu0 -O4 -ms -g --include_path="C:/ti/ccsv5/tools/compiler/c2000_6.1.3/include" --include_path="C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_common/include" --include_path="C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_headers/include" --include_path="C:/ti/xdais_7_21_01_07/packages/ti/xdais" --diag_warning=225 --display_error_number --preproc_with_compile --preproc_dependency="setup_c28.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

utils.obj: ../utils.c $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv5/tools/compiler/c2000_6.1.3/bin/cl2000" -v28 -ml -mt --float_support=fpu32 --vcu_support=vcu0 -O4 -ms -g --include_path="C:/ti/ccsv5/tools/compiler/c2000_6.1.3/include" --include_path="C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_common/include" --include_path="C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_headers/include" --include_path="C:/ti/xdais_7_21_01_07/packages/ti/xdais" --diag_warning=225 --display_error_number --preproc_with_compile --preproc_dependency="utils.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

wii.obj: ../wii.c $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv5/tools/compiler/c2000_6.1.3/bin/cl2000" -v28 -ml -mt --float_support=fpu32 --vcu_support=vcu0 -O4 -ms -g --include_path="C:/ti/ccsv5/tools/compiler/c2000_6.1.3/include" --include_path="C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_common/include" --include_path="C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_headers/include" --include_path="C:/ti/xdais_7_21_01_07/packages/ti/xdais" --diag_warning=225 --display_error_number --preproc_with_compile --preproc_dependency="wii.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '


