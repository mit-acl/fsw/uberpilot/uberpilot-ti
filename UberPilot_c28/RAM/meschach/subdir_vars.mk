################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../meschach/copy.c \
../meschach/hsehldr.c \
../meschach/init.c \
../meschach/machine.c \
../meschach/matop.c \
../meschach/meminfo.c \
../meschach/memory.c \
../meschach/norm.c \
../meschach/qrfactor.c \
../meschach/solve.c \
../meschach/submat.c \
../meschach/vecop.c 

OBJS += \
./meschach/copy.obj \
./meschach/hsehldr.obj \
./meschach/init.obj \
./meschach/machine.obj \
./meschach/matop.obj \
./meschach/meminfo.obj \
./meschach/memory.obj \
./meschach/norm.obj \
./meschach/qrfactor.obj \
./meschach/solve.obj \
./meschach/submat.obj \
./meschach/vecop.obj 

C_DEPS += \
./meschach/copy.pp \
./meschach/hsehldr.pp \
./meschach/init.pp \
./meschach/machine.pp \
./meschach/matop.pp \
./meschach/meminfo.pp \
./meschach/memory.pp \
./meschach/norm.pp \
./meschach/qrfactor.pp \
./meschach/solve.pp \
./meschach/submat.pp \
./meschach/vecop.pp 

C_DEPS__QUOTED += \
"meschach\copy.pp" \
"meschach\hsehldr.pp" \
"meschach\init.pp" \
"meschach\machine.pp" \
"meschach\matop.pp" \
"meschach\meminfo.pp" \
"meschach\memory.pp" \
"meschach\norm.pp" \
"meschach\qrfactor.pp" \
"meschach\solve.pp" \
"meschach\submat.pp" \
"meschach\vecop.pp" 

OBJS__QUOTED += \
"meschach\copy.obj" \
"meschach\hsehldr.obj" \
"meschach\init.obj" \
"meschach\machine.obj" \
"meschach\matop.obj" \
"meschach\meminfo.obj" \
"meschach\memory.obj" \
"meschach\norm.obj" \
"meschach\qrfactor.obj" \
"meschach\solve.obj" \
"meschach\submat.obj" \
"meschach\vecop.obj" 

C_SRCS__QUOTED += \
"../meschach/copy.c" \
"../meschach/hsehldr.c" \
"../meschach/init.c" \
"../meschach/machine.c" \
"../meschach/matop.c" \
"../meschach/meminfo.c" \
"../meschach/memory.c" \
"../meschach/norm.c" \
"../meschach/qrfactor.c" \
"../meschach/solve.c" \
"../meschach/submat.c" \
"../meschach/vecop.c" 


