/*
 * defines_c28.h
 *
 *  Created on: Nov 5, 2012
 *      Author: mark
 */

#ifndef DEFINES_C28_H_
#define DEFINES_C28_H_

#include "DSP28x_Project.h"     // Device Headerfile and Examples Include File
#include "F28M35x_Ipc_drivers.h"
#include "F28M35x_Adc.h"

#include <math.h>
#include <stdlib.h>

#define _C28_
#undef _M3_
#undef _OFFBOARDFILT_

// local includes
#include "../UberPilot_shared/data_structs.h"
#include "AHRS.h"
#include "wii.h"
#include "meschach/matrix.h"
#include "meschach/matrix2.h"

#define USE_VELOCITY_MEASUREMENT	// use velocity as a measurement
//#define USE_SEPARATE_Z_MEASUREMENT	// do separate x,y and z position update steps
#define USE_VICON_ATTITUDE
#define USE_VICON_YAW

#define MASS 	0.39 //0.15	// vehicle weight in kgrams

#define POSITION_CONTROL_DT		0.01		// 100 Hz
#define ATTITUDE_CONTROL_DT		0.002		// Hz

#define IMUDT	0.001

#define PI	3.14159265359
#define GRAVITY 9.81

#define TRUE	1
#define	FALSE	0

#define VSCALE	3.4514 	// voltage scale
#define VBIAS	-4.7237	// voltage bias

enum state_identifiers {
	XPOS,
	YPOS,
	ZPOS,
	XVEL,
	YVEL,
	ZVEL,
	AXBIAS,
	AYBIAS,
	AZBIAS,
	AX = 0,
	AY,
	AZ,
	E0,
	E1,
	E2,
	E3
};


/******************************
 * PWM Defines
 */
#define PWM1	EPwm1Regs.CMPA.half.CMPA
#define PWM2	EPwm1Regs.CMPB
#define PWM3	EPwm2Regs.CMPA.half.CMPA
#define PWM4	EPwm2Regs.CMPB
#define PWM5	EPwm3Regs.CMPA.half.CMPA
#define PWM6	EPwm3Regs.CMPB
#define PWM7	EPwm4Regs.CMPA.half.CMPA
#define PWM8	EPwm4Regs.CMPB
#define SETPWM(channel,x)	(channel = (x*15000 + 15000))

#define  PERIOD         4000
#define  DUTY_CYCLE_A   PERIOD/2

#define DBG_PB5			GPADAT.bit.GPIO13
#define toggle_dbg(x)	GpioG1DataRegs.x = !GpioG1DataRegs.x

void InitPWM(void);
void getDT(volatile float32 *dt, volatile Uint32 *tprev);
void satvoid(float32 * val, float32 hi, float32 low);
float32 satreturn(float32 val, float32 hi, float32 low);
void InitSysCtrlC28(void);
void InitGPIOC28(void);
void InitTimer(void);
void InitInterrupts(void);
void InitADC(void);
void InitFilter(void);

// controller function definitions
void RunController(void);
void GetAccel(void);
void GetAttitude(void);
void GetRate(void);
void CheckController(void);
float32 F2motorCmd(float32 f);

typedef struct sLoopFlags{
    int TimeProp;
    int MeasurementProp_posvel;
    int MeasurementProp_pos;
    int MeasurementProp_xy;
    int MeasurementProp_z;
    int MeasurementUpdate;
    int WiiProp;
}tLoopFlags;


#endif /* DEFINES_C28_H_ */
