/*
 * PositionControl.c
 *  Created on: Jan 24, 2013
 *      Author: Mark Cutler
 */

#include "defines_c28.h"

// globals
extern volatile tAHRSdata AHRSdata;
extern volatile tAttitudeCmd AttitudeCmd;
extern volatile tGains Gains;
extern volatile tGoal Goal;
extern volatile tState State;

void GetAccel(void)
{
  // Position Errors
  float32 xPosErr = satreturn(Goal.x - State.x, Gains.maxPosErr_xy, -Gains.maxPosErr_xy);
  float32 yPosErr = satreturn(Goal.y - State.y, Gains.maxPosErr_xy, -Gains.maxPosErr_xy);
  float32 zPosErr = satreturn(Goal.z - State.z, Gains.maxPosErr_z, -Gains.maxPosErr_z);

  // Velocity Errors
  float32 xVelErr = satreturn(Goal.dx - State.dx, Gains.maxVelErr_xy, -Gains.maxVelErr_xy);
  float32 yVelErr = satreturn(Goal.dy - State.dy, Gains.maxVelErr_xy, -Gains.maxVelErr_xy);
  float32 zVelErr = satreturn(Goal.dz - State.dz, Gains.maxVelErr_z, -Gains.maxVelErr_z);

  // Integrate Position Errors
  if (fabs(xPosErr < Gains.I_xy_int_thresh) && AttitudeCmd.AttCmd) // TODO: only increment integrator when flying (need to condition on throttle value maybe?
  {
	  Gains.I_x += xPosErr*Gains.PositionDT;
  }
  if (fabs(yPosErr < Gains.I_xy_int_thresh) && AttitudeCmd.AttCmd)
  {
	  Gains.I_y += yPosErr*Gains.PositionDT;
  }
  if (fabs(zPosErr < Gains.I_z_int_thresh) && AttitudeCmd.AttCmd)
  {
	  Gains.I_z += zPosErr*Gains.PositionDT;
  }

  // Desired Feedback Acceleration -- this is added to the feedforward acceleration
  if (AttitudeCmd.AttCmd == POSITION)
  {
	  Goal.d2x_fb = Gains.Kp_xy * xPosErr + Gains.Kd_xy * xVelErr + Gains.Ki_xy * Gains.I_x;
	  Goal.d2y_fb = Gains.Kp_xy * yPosErr + Gains.Kd_xy * yVelErr + Gains.Ki_xy * Gains.I_y;
  }
  Goal.d2z_fb = Gains.Kp_z * zPosErr + Gains.Kd_z * zVelErr + Gains.Ki_z * Gains.I_z;

  float32 r1_nom = -MASS*Goal.d2x;
  float32 r2_nom = -MASS*Goal.d2y;
  float32 r3_nom =  MASS*(Goal.d2z + GRAVITY);
  Gains.rmag = sqrt(r1_nom*r1_nom + r2_nom*r2_nom + r3_nom*r3_nom);

  if (Gains.rmag > Gains.minRmagint)
  {	// use feedback values
    Goal.useFeedback = TRUE;
  }
  else
  {
    Goal.useFeedback = FALSE;
    Gains.I_x = 0.0;
    Gains.I_y = 0.0;
    Gains.I_z = 0.0; // reset position integrators when doing "open loop" attitude
    Goal.d2x_fb = 0.0;
    Goal.d2y_fb = 0.0;
  }

  if (AttitudeCmd.AttCmd == POSITION)
  {
	  // Numerically Differentiate feedback acceleration
	  Goal.d3x_fb = (Goal.d2x_fb - Goal.d2x_fb_old) / Gains.PositionDT;
	  Goal.d3y_fb = (Goal.d2y_fb - Goal.d2y_fb_old) / Gains.PositionDT;
	  Goal.d3z_fb = (Goal.d2z_fb - Goal.d2z_fb_old) / Gains.PositionDT;

	  // filter the differentiation
	  float32 alpha = Gains.PositionDT / (0.1 + Gains.PositionDT);
	  Goal.d3x_fb = Goal.d3x_fb_old + alpha * (Goal.d3x_fb - Goal.d3x_fb_old);
	  Goal.d3y_fb = Goal.d3y_fb_old + alpha * (Goal.d3y_fb - Goal.d3y_fb_old);
	  Goal.d3z_fb = Goal.d3z_fb_old + alpha * (Goal.d3z_fb - Goal.d3z_fb_old);
  }

  // zero numerical differentiation if real commands are being sent
  if (fabs(Goal.d3x) > 0.1)
	  Goal.d3x_fb = 0.0;
  if (fabs(Goal.d3y) > 0.1)
    Goal.d3y_fb = 0.0;
  if (fabs(Goal.d3z) > 0.1)
    Goal.d3z_fb = 0.0;

  // update variables
  Goal.d2x_fb_old = Goal.d2x_fb;
  Goal.d2y_fb_old = Goal.d2y_fb;
  Goal.d2z_fb_old = Goal.d2z_fb;

  Goal.d3x_fb_old = Goal.d3x_fb;
  Goal.d3y_fb_old = Goal.d3y_fb;
  Goal.d3z_fb_old = Goal.d3z_fb;
}

void GetAttitude()
{

  float32 r1_nom,r2_nom,r3_nom;
  r3_nom = MASS *(Goal.d2z + GRAVITY);
  if (Goal.useFeedback)
    {
      r1_nom  = -MASS*(Goal.d2x + Goal.d2x_fb);
      r2_nom  = -MASS*(Goal.d2y + Goal.d2y_fb);
    }
  else
    {
      r1_nom  = -MASS*(Goal.d2x);
      r2_nom  = -MASS*(Goal.d2y);
    }

  float r1 = r1_nom;
  float r2 = r2_nom;
  float r3 = r3_nom;
  float rmag = sqrt(r1*r1 + r2*r2 + r3*r3);

  if (rmag > 0.001){
    r1 /= rmag;
    r2 /= rmag;
    r3 /= rmag;
  }

  //b
  float32 b1 = 0.;
  float32 b2 = 0.;
  float32 b3 = 1.;

  // cross product of r and b
  float32 rxb1 = r2*b3 - r3*b2;
  float32 rxb2 = r3*b1 - r1*b3;
  float32 rxb3 = r1*b2 - r2*b1;

  // dot product of r and b
  float32 rdb = r1*b1 + r2*b2 + r3*b3;

  // find qmin
  float32 tmp = 0;
  float32 eps = 0.05;
  tQuaternion qmin = {1.0,0.0,0.0,0.0};

  if (Gains.rmag > Gains.minRmag)
    {
      if (rdb > -1.0 + eps)
	{
	  tmp = 1.0 / sqrt(2.0 * (1.0 + rdb));
	  qmin.x = tmp*rxb1;
	  qmin.y = tmp*rxb2;
	  qmin.z = tmp*rxb3;
	  qmin.o = tmp*(1 + rdb);
	}
    }

  qnormalize(&qmin);

  // recompute r3 using true desired acceleration
  r3_nom = MASS*(Goal.d2z + Goal.d2z_fb + GRAVITY);
  rmag = sqrt(r1_nom*r1_nom + r2_nom*r2_nom + r3_nom*r3_nom);
  r1 = r1_nom/rmag;
  r2 = r2_nom/rmag;
  r3 = r3_nom/rmag;

  tQuaternion rnorm;
  rnorm.o = 0.0;
  rnorm.x = r1;
  rnorm.y = r2;
  rnorm.z = r3;

  // calculate sum of forces for thrust control
  tQuaternion bnew = qprod(qmin,qprod(rnorm,qconj(qmin)));
  qnormalize(&bnew);

  Goal.f_total = rmag * bnew.z;

  // ensure quaternions agree with each other // TODO: make this a separate function
  if (Gains.rmag > Gains.minRmag)
  {
    // check for continuity with last qmin
    if (qdot(qmin,AttitudeCmd.qmin_old) < 0.0)
    {
      qmult(qmin,-1.0);
    }

    // check for continuity with State quaternion
    if (qdot(qmin,AHRSdata.q_est) < 0.0)
    {
      qmult(qmin,-1.0);
    }
    qnormalize(&qmin);

    AttitudeCmd.qmin_old = qmin;
  }


  if (AttitudeCmd.AttCmd >= ATTITUDE_AND_Z)
	  AttitudeCmd.throttle = satreturn(F2motorCmd(Goal.f_total), 0.9, 0.0);
  // otherwise, throttle is being set from an outside program

  // only assign Goal attitude if you aren't close to a singularity or aren't trying to set it
  // via the xbox controller
  if (Gains.rmag > Gains.minRmag && AttitudeCmd.AttCmd == POSITION)
  {
	  AttitudeCmd.q_cmd = qmin;

    // Rotate by yaw
    Goal.qyaw_des.o = cos(Goal.yaw/2.0);
    Goal.qyaw_des.x = 0.0;
    Goal.qyaw_des.y = 0.0;
    Goal.qyaw_des.z = sin(Goal.yaw/2.0);
    AttitudeCmd.q_cmd = qprod(AttitudeCmd.q_cmd, Goal.qyaw_des);
  }

}

// convert a desired force in newtons to desired motor command
float32 F2motorCmd(float32 f)
{
  return Gains.a * f * f + Gains.b * f + Gains.c;
}

void GetRate()
{	// TODO: make getRate and getAttitude compatible with each other
  float f1,f2;
  float32 f3 = MASS * (Goal.d2z + GRAVITY);
  if (Goal.useFeedback)
    {
      f1 = MASS * (Goal.d2x + Goal.d2x_fb);
      f2 = MASS * (Goal.d2y + Goal.d2y_fb);
    }
  else
    {
      f1 = MASS * Goal.d2x;
      f2 = MASS * Goal.d2y;
    }

  //fbar
  float32 fmag = sqrt(f1*f1 + f2*f2 + f3*f3);
  float32 fb1 = f1/fmag;
  //float32 fb2 = f2/fmag;
  float32 fb3 = f3/fmag;

  //fdot
  float32 fd1 = MASS * (Goal.d3x + Goal.d3x_fb);
  float32 fd2 = MASS * (Goal.d3y + Goal.d3y_fb);
  float32 fd3 = MASS * (Goal.d3z + Goal.d3z_fb);

  //fbardot
  float32 fdotfd = f1*fd1 + f2*fd2 + f3*fd3;
  float32 den = 1/(fmag*fmag*fmag);
  float32 dfb1 = fd1/fmag - f1*fdotfd*den;
  float32 dfb2 = fd2/fmag - f2*fdotfd*den;
  float32 dfb3 = fd3/fmag - f3*fdotfd*den;

  // omega = fbar x dfbar
  float32 w1 = fb1*dfb3 - fb3*dfb2;
  float32 w2 = fb3*dfb1 - fb1*dfb3;
  //float32 w3 = fb1*dfb2 - fb2*dfb1;

  if (AttitudeCmd.AttCmd == POSITION)
  {
	  if (Gains.rmag > Gains.minRmag)
	  {
		  AttitudeCmd.p = w1;
		  AttitudeCmd.q = w2;
	  }
	  AttitudeCmd.r = 0.0;  // yaw rate is not valid to compute here

	  // rotate by yaw
	  tQuaternion qrate;
	  qrate.o = 0.0;
	  qrate.x = AttitudeCmd.p;
	  qrate.y = AttitudeCmd.q;
	  qrate.z = 0.0;
	  qrate = qprod(qconj(Goal.qyaw_des),qprod(qrate,Goal.qyaw_des));
	  AttitudeCmd.p = 0.0;//qrate.x;
	  AttitudeCmd.q = 0.0;//qrate.y;
  }
}

void RunController(void)
{
  GetAccel();
  GetAttitude();
  GetRate();
}

void CheckController(void)
{
	if (AttitudeCmd.AttCmd == NOT_FLYING)
	{
		// Reset Integrators
		Gains.I_x = Gains.I_y = Gains.I_z = 0.0;
	}
}
