/*******************************
 * UberPilot -- autopilot software for the Aeropace Controls Lab at MIT
 * Written by Mark Cutler
 * acl.mit.edu
 * C28 Core Code
 *******************************/

//local includes
#include "defines_c28.h"
#include "SRCDKF.h"

#include<stdio.h>
#include <math.h>
#include<stdlib.h>

#include "F28M35x_Device.h"     // F28M35x Headerfile Include File
#include "F28M35x_Examples.h"   // F28M35x Examples Include File


#ifdef _FLASH
// These are defined by the linker (see device linker command file)
extern Uint16 RamfuncsLoadStart;
extern Uint16 RamfuncsLoadSize;
extern Uint16 RamfuncsRunStart;

extern Uint16 FPUmathTablesLoadStart;
extern Uint16 FPUmathTablesLoadEnd;
extern Uint16 FPUmathTablesRunStart;
#endif

//*****************************************************************************
// Function Prototypes
//*****************************************************************************
void Error(void);
interrupt void MtoCIPC2IntHandler(void);
interrupt void ADC2IntHandler(void);
interrupt void CPUTimer1IntHandler(void);
void CtoMIPCBlockWrite(unsigned char id, void * data, unsigned int size, unsigned int blocking);

//*****************************************************************************
// IPC Definitions
//*****************************************************************************
// see f28m35h32b.pdf for these addresses
#define C28_MTOC_PASSMSG  0x0003FC00//0x0003FFF4            // Used by M3 to pass address of
// local variables to perform
// actions on
#define C28_CTOM_PASSMSG  0x0003F800//0x0003FBF4            // Used by C28 to pass address
// of local variables to perform
// actions on


//*****************************************************************************
// Global variable
//*****************************************************************************
volatile Uint16 ErrorFlag;
volatile tIMUPacket IMU;
volatile tSensorCal SensorCal;
volatile tAHRSdata AHRSdata;
volatile tStatePacket StatePacket;
volatile tPosVelPacket PosVelPacket;
volatile tPosPacket PosPacket;
volatile tWiiIPCPacket WiiPacket;
volatile tAttitudeCmd AttitudeCmd;
volatile tAttCmdPacket AttCmdPacket;
volatile tAttZCmdPacket AttZCmdPacket;
volatile tPosYawCmdPacket PosYawCmdPacket;
volatile tSensorCalPacket SensorCalPacket;
volatile tMagPacket MagPacket;
volatile tViconStatePacket ViconStatePacket;
volatile tGains Gains;
volatile tMotorData MotorData;
volatile tGoal Goal;
volatile tState State;
Uint8 *pucMtoCMsgRam; // pointer of unsigned chars containing data from M to C
Uint8 *pucCtoMMsgRam; // pointer of unsigned chars containing data from C to M
volatile tLoopFlags loop;

// variables used for voltage conversion
Uint16 LoopCount;
Uint16 ConversionCount;
Uint16 Voltage[10];
Uint16 AvgVoltage;
Uint32 SumVoltage;

extern VEC * x; // State vector
VEC * u;
#ifdef USE_SEPARATE_Z_MEASUREMENT
VEC * Y_xy;
VEC * Sn_xy;
VEC * Y_z;
VEC * Sn_z;
#else
VEC * Y;
VEC * Sn;
#endif

struct CPUTIMER_VARS CpuTimer0;
struct CPUTIMER_VARS CpuTimer1;

//*****************************************************************************
// Main Function
//*****************************************************************************
main(){


#ifdef _FLASH
// Copy time critical code and Flash setup code to RAM
// This includes the following functions: InitFlash();
// The  RamfuncsLoadStart, RamfuncsLoadEnd, and RamfuncsRunStart
// symbols are created by the linker. Refer to the device .cmd file.
        memcpy(&RamfuncsRunStart, &RamfuncsLoadStart, (size_t)&RamfuncsLoadSize);
        memcpy(&FPUmathTablesLoadStart, &FPUmathTablesLoadEnd, (size_t)&FPUmathTablesRunStart);

// Call Flash Initialization to setup flash waitstates
// This function must reside in RAM
        InitFlash();
#endif

// Step 1. Initialize System Control:
// PLL, WatchDog, enable Peripheral Clocks
// This example function is found in the F28M35x_SysCtrl.c file.
        //InitSysCtrl();
        InitSysCtrlC28();

        //tmp

// Step 2. Initalize GPIO:
// This example function is found in the F28M35x_Gpio.c file and
// illustrates how to set the GPIO to it's default state.
// InitGpio();  // Skipped for this example
        InitGPIOC28();

// Step 3. Clear all interrupts and initialize PIE vector table:
// Disable CPU interrupts

        DINT;

// Initialize PIE control registers to their default state.
// The default state is all PIE interrupts disabled and flags
// are cleared.
// This function is found in the F28M35x_PieCtrl.c file.
        InitPieCtrl();

// Disable CPU interrupts and clear all CPU interrupt flags:
        IER = 0x0000;
        IFR = 0x0000;

// Initialize the PIE vector table with pointers to the shell Interrupt
// Service Routines (ISR).
// This will populate the entire table, even if the interrupt
// is not used in this example.  This is useful for debug purposes.
// The shell ISR routines are found in F28M35x_DefaultIsr.c.
// This function is found in F28M35x_PieVect.c.
        InitPieVectTable();

        // Interrupts that are used in this example are re-mapped to
        // ISR functions found within this file.
        EALLOW; // This is needed to write to EALLOW protected registers
        PieVectTable.MTOCIPC_INT2 = &MtoCIPC2IntHandler;
        PieVectTable.ADCINT1 = &ADC2IntHandler;  // ADC interrumpt handler
        PieVectTable.TINT1 = &CPUTimer1IntHandler;  // control timer interrupt
        EDIS;    // This is needed to disable write to EALLOW protected registers

// Step 4. Initialize the Device Peripherals:
        // PWM Initialization
        InitPWM();
        InitAdc2();
        InitTimer();

// Step 5. User specific code, enable interrupts:
        InitInterrupts();
        InitADC();
        InitFilter();
        AHRS_init();
        Controller_Init();

        // Define M2C pointer
        pucMtoCMsgRam = (void *)C28_MTOC_PASSMSG;
        pucCtoMMsgRam = (void *)C28_CTOM_PASSMSG;

        // Flag to M3 that the variables are ready in MSG RAM with CTOM IPC Flag 17
        //
        IPCCtoMFlagSet(IPC_FLAG17);

        // Start CpuTimer0 for calculating dt's
        CpuTimer0Regs.TCR.bit.TSS = 0;      // 1 = Stop timer, 0 = Start/Restart
        // Timer

        // Start CPUTimer1 for running control laws
        CpuTimer1Regs.TCR.bit.TSS = 0;

        float32 dt = 1.0/100.0;
        Uint32 tprev = 0;

        //todo : clean up this numerical diff code
        float32 xMeas = 0.0;
        float32 xMeas_old = 0.0;
        float32 dxMeas = 0.0;
        float32 dxMeas_old = 0.0;
        float32 yMeas = 0.0;
        float32 yMeas_old = 0.0;
        float32 dyMeas = 0.0;
        float32 dyMeas_old = 0.0;
        for(;;)
        {

                if (loop.WiiProp)
                {
                        loop.WiiProp = 0;
                        processWiiData();
						
#ifdef USE_VELOCITY_MEASUREMENT
                        float32 alpha = 0.1;
                        float32 wiiDt = 0.01;
                        // numerically differentiate position estimates
						xMeas = (float32) PosPacket.x / POS_SCALE;
						dxMeas = (xMeas - xMeas_old) / wiiDt; // 100 Hz
						dxMeas = dxMeas_old + alpha * (dxMeas - dxMeas_old);
						xMeas_old = xMeas;
						dxMeas_old = dxMeas;

                        // numerically differentiate position estimates
						yMeas = (float32) PosPacket.y / POS_SCALE;
						dyMeas = (yMeas - yMeas_old) / wiiDt; // 100 Hz
						dyMeas = dyMeas_old + alpha * (dyMeas - dyMeas_old);
						yMeas_old = yMeas;
						dyMeas_old = dyMeas;


						PosVelPacket.x = (int32_t) (xMeas * POS_SCALE);
//						markxwhy = (int32_t) (xMeas * POS_SCALE);
						PosVelPacket.dx = (int32_t) (dxMeas * VEL_SCALE);
						PosVelPacket.y = (int32_t) (yMeas * POS_SCALE);
						PosVelPacket.dy = (int32_t) (dyMeas * VEL_SCALE);


						loop.MeasurementProp_posvel = 1;

                        // debug!!!
//                        State.x = xMeas;
//                        State.y = yMeas;
//                        State.dx = dxMeas;
//                        State.dy = dyMeas;
#endif
#ifdef USE_SEPARATE_Z_MEASUREMENT
                        loop.MeasurementProp_xy = 1;
#else
//                        loop.MeasurementProp_pos = 1;
#endif
                }

#ifdef USE_VELOCITY_MEASUREMENT
                if (loop.MeasurementProp_posvel)
                {
                        loop.MeasurementProp_posvel = 0;
                        loop.MeasurementUpdate = 1;
                        Y->ve[0] = PosVelPacket.x / POS_SCALE;
                        Y->ve[1] = PosVelPacket.y / POS_SCALE;
                        Y->ve[2] = PosVelPacket.z / POS_SCALE;
                        Y->ve[3] = PosVelPacket.dx / VEL_SCALE;
                        Y->ve[4] = PosVelPacket.dy / VEL_SCALE;
                        Y->ve[5] = PosVelPacket.dz / VEL_SCALE;
                        SRCDKF_measUpdate(Y, &h_posvel, Sn);
                        loop.MeasurementUpdate = 0;
                }
#elif defined USE_SEPARATE_Z_MEASUREMENT
                if (loop.MeasurementProp_xy)
                {
                        loop.MeasurementProp_xy = 0;
                        loop.MeasurementUpdate = 1;

                        Y_xy->ve[0] = PosPacket.x / POS_SCALE;
                        Y_xy->ve[1] = PosPacket.y / POS_SCALE;
                        toggle_dbg(DBG_PB5);
                        SRCDKF_measUpdate(Y_xy, &h_pos_xy, Sn_xy);
                        toggle_dbg(DBG_PB5);
                        loop.MeasurementUpdate = 0;
                }

                if (loop.MeasurementProp_z)
                {
                        loop.MeasurementProp_z = 0;
                        loop.MeasurementUpdate = 1;

                        Y_z->ve[0] = PosPacket.z / POS_SCALE;
                        toggle_dbg(DBG_PB5);
                        SRCDKF_measUpdate(Y_z, &h_pos_z, Sn_z);
                        toggle_dbg(DBG_PB5);
                        loop.MeasurementUpdate = 0;
                }
#else
                if (loop.MeasurementProp_pos)
                {
                        loop.MeasurementProp_pos = 0;
                        loop.MeasurementUpdate = 1;
                        //toggle_dbg(DBG_PB5);
                        //Y = v_resize(Y, 3);
                        Y->ve[0] = PosPacket.x / POS_SCALE;
                        Y->ve[1] = PosPacket.y / POS_SCALE;
                        Y->ve[2] = PosPacket.z / POS_SCALE;
                        SRCDKF_measUpdate(Y, &h_pos, Sn);
                        loop.MeasurementUpdate = 0;
                        //toggle_dbg(DBG_PB5);
                }
#endif

                if (loop.TimeProp)
                {
                        loop.TimeProp = 0;
                        //float32 dt = IMUDT;
                        //toggle_dbg(DBG_PB5);
                        // Approximately 50 Hz
                        static unsigned int cnt = 0;
                        if (cnt % 20 == 1){
                                //toggle_dbg(DBG_PB5);
#ifdef USE_VICON_ATTITUDE
                                AHRS_ViconCorrect();
#else
                                AHRS_AccMagCorrect();
#endif
                                //toggle_dbg(DBG_PB5);
                        }

                        cnt++;

                        getDT(&dt,&tprev);
                        u->ve[0] = AHRSdata.ax*GRAVITY; // ax (convert from g to m/s^2)
                        u->ve[1] = AHRSdata.ay*GRAVITY; // ay (convert from g to m/s^2)
                        u->ve[2] = AHRSdata.az*GRAVITY; // az (convert from g to m/s^2)
                        u->ve[3] = AHRSdata.q_est.o;    // qw
                        u->ve[4] = AHRSdata.q_est.x;    // qx
                        u->ve[5] = AHRSdata.q_est.y;    // qy
                        u->ve[6] = AHRSdata.q_est.z;    // qz

                        SRCDKF_timeUpdate(u, &dt, &f_kinematic);

                        State.x = x->ve[XPOS];
                        State.y = x->ve[YPOS];
                        State.z = x->ve[ZPOS];
                        State.dx = x->ve[XVEL];
                        State.dy = x->ve[YVEL];
                        State.dz = x->ve[ZVEL];

                        StatePacket.x = (int16_t) (State.x * ROT_SCALE);
                        StatePacket.y = (int16_t) (State.y * ROT_SCALE);
                        StatePacket.z = (int16_t) (State.z * ROT_SCALE);
                        StatePacket.dx = (int16_t) (State.dx * ROT_SCALE);
                        StatePacket.dy = (int16_t) (State.dy * ROT_SCALE);
                        StatePacket.dz = (int16_t) (State.dz * ROT_SCALE);
                        StatePacket.ddx = (int16_t) (AHRSdata.ax*GRAVITY * ROT_SCALE);
                        StatePacket.ddy = (int16_t) (AHRSdata.ay*GRAVITY * ROT_SCALE);
                        StatePacket.ddz = (int16_t) (AHRSdata.az*GRAVITY * ROT_SCALE);
                        StatePacket.ddx_bias = (int16_t) (x->ve[AXBIAS] * ROT_SCALE);
                        StatePacket.ddy_bias = (int16_t) (x->ve[AYBIAS] * ROT_SCALE);
                        StatePacket.ddz_bias = (int16_t) (x->ve[AZBIAS] * ROT_SCALE);
                        StatePacket.qw = (int16_t) (AHRSdata.q_est.o * QUAT_SCALE);
                        StatePacket.qx = (int16_t) (AHRSdata.q_est.x * QUAT_SCALE);
                        StatePacket.qy = (int16_t) (AHRSdata.q_est.y * QUAT_SCALE);
                        StatePacket.qz = (int16_t) (AHRSdata.q_est.z * QUAT_SCALE);
//                        StatePacket.qw = (int16_t) (AHRSdata.q_meas.o * QUAT_SCALE);
//                        StatePacket.qx = (int16_t) (AHRSdata.q_meas.x * QUAT_SCALE);
//                        StatePacket.qy = (int16_t) (AHRSdata.q_meas.y * QUAT_SCALE);
//                        StatePacket.qz = (int16_t) (AHRSdata.q_meas.z * QUAT_SCALE);
                        StatePacket.p = (int16_t) (AHRSdata.p * ROT_SCALE);
                        StatePacket.q = (int16_t) (AHRSdata.q * ROT_SCALE);
                        StatePacket.r = (int16_t) (AHRSdata.r * ROT_SCALE);

                        StatePacket.x_cmd = (int16_t) (Goal.x * ROT_SCALE);
                        StatePacket.y_cmd = (int16_t) (Goal.y * ROT_SCALE);
                        StatePacket.z_cmd = (int16_t) (Goal.z * ROT_SCALE);
                        StatePacket.dx_cmd = (int16_t) (Goal.dx * ROT_SCALE);
                        StatePacket.dy_cmd = (int16_t) (Goal.dy * ROT_SCALE);
                        StatePacket.dz_cmd = (int16_t) (Goal.dz * ROT_SCALE);
                        StatePacket.qw_cmd = (int16_t) (AttitudeCmd.q_cmd.o * QUAT_SCALE);
                        StatePacket.qx_cmd = (int16_t) (AttitudeCmd.q_cmd.x * QUAT_SCALE);
                        StatePacket.qy_cmd = (int16_t) (AttitudeCmd.q_cmd.y * QUAT_SCALE);
                        StatePacket.qz_cmd = (int16_t) (AttitudeCmd.q_cmd.z * QUAT_SCALE);
                        StatePacket.p_cmd = (int16_t) (AttitudeCmd.p * ROT_SCALE);
                        StatePacket.q_cmd = (int16_t) (AttitudeCmd.q * ROT_SCALE);
                        StatePacket.r_cmd = (int16_t) (AttitudeCmd.r * ROT_SCALE);

                        StatePacket.magx = (int16_t) (AHRSdata.magx * QUAT_SCALE);
                        StatePacket.magy = (int16_t) (AHRSdata.magy * QUAT_SCALE);
                        StatePacket.magz = (int16_t) (AHRSdata.magz * QUAT_SCALE);

                        StatePacket.dt = (uint16_t) (dt * VEL_SCALE);


                        // Approximately 10 Hz
                        if (cnt % 100 == 0)
                        {
                                //toggle_dbg(DBG_PB5);
                                CtoMIPCBlockWrite(PACKETID_STATE, (void *)&StatePacket.x, sizeof(tStatePacket), 0);
                                //toggle_dbg(DBG_PB5);
                        }

                        // Approximately 10 Hz
                        if (cnt % 100 == 1)
                        {
                                tVoltagePacket v;
                                v.voltage = AvgVoltage;
                                CtoMIPCBlockWrite(PACKETID_VOLTAGE, (void *)&v.voltage, sizeof(tVoltagePacket), 0);
                        }


                }

        }
}


void getDT(volatile float32 *dt, volatile Uint32 *tprev)
{
        static float32 div = 1/150000000.0;
        if (CpuTimer0Regs.TCR.bit.TIF)
        {
                CpuTimer0Regs.TCR.bit.TIF = 1; // clock rolled over
                *dt = (float32)(*tprev - CpuTimer0Regs.TIM.all + 0xFFFFFFFF)*div;
        } else
        {
                *dt = (*tprev - CpuTimer0Regs.TIM.all)*div;
        }
        if (CpuTimer0Regs.TIM.all < 1000000)
        {
                static int i = 0;
                i++;
        }
        *tprev = CpuTimer0Regs.TIM.all;
}

int controllerCnt = 0;
interrupt void CPUTimer1IntHandler(void)
{

	if (controllerCnt >= 5)
	{
//              // debug filter test!
//              if( SensorCal.biasCount >= SensorCal.biasTotal){
//              loop.MeasurementProp_pos = 1;
//              PosPacket.x = 0;
//              PosPacket.y = 0;
//              PosPacket.z = 0;
//              }
//
////             debug position controller test!
//              Goal.x = -2.5; Goal.y = -3.0; Goal.z = 0.75;
//              Goal.dx = 0.0; Goal.dy = 0.0; Goal.dz = 0.0;
//              Goal.d2x = 0.0; Goal.d2y = 0.0; Goal.d2z = 0.0;
//              Goal.d3x = 0.0; Goal.d3y = 0.0; Goal.d3z = 0.0;
//              AttitudeCmd.AttCmd = POSITION;
//              Goal.yaw = 0.0;

//              State.x = 0.0; State.y = 0.0; State.z = 0.0;
//              State.dx = -1.0; State.dy = -0.5; State.dz = 0.0;

		//toggle_dbg(DBG_PB5);
		RunController();        // run position controller at 100 Hz
		CheckController();
		//toggle_dbg(DBG_PB5);
		controllerCnt = 0;
	}
	controllerCnt++;
        //toggle_dbg(DBG_PB5);
        Controller_Update();    // run attitude controller at 500 Hz
        //toggle_dbg(DBG_PB5);

        // The CPU acknowledges the interrupt.
        EDIS;
}

//*****************************************************************************
// Function to Indicate an Error has Occurred (Invalid Command Received).
//*****************************************************************************
void
Error(void)
{
        // An error has occurred (invalid command received). Loop forever.
        for (;;)
        {
        }
}

//*****************************************************************************
// MtoC INT2 Interrupt Handler - Handles Data Word Reads/Writes
//*****************************************************************************
float32 zMeas = 0.0;
float32 zMeas_old = 0.0;
float32 dzMeas = 0.0;
float32 dzMeas_old = 0.0;
float32 qwtmp,qxtmp,qytmp,qztmp;
float32 yawVicon = 0.0;
interrupt void
MtoCIPC2IntHandler (void)
{
        Uint8 id = pucMtoCMsgRam[0];
        switch (id)
        {
        case PACKETID_IMU:
                //toggle_dbg(DBG_PB5);
                memcpy( (void*)&IMU.gyroX, pucMtoCMsgRam+1, sizeof(tIMUPacket));
                AHRS_GyroProp();
                //toggle_dbg(DBG_PB5);
                break;

        case PACKETID_POSVEL:
                memcpy( (void*)&PosVelPacket.x, pucMtoCMsgRam+1, sizeof(tPosVelPacket));

                // tmp -- DEBUG with vicon data
                State.x = PosVelPacket.x / POS_SCALE;
                State.y = PosVelPacket.y / POS_SCALE;
                State.z = PosVelPacket.z / POS_SCALE;
                State.dx = PosVelPacket.dx / VEL_SCALE;
                State.dy = PosVelPacket.dy / VEL_SCALE;
                State.dz = PosVelPacket.dz / VEL_SCALE;

                //if( SensorCal.biasCount >= SensorCal.biasTotal)
                //      loop.MeasurementProp_posvel = 1;
                break;

        case PACKETID_POS:
                memcpy( (void*)&PosPacket.x, pucMtoCMsgRam+1, sizeof(tPosPacket));
                if( SensorCal.biasCount >= SensorCal.biasTotal){
#ifdef USE_SEPARATE_Z_MEASUREMENT
                        loop.MeasurementProp_xy = 1;
                        if (cnttmpmark % 10 == 0)
                                loop.MeasurementProp_z = 1;
                        cnttmpmark++;
#else
                        loop.MeasurementProp_pos = 1;
#endif
                }
                break;


        case PACKETID_ALT:
                memcpy( (void*)&PosPacket.z, pucMtoCMsgRam+1, sizeof(tAltPacket));

                // DBG!!!
//                State.z = (float32) PosPacket.z / POS_SCALE;
#ifdef USE_VELOCITY_MEASUREMENT
                zMeas = (float32) PosPacket.z / POS_SCALE;
                dzMeas = (zMeas - zMeas_old) / 0.1; // 10 Hz
                dzMeas = dzMeas_old + 0.9 * (dzMeas - dzMeas_old);
                zMeas_old = zMeas;
                dzMeas_old = dzMeas;

                PosVelPacket.z = (int32_t) (zMeas * POS_SCALE);
                PosVelPacket.dz = (int32_t) (dzMeas * VEL_SCALE);

                // DEBUG!!!
//                State.z = zMeas;
//                State.dz = dzMeas;
#endif

                if( SensorCal.biasCount >= SensorCal.biasTotal){
#ifdef USE_SEPARATE_Z_MEASUREMENT
                        loop.MeasurementProp_z = 1;
#elif defined USE_VELOCITY_MEASUREMENT
//                        loop.MeasurementProp_posvel = 1;
#else
                        loop.MeasurementProp_pos = 1;
#endif
                }
                break;	

        case PACKETID_ATT_CMD:
                memcpy( (void*)&AttCmdPacket.p_cmd, pucMtoCMsgRam+1, sizeof(tAttCmdPacket));
                // convert to attitudeCmd packet
                AttitudeCmd.p = (float32) AttCmdPacket.p_cmd / ROT_SCALE;
                AttitudeCmd.q = (float32) AttCmdPacket.q_cmd / ROT_SCALE;
                AttitudeCmd.r = (float32) AttCmdPacket.r_cmd / ROT_SCALE;
                AttitudeCmd.q_cmd.o = (float32) AttCmdPacket.qo_cmd / QUAT_SCALE;
                AttitudeCmd.q_cmd.x = (float32) AttCmdPacket.qx_cmd / QUAT_SCALE;
                AttitudeCmd.q_cmd.y = (float32) AttCmdPacket.qy_cmd / QUAT_SCALE;
                AttitudeCmd.q_cmd.z = (float32) AttCmdPacket.qz_cmd / QUAT_SCALE;
#ifdef USE_VICON_ATTITUDE
                AHRSdata.q_meas.o = (float32) AttCmdPacket.qo_meas / QUAT_SCALE;
                AHRSdata.q_meas.x = (float32) AttCmdPacket.qx_meas / QUAT_SCALE;
                AHRSdata.q_meas.y = (float32) AttCmdPacket.qy_meas / QUAT_SCALE;
                AHRSdata.q_meas.z = (float32) AttCmdPacket.qz_meas / QUAT_SCALE;
#endif
                AttitudeCmd.throttle = (float32) AttCmdPacket.throttle / QUAT_SCALE;
                AttitudeCmd.AttCmd = AttCmdPacket.AttCmd;
                break;

        case PACKETID_ATT_Z_CMD:
                memcpy( (void*)&AttZCmdPacket.p_cmd, pucMtoCMsgRam+1, sizeof(tAttZCmdPacket));
                // convert to attitudeCmd packet
                AttitudeCmd.p = (float32) AttZCmdPacket.p_cmd / ROT_SCALE;
                AttitudeCmd.q = (float32) AttZCmdPacket.q_cmd / ROT_SCALE;
                AttitudeCmd.r = (float32) AttZCmdPacket.r_cmd / ROT_SCALE;
                AttitudeCmd.q_cmd.o = (float32) AttZCmdPacket.qo_cmd / QUAT_SCALE;
                AttitudeCmd.q_cmd.x = (float32) AttZCmdPacket.qx_cmd / QUAT_SCALE;
                AttitudeCmd.q_cmd.y = (float32) AttZCmdPacket.qy_cmd / QUAT_SCALE;
                AttitudeCmd.q_cmd.z = (float32) AttZCmdPacket.qz_cmd / QUAT_SCALE;
                AttitudeCmd.AttCmd = (uint8_t) AttZCmdPacket.AttCmd;
                Goal.z = (float32) AttZCmdPacket.z / ROT_SCALE;
                break;

        case PACKETID_POS_YAW_CMD:
                memcpy( (void*)&PosYawCmdPacket.x_cmd, pucMtoCMsgRam+1, sizeof(tPosYawCmdPacket));
                Goal.x = (float32) PosYawCmdPacket.x_cmd / ROT_SCALE;
                Goal.y = (float32) PosYawCmdPacket.y_cmd / ROT_SCALE;
                Goal.z = (float32) PosYawCmdPacket.z_cmd / ROT_SCALE;
                Goal.dx = (float32) PosYawCmdPacket.dx_cmd / ROT_SCALE;
                Goal.dy = (float32) PosYawCmdPacket.dy_cmd / ROT_SCALE;
                Goal.dz = (float32) PosYawCmdPacket.dz_cmd / ROT_SCALE;
                Goal.yaw = (float32) PosYawCmdPacket.yaw / ROT_SCALE;
                AttitudeCmd.AttCmd = (uint8_t) PosYawCmdPacket.AttCmd;
                break;
        case PACKETID_WII_IPC:
                memcpy( (void*)&WiiPacket.x[0], pucMtoCMsgRam+1, sizeof(tWiiIPCPacket));
                if( SensorCal.biasCount >= SensorCal.biasTotal){
                        loop.WiiProp = 1;
                }
                break;
        case PACKETID_SENSORCAL:
                memcpy( (void*)&SensorCalPacket.gyroXScale, pucMtoCMsgRam+1, sizeof(tSensorCalPacket));
                SensorCal.gyroXScale = (float32) SensorCalPacket.gyroXScale / QUAT_SCALE;
                SensorCal.gyroYScale = (float32) SensorCalPacket.gyroYScale / QUAT_SCALE;
                SensorCal.gyroZScale = (float32) SensorCalPacket.gyroZScale / QUAT_SCALE;
                SensorCal.axScale = (float32) SensorCalPacket.accelXScale / QUAT_SCALE;
                SensorCal.ayScale = (float32) SensorCalPacket.accelYScale / QUAT_SCALE;
                SensorCal.azScale = (float32) SensorCalPacket.accelZScale / QUAT_SCALE;
                SensorCal.axZero = (float32) SensorCalPacket.accelXZero / QUAT_SCALE;
                SensorCal.ayZero = (float32) SensorCalPacket.accelYZero / QUAT_SCALE;
                SensorCal.azZero = (float32) SensorCalPacket.accelZZero / QUAT_SCALE;
                SensorCal.magXZero = (float32) SensorCalPacket.magXZero / ROT_SCALE;
                SensorCal.magYZero = (float32) SensorCalPacket.magYZero / ROT_SCALE;
                SensorCal.magZZero = (float32) SensorCalPacket.magZZero / ROT_SCALE;
                SensorCal.magComp00 = (float32) SensorCalPacket.magComp00 / QUAT_SCALE;
                SensorCal.magComp01 = (float32) SensorCalPacket.magComp01 / QUAT_SCALE;
                SensorCal.magComp02 = (float32) SensorCalPacket.magComp02 / QUAT_SCALE;
                SensorCal.magComp10 = (float32) SensorCalPacket.magComp10 / QUAT_SCALE;
                SensorCal.magComp11 = (float32) SensorCalPacket.magComp11 / QUAT_SCALE;
                SensorCal.magComp12 = (float32) SensorCalPacket.magComp12 / QUAT_SCALE;
                SensorCal.magComp20 = (float32) SensorCalPacket.magComp20 / QUAT_SCALE;
                SensorCal.magComp21 = (float32) SensorCalPacket.magComp21 / QUAT_SCALE;
                SensorCal.magComp22 = (float32) SensorCalPacket.magComp22 / QUAT_SCALE;
                SensorCal.kGyroBias = (float32) SensorCalPacket.kGyroBias / QUAT_SCALE;
                SensorCal.kAttFilter = (float32) SensorCalPacket.kAttFilter / QUAT_SCALE;
                break;
        case PACKETID_MAG:
                memcpy( (void*)&MagPacket.magx, pucMtoCMsgRam+1, sizeof(tMagPacket));
                break;

        case PACKETID_VICON_STATE:
        		memcpy( (void*)&ViconStatePacket.x, pucMtoCMsgRam+1, sizeof(tViconStatePacket));
//                State.x = (float32) ViconStatePacket.x / ROT_SCALE;
//                State.y = (float32) ViconStatePacket.y / ROT_SCALE;
//                State.z = (float32) ViconStatePacket.z / ROT_SCALE;
//                State.dx = (float32) ViconStatePacket.dx / ROT_SCALE;
//                State.dy = (float32) ViconStatePacket.dy / ROT_SCALE;
//                State.dz = (float32) ViconStatePacket.dz / ROT_SCALE;
                AHRSdata.q_meas.o = (float32) ViconStatePacket.qw / QUAT_SCALE;
				AHRSdata.q_meas.x = (float32) ViconStatePacket.qx / QUAT_SCALE;
				AHRSdata.q_meas.y = (float32) ViconStatePacket.qy / QUAT_SCALE;
				AHRSdata.q_meas.z = (float32) ViconStatePacket.qz / QUAT_SCALE;

//        		pull out yaw
//                qwtmp = (float32) ViconStatePacket.qw / QUAT_SCALE;
//                qxtmp = (float32) ViconStatePacket.qx / QUAT_SCALE;
//                qytmp = (float32) ViconStatePacket.qy / QUAT_SCALE;
//                qztmp = (float32) ViconStatePacket.qz / QUAT_SCALE;
//                yawVicon = atan2( 2.0*(qwtmp*qztmp + qxtmp*qytmp),
//                		1.0 - 2.0*(qytmp*qytmp + qztmp*qztmp));
//                AHRSdata.q_meas.o = cos(yawVicon/2.0);
//                AHRSdata.q_meas.x = 0.0;
//                AHRSdata.q_meas.y = 0.0;
//                AHRSdata.q_meas.z = sin(yawVicon/2.0);

//				PosVelPacket.x = (int32_t) (ViconStatePacket.x * (POS_SCALE / ROT_SCALE) );
//				PosVelPacket.y = (int32_t) (ViconStatePacket.y * (POS_SCALE / ROT_SCALE) );
//				PosVelPacket.z = (int32_t) (ViconStatePacket.z * (POS_SCALE / ROT_SCALE) );
//				PosVelPacket.dx = (int32_t) (ViconStatePacket.dx * (VEL_SCALE / ROT_SCALE) );
//				PosVelPacket.dy = (int32_t) (ViconStatePacket.dy * (VEL_SCALE / ROT_SCALE) );
//				PosVelPacket.dz = (int32_t) (ViconStatePacket.dz * (VEL_SCALE / ROT_SCALE) );

//				toggle_dbg(DBG_PB5);
//                if( SensorCal.biasCount >= SensorCal.biasTotal)
//                      loop.MeasurementProp_posvel = 1;
				break;
        default:
                ErrorFlag = 1;
                break;
        }

        IPCMtoCFlagAcknowledge(IPC_FLAG2); // acknowledge the data from m3
        PieCtrlRegs.PIEACK.all = PIEACK_GROUP11;   // Acknowledge interrupt to PIE
}

/*---------------------------------------------------------------------
  Function Name: CtoMIPCBlockWrite
  Description:   Send block of data to M3
  Inputs:        id - unique data packet id shared between cores
  data - pointer to data
  size - size of data to send
  blocking - enable blocking to force the core to wait for the other core to process previous data
  Returns:       None
  -----------------------------------------------------------------------*/
void CtoMIPCBlockWrite(unsigned char id, void * data, unsigned int size, unsigned int blocking)
{

        if (IPCCtoMFlagBusy(IPC_FLAG3)) // using flag 2 -- could be using 1,2,3, or 4 to generate interrupts
        {
                // C28 hasn't finished processing
                if (blocking)
                {
                        while (IPCCtoMFlagBusy(IPC_FLAG3))
                        {}
                } else
                {
                        return; // todo: something more intelligent here?
                }
        }

        pucCtoMMsgRam[0] = id;  // Packet ID header
        memcpy(pucCtoMMsgRam+1, data, size);  // on C28 all data types are at least 16 bits

        IPCCtoMFlagSet(IPC_FLAG3);      // signal M3 that data is ready

}


interrupt void  ADC2IntHandler(void)
{
        Uint16 numAvg = 10;
        Uint16 newVoltage = Adc2Result.ADCRESULT0;
        SumVoltage -= Voltage[ConversionCount]; // subtract oldest reading
        SumVoltage += newVoltage;                               // add new reading
        Voltage[ConversionCount] = newVoltage;  // fill buffer with new reading

        AvgVoltage = SumVoltage/numAvg*VSCALE + VBIAS;

        // If numAvg conversions have been logged, start over
        if(ConversionCount >= numAvg-1)
        {
                ConversionCount = 0;
        }
        else ConversionCount++;


        Adc2Regs.ADCINTFLGCLR.bit.ADCINT1 = 1;  //Clear ADCINT1 flag reinitialize
        // for next SOC
        PieCtrlRegs.PIEACK.all = PIEACK_GROUP1; // Acknowledge interrupt to PIE

        return;
}

/*---------------------------------------------------------------------
  Function Name: InitFilter
  Description:   Initialize Filter
  Inputs:        None
  Returns:       None
  -----------------------------------------------------------------------*/
void InitFilter(void)
{
        /* Filter Initialization */
        float32 process_noise = 1.0;
        float32 measurement_noise = 0.001;

        VEC * x0 = v_get(9);
        x0->ve[0] = 0.0;        // xpos
        x0->ve[1] = 0.0;        // ypos
        x0->ve[2] = 0.0;        // zpos
        x0->ve[3] = 0.0;        // dx
        x0->ve[4] = 0.0;        // dy
        x0->ve[5] = 0.0;        // dz
        x0->ve[6] = 0.0;        // ax_bias
        x0->ve[7] = 0.0;        // ay_bias
        x0->ve[8] = 0.0;        // az_bias
        VEC * Rx = v_get(9);
        Rx->ve[0] = 0.1;
        Rx->ve[1] = 0.1;
        Rx->ve[2] = 0.1;
        Rx->ve[3] = 0.1;
        Rx->ve[4] = 0.1;
        Rx->ve[5] = 0.1;
        Rx->ve[6] = 0.1;
        Rx->ve[7] = 0.1;
        Rx->ve[8] = 0.1;
        VEC * Rv = v_get(3);
        Rv->ve[0] = process_noise;
        Rv->ve[1] = process_noise;
        Rv->ve[2] = process_noise;

#ifdef USE_VELOCITY_MEASUREMENT
        Sn = v_get(6);
        Sn->ve[0] = sqrt(measurement_noise);
        Sn->ve[1] = sqrt(measurement_noise);
        Sn->ve[2] = sqrt(measurement_noise);
        Sn->ve[3] = sqrt(measurement_noise);
        Sn->ve[4] = sqrt(measurement_noise);
        Sn->ve[5] = sqrt(measurement_noise);
#elif defined USE_SEPARATE_Z_MEASUREMENT
        Sn_xy = v_get(2);
        Sn_z = v_get(1);
        Sn_xy->ve[0] = sqrt(measurement_noise);
        Sn_xy->ve[1] = sqrt(measurement_noise);
        Sn_z->ve[0] = sqrt(measurement_noise);
#else
        Sn = v_get(3);
        Sn->ve[0] = sqrt(measurement_noise);
        Sn->ve[1] = sqrt(measurement_noise);
        Sn->ve[2] = sqrt(measurement_noise);
#endif

        VEC * RWvar = NULL;
        float32 h_width = sqrt(3);



        // initialize
        SRCDKF_initialize(x0, Rx, Rv, RWvar, h_width);

        u = NULL;
        u = v_resize(u,7);
        u->ve[0] = 0.0; // ax
        u->ve[1] = 0.0; // ay
        u->ve[2] = 9.81;        // az
        u->ve[3] = 1.0; // qw
        u->ve[4] = 0.0; // qx
        u->ve[5] = 0.0; // qy
        u->ve[6] = 0.0; // qz

#ifdef USE_VELOCITY_MEASUREMENT
        Y = v_get(6);
        Y->ve[0] = 0.0; // x
        Y->ve[1] = 0.0; // y
        Y->ve[2] = 0.0; // z
        Y->ve[3] = 0.0; // dx
        Y->ve[4] = 0.0; // dy
        Y->ve[5] = 0.0; // dz
#elif defined USE_SEPARATE_Z_MEASUREMENT
        Y_xy = v_get(2);
        Y_xy->ve[0] = 0.0;      // x
        Y_xy->ve[1] = 0.0;      // y
        Y_z = v_get(1);
        Y_z->ve[0];
#else
        Y = v_get(3);
        Y->ve[0] = 0.0; // x
        Y->ve[1] = 0.0; // y
        Y->ve[2] = 0.0; // z
#endif

}

