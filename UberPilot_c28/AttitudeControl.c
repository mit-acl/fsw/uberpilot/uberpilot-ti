#include "defines_c28.h"

/****************************************
Run the attitude controller at 500Hz
****************************************/

extern volatile tAHRSdata AHRSdata;
extern volatile tAttitudeCmd AttitudeCmd;
extern volatile tGains Gains;
extern volatile tMotorData MotorData;
extern volatile tGoal Goal;
extern volatile tState State;

/*---------------------------------------------------------------------
  Function Name: Controller_Update
  Description:   Run the attitude controller, should execute at 500Hz
					Set motor values
  Inputs:        None
  Returns:       None
-----------------------------------------------------------------------*/
int8_t healthcnt = 1;
void Controller_Update(void)
{
    // Quaternion attitude error
    tQuaternion qerror = qprod(qconj(AHRSdata.q_est), AttitudeCmd.q_cmd);

    // old gains must be multiplied by 2*255 for i2c escs and 3125 for pwm escs

    //DEBUG
    //attitudeCmd.AttCmd = 1;
    //attitudeCmd.throttle = 50;

    // New Quaternion Controller from Frazzoli
    float32 pErr = AHRSdata.p - AttitudeCmd.p;
    float32 qErr = AHRSdata.q - AttitudeCmd.q;
    float32 rErr = AHRSdata.r - AttitudeCmd.r;
    float32 rollCmd = Gains.Kp_roll*qerror.x;
    float32 pitchCmd = Gains.Kp_pitch*qerror.y;
    float32 yawCmd = Gains.Kp_yaw*qerror.z;
    if (qerror.o < 0.0){
        rollCmd = -rollCmd;
        pitchCmd = -pitchCmd;
        yawCmd = -yawCmd;
    }

    // Increment Integrators if motor commands are being sent
    if (AttitudeCmd.AttCmd != NOT_FLYING) {
        Gains.IntRoll += rollCmd*Gains.AttitudeDT;
        Gains.IntPitch += pitchCmd*Gains.AttitudeDT;
        Gains.IntYaw += yawCmd*Gains.AttitudeDT;
    }

    rollCmd += Gains.Ki_roll*Gains.IntRoll - Gains.Kd_roll*pErr;
    pitchCmd += Gains.Ki_pitch*Gains.IntPitch - Gains.Kd_pitch*qErr;
    yawCmd += Gains.Ki_yaw*Gains.IntYaw - Gains.Kd_yaw*rErr;
    // End New Quaternion controller

    // Form motor commands
    float32 m1 = -pitchCmd - yawCmd;
    float32 m2 = -rollCmd  + yawCmd;
    float32 m3 =  pitchCmd - yawCmd;
    float32 m4 =  rollCmd  + yawCmd;

    if (AttitudeCmd.AttCmd == NOT_FLYING) {

      	// set motor values
        SETPWM(PWM1,0);
        SETPWM(PWM2,0);
        SETPWM(PWM3,0);
        SETPWM(PWM4,0);

    } else {
    	//GpioG1DataRegs.GPADAT.bit.GPIO13 = 1;
    	/**** PWM motors ****/
		float32 motorCmd = 0;
		motorCmd = m1 + AttitudeCmd.throttle;
		//satvoid(&motorCmd, 1.0, 0.0);
		satvoid(&motorCmd, 0.4, 0.0);  // TODO: find lower saturation bound here so motors don't turn off
		SETPWM(PWM1,motorCmd);

		motorCmd = m2 + AttitudeCmd.throttle;
//		satvoid(&motorCmd, 1.0, 0.0);     // TODO: find lower saturation bound here so motors don't turn off
		satvoid(&motorCmd, 0.4, 0.0);
		SETPWM(PWM2,motorCmd);

		motorCmd = m3 + AttitudeCmd.throttle;
//		satvoid(&motorCmd, 1.0, 0.0);     // TODO: find lower saturation bound here so motors don't turn off
		satvoid(&motorCmd, 0.4, 0.0);
		SETPWM(PWM3,motorCmd);

		motorCmd = m4 + AttitudeCmd.throttle;
//		satvoid(&motorCmd, 1.0, 0.0);     // TODO: find lower saturation bound here so motors don't turn off
		satvoid(&motorCmd, 0.4, 0.0);
		SETPWM(PWM4,motorCmd);
		//GpioG1DataRegs.GPADAT.bit.GPIO13 = 0;
    }

//    // tmp debug
//    SETPWM(PWM1,0.7);
//    SETPWM(PWM2,0.7);
//    SETPWM(PWM3,0.7);
//    SETPWM(PWM4,0.7);

    /* read health information */
    /* switch(healthcnt){ */
    /*     case 1: */
    /*         I2C2_ReadData(MOTOR1_READ, 3); */
    /*         break; */
    /*     case 2: */
    /*         I2C2_ReadData(MOTOR2_READ, 3); */
    /*         break; */
    /*     case 3: */
    /*         I2C2_ReadData(MOTOR3_READ, 3); */
    /*         break; */
    /*     case 4: */
    /*         I2C2_ReadData(MOTOR4_READ, 3); */
    /*         healthcnt = 0; */
    /*         break; */
    /* } */
    /* healthcnt++; */
}

/*---------------------------------------------------------------------
  Function Name: Controller_Init
  Description:   Initialize all the controller variables
  Inputs:        None
  Returns:       None
-----------------------------------------------------------------------*/
void Controller_Init(void)
{
    // ESCs
    SETPWM(PWM1,0);
    SETPWM(PWM2,0);
    SETPWM(PWM3,0);
    SETPWM(PWM4,0);

    AttitudeCmd.q_cmd.o = 1.0;
    AttitudeCmd.q_cmd.x = 0.0;
    AttitudeCmd.q_cmd.y = 0.0;
    AttitudeCmd.q_cmd.z = 0.0;
    AttitudeCmd.qmin_old.o = 1.0;
	AttitudeCmd.qmin_old.x = 0.0;
	AttitudeCmd.qmin_old.y = 0.0;
	AttitudeCmd.qmin_old.z = 0.0;
    AttitudeCmd.p = 0.0;
    AttitudeCmd.q = 0.0;
    AttitudeCmd.r = 0.0;
    AttitudeCmd.throttle = 0.0;
    AttitudeCmd.AttCmd = 0;

	// mQxx default values
    Gains.Kp_roll = 0.096;
    Gains.Ki_roll = 0.0;
    Gains.Kd_roll = 0.032;

    Gains.Kp_pitch = 0.096;
    Gains.Ki_pitch = 0.0;
    Gains.Kd_pitch = 0.032;

    Gains.Kp_yaw = 0.096;
    Gains.Ki_yaw = 0.002;
    Gains.Kd_yaw = 0.032;

    Gains.maxang = 0.8;
    Gains.lowBatt = 10800; // 10.8 volts
    Gains.stream_data = 0;

    Gains.IntRoll = 0.0;
    Gains.IntPitch = 0.0;
    Gains.IntYaw = 0.0;
    Gains.AttitudeDT = ATTITUDE_CONTROL_DT;
    Gains.PositionDT = POSITION_CONTROL_DT;

    Gains.I_x = Gains.I_y = Gains.I_z = 0.0;
    Gains.maxPosErr_xy = 2.0;
    Gains.maxPosErr_z = 1.0;
    Gains.maxVelErr_xy = 3.0;
    Gains.maxVelErr_z = 3.0;

    Gains.Kp_xy = 2.0;
    Gains.Ki_xy = 0.3;
    Gains.Kd_xy = 0.9;

    Gains.Kp_z = 1.5;
    Gains.Ki_z = 0.5;
    Gains.Kd_z = 1.0;

    Gains.I_xy_int_thresh = 1.0;
    Gains.I_z_int_thresh = 1.0;

    Gains.a = 0.0;
    Gains.b = 0.025;//0.1334;
    Gains.c = 0.1961;

    Gains.rmag = 0.0;
    Gains.minRmag = 0.1;
    Gains.minRmagint = 0.7;


    Goal.x = Goal.y = Goal.z = 0.0;
    Goal.dx = Goal.dy = Goal.dz = 0.0;
    Goal.d2x = Goal.d2y = Goal.d2z = 0.0;
    Goal.d2x_fb = Goal.d2y_fb = Goal.d2z_fb = 0.0;
    Goal.d2x_fb_old = Goal.d2y_fb_old = Goal.d2z_fb_old = 0.0;
    Goal.d3x = Goal.d3y = Goal.d3z = 0.0;
    Goal.d3x_fb = Goal.d3y_fb = Goal.d3z_fb = 0.0;
	Goal.d3x_fb_old = Goal.d3y_fb_old = Goal.d3z_fb_old = 0.0;

	Goal.f_total = 0.0;
	Goal.yaw = 0.0;
	Goal.qyaw_des.o = Goal.qyaw_des.x = Goal.qyaw_des.y = Goal.qyaw_des.z = 0.0;
	Goal.useFeedback = 1;

	State.x = State.y = State.z = 0.0;
	State.dx = State.dy = State.dz = 0.0;
}
//======================================================================
