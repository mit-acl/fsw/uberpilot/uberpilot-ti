/*
 * setup_c28.c
 *
 *  Created on: Nov 6, 2012
 *      Author: mark
 */

#include "defines_c28.h"

extern Uint16 LoopCount;
extern Uint16 ConversionCount;
extern VEC * u;
extern VEC * Y;
extern VEC * Sn;

// Functions that will be run from RAM need to be assigned to
// a different section.  This section will then be mapped to a load and
// run address using the linker cmd file.

#pragma CODE_SECTION(InitFlash, "ramfuncs");
#pragma CODE_SECTION(SetupFlash, "ramfuncs");
#pragma CODE_SECTION(FlashGainPump,"ramfuncs");
#pragma CODE_SECTION(FlashLeavePump,"ramfuncs");

/*---------------------------------------------------------------------
  Function Name: InitPWM
  Description:   Initialize 8 PWM Channels
  Inputs:        None
  Returns:       None
-----------------------------------------------------------------------*/
void InitPWM(void)
{
	// PWM 1 - EPwm1A,  PWM 3 - EPwm1B
    EPwm1Regs.TBPHS.half.TBPHS = 0; // Set Phase register to zero
    EPwm1Regs.TBCTR = 0; // clear TB counter
    EPwm1Regs.TBCTL.bit.CTRMODE = TB_COUNT_UP;
    EPwm1Regs.TBCTL.bit.PHSEN = TB_DISABLE; // Phase loading disabled
    EPwm1Regs.TBCTL.bit.PRDLD = TB_SHADOW;
    EPwm1Regs.TBCTL.bit.SYNCOSEL = TB_SYNC_DISABLE;
    EPwm1Regs.TBCTL.bit.HSPCLKDIV = 0x05; // 10 prescaler
    EPwm1Regs.TBCTL.bit.CLKDIV = TB_DIV1; // 1 prescaler  TBCLK = 150 Mz/(10 x 1) = 15 MHz
    EPwm1Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
    EPwm1Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
    EPwm1Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO; // load on CTR = Zero
    EPwm1Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO; // load on CTR = Zero
    EPwm1Regs.AQCTLA.bit.ZRO = AQ_SET;
    EPwm1Regs.AQCTLA.bit.CAU = AQ_CLEAR;
    EPwm1Regs.AQCTLB.bit.ZRO = AQ_SET;
    EPwm1Regs.AQCTLB.bit.CBU = AQ_CLEAR;
    EPwm1Regs.TBPRD = 31249; // Period = 31250 TBCLK counts  , 15 MHz Hz / 31250 = 480 Hz

	// PWM 3 - EPwm2A,  PWM 4 - EPwm2B
	EPwm2Regs.TBPHS.half.TBPHS = 0; // Set Phase register to zero
	EPwm2Regs.TBCTR = 0; // clear TB counter
	EPwm2Regs.TBCTL.bit.CTRMODE = TB_COUNT_UP;
	EPwm2Regs.TBCTL.bit.PHSEN = TB_DISABLE; // Phase loading disabled
	EPwm2Regs.TBCTL.bit.PRDLD = TB_SHADOW;
	EPwm2Regs.TBCTL.bit.SYNCOSEL = TB_SYNC_DISABLE;
	EPwm2Regs.TBCTL.bit.HSPCLKDIV = 0x05; // 10 prescaler
	EPwm2Regs.TBCTL.bit.CLKDIV = TB_DIV1; // 1 prescaler  TBCLK = 150 Mz/(10 x 1) = 15 MHz
	EPwm2Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
	EPwm2Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
	EPwm2Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO; // load on CTR = Zero
	EPwm2Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO; // load on CTR = Zero
	EPwm2Regs.AQCTLA.bit.ZRO = AQ_SET;
	EPwm2Regs.AQCTLA.bit.CAU = AQ_CLEAR;
	EPwm2Regs.AQCTLB.bit.ZRO = AQ_SET;
	EPwm2Regs.AQCTLB.bit.CBU = AQ_CLEAR;
	EPwm2Regs.TBPRD = 31249; // Period = 31250 TBCLK counts  , 15 MHz Hz / 31250 = 480 Hz

	// PWM 5 - EPwm3A,  PWM 6 - EPwm3B
	/*EPwm3Regs.TBPHS.half.TBPHS = 0; // Set Phase register to zero
	EPwm3Regs.TBCTR = 0; // clear TB counter
	EPwm3Regs.TBCTL.bit.CTRMODE = TB_COUNT_UP;
	EPwm3Regs.TBCTL.bit.PHSEN = TB_DISABLE; // Phase loading disabled
	EPwm3Regs.TBCTL.bit.PRDLD = TB_SHADOW;
	EPwm3Regs.TBCTL.bit.SYNCOSEL = TB_SYNC_DISABLE;
	EPwm3Regs.TBCTL.bit.HSPCLKDIV = 0x05; // 10 prescaler
	EPwm3Regs.TBCTL.bit.CLKDIV = TB_DIV1; // 1 prescaler  TBCLK = 150 Mz/(10 x 1) = 15 MHz
	EPwm3Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
	EPwm3Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
	EPwm3Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO; // load on CTR = Zero
	EPwm3Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO; // load on CTR = Zero
	EPwm3Regs.AQCTLA.bit.ZRO = AQ_SET;
	EPwm3Regs.AQCTLA.bit.CAU = AQ_CLEAR;
	EPwm3Regs.AQCTLB.bit.ZRO = AQ_SET;
	EPwm3Regs.AQCTLB.bit.CBU = AQ_CLEAR;
	EPwm3Regs.TBPRD = 31249; // Period = 31250 TBCLK counts  , 15 MHz Hz / 31250 = 480 Hz

	// PWM 7 - EPwm4A,  PWM 8 - EPwm4B
	EPwm4Regs.TBPHS.half.TBPHS = 0; // Set Phase register to zero
	EPwm4Regs.TBCTR = 0; // clear TB counter
	EPwm4Regs.TBCTL.bit.CTRMODE = TB_COUNT_UP;
	EPwm4Regs.TBCTL.bit.PHSEN = TB_DISABLE; // Phase loading disabled
	EPwm4Regs.TBCTL.bit.PRDLD = TB_SHADOW;
	EPwm4Regs.TBCTL.bit.SYNCOSEL = TB_SYNC_DISABLE;
	EPwm4Regs.TBCTL.bit.HSPCLKDIV = 0x05; // 10 prescaler
	EPwm4Regs.TBCTL.bit.CLKDIV = TB_DIV1; // 1 prescaler  TBCLK = 150 Mz/(10 x 1) = 15 MHz
	EPwm4Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
	EPwm4Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
	EPwm4Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO; // load on CTR = Zero
	EPwm4Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO; // load on CTR = Zero
	EPwm4Regs.AQCTLA.bit.ZRO = AQ_SET;
	EPwm4Regs.AQCTLA.bit.CAU = AQ_CLEAR;
	EPwm4Regs.AQCTLB.bit.ZRO = AQ_SET;
	EPwm4Regs.AQCTLB.bit.CBU = AQ_CLEAR;
	EPwm4Regs.TBPRD = 31249;*/ // Period = 31250 TBCLK counts  , 15 MHz Hz / 31250 = 480 Hz

	// Set all PWM values to 1 ms
	SETPWM(PWM1,0);
	SETPWM(PWM2,0);
	SETPWM(PWM3,0);
	SETPWM(PWM4,0);
	//PWM1 = 15000;  //EPwm1Regs.CMPA.half.CMPA = 15000; // Compare A in TBCL counts  480 Hz * 31250 / 1000 Hz = 15000 (1ms)
	//PWM2 = 15000;  //EPwm1Regs.CMPB = 15000; // Compare B in TBCL counts   480 Hz * 31250 / 500 Hz = 30000 (2ms)
	//PWM3 = 15000;  //EPwm2Regs.CMPA.half.CMPA = 15000; // Compare A in TBCL counts  480 Hz * 31250 / 1000 Hz = 15000 (1ms)
	//PWM4 = 15000;  //EPwm2Regs.CMPB = 15000; // Compare B in TBCL counts   480 Hz * 31250 / 500 Hz = 30000 (2ms)
	//PWM5 = 15000;  //EPwm3Regs.CMPA.half.CMPA = 15000; // Compare A in TBCL counts  480 Hz * 31250 / 1000 Hz = 15000 (1ms)
	//PWM6 = 15000;  //EPwm3Regs.CMPB = 15000; // Compare B in TBCL counts   480 Hz * 31250 / 500 Hz = 30000 (2ms)
	//PWM7 = 15000;  //EPwm4Regs.CMPA.half.CMPA = 15000; // Compare A in TBCL counts  480 Hz * 31250 / 1000 Hz = 15000 (1ms)
	//PWM8 = 15000;  //EPwm4Regs.CMPB = 15000; // Compare B in TBCL counts   480 Hz * 31250 / 500 Hz = 30000 (2ms)
}


/*---------------------------------------------------------------------
  Function Name: InitSysCtrlC28
  Description:   Initialize Analog 2 Digital Converter for reading battery voltage
  Inputs:        None
  Returns:       None
-----------------------------------------------------------------------*/
void InitSysCtrlC28(void)
{
    // *IMPORTANT*
    // The Device_cal function MUST be called
    // for the ADC and oscillators to function according
    // to specification. The clocks to the ADC MUST be enabled before calling
    // this function. See the device data manual and/or the ADC Reference
    // Manual for more information.

    (**InitAnalogSystemClock)(ACLKDIV4);

    EALLOW;
    // Initialize the Analog Sub-System and set the clock divider to divide by 4
    while((**AnalogClockEnable)(AnalogConfig1,ADC1_ENABLE));    // Enable ADC 1
    while((**AnalogClockEnable)(AnalogConfig2,ADC2_ENABLE));    // Enable ADC 2

    // Reset both ADC in case the last reset was a debugger reset (which doesn't
    // reset the ADCs
    Adc1Regs.ADCCTL1.bit.RESET = 1;
    Adc2Regs.ADCCTL1.bit.RESET = 1;
    // Wait to ensure ADCs are out of reset before device cal is called
    asm(" nop");
    asm(" nop");


    // Calibrate the device for temperature
    (**Device_Cal)();

    while((**AnalogClockDisable)(AnalogConfig1,ADC1_ENABLE));   // Disable ADC1
    while((**AnalogClockDisable)(AnalogConfig2,ADC2_ENABLE));   // Disable ADC2
    EDIS;




	// LOSPCP prescale register settings, normally it will be set to default
	// values
	EALLOW;
	SysCtrlRegs.LOSPCP.all = 0x0002;
	EDIS;


	// Initialize the Analog Sub-System and set the clock divider to divide by 4
	(**InitAnalogSystemClock)(ACLKDIV4);

	EALLOW;
	while((**AnalogClockEnable)(AnalogConfig2,ADC2_ENABLE)); // Enable ADC 2


	// Peripheral clock enables set for the selected peripherals.
	// If you are not using a peripheral leave the clock off
	// to save on power.
	SysCtrlRegs.PCLKCR1.bit.EPWM1ENCLK = 1; // ePWM1
	SysCtrlRegs.PCLKCR1.bit.EPWM2ENCLK = 1; // ePWM2
	SysCtrlRegs.PCLKCR1.bit.EPWM3ENCLK = 1; // ePWM3

	SysCtrlRegs.PCLKCR3.bit.DMAENCLK = 1;   // DMA
	SysCtrlRegs.PCLKCR3.bit.CPUTIMER0ENCLK = 1; // Timer 0
	SysCtrlRegs.PCLKCR3.bit.CPUTIMER1ENCLK = 1; // Timer 1
	SysCtrlRegs.PCLKCR3.bit.CPUTIMER2ENCLK = 1; // Timer 2

	SysCtrlRegs.PCLKCR0.bit.I2CAENCLK = 1;   // I2C-A

	SysCtrlRegs.PCLKCR0.bit.HRPWMENCLK = 1; // HRPWM
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 1;  // Enable TBCLK within the ePWM
	EDIS;
}

/*---------------------------------------------------------------------
  Function Name: InitGPIOC28
  Description:   Initialize GPIO's
  Inputs:        None
  Returns:       None
-----------------------------------------------------------------------*/
void InitGPIOC28(void)
{
	// set up PWM pins
	InitEPwm1Gpio();
	InitEPwm2Gpio();
//	InitEPwm3Gpio();
//	InitEPwm4Gpio();

	// Configure PB5_GPIO13 as debugging pin
	GpioCtrlRegs.GPAMUX1.bit.GPIO13 = 0;   // Configure GPIO1 as GPIO
	EALLOW;
	GpioG1CtrlRegs.GPADIR.bit.GPIO13 = 1; // output
	EDIS;
	GpioG1DataRegs.GPADAT.bit.GPIO13 = 0; // low to start

	// set up gpio's for battery adc
    EALLOW;
    GpioG1CtrlRegs.GPADIR.bit.GPIO4      = 1;       //Set as output
	GpioG1CtrlRegs.GPADIR.bit.GPIO10      = 1;       //Set as output
	GpioG1CtrlRegs.GPAMUX1.bit.GPIO4       = 1;     //Set mux to EPWM3A
	GpioG1CtrlRegs.GPAMUX1.bit.GPIO10       = 3;     //Set mux to ADCSOCBn
    EDIS;
}

/*---------------------------------------------------------------------
  Function Name: InitTimer
  Description:   Initialize Timer for dt calculations and for control calculations
  Inputs:        None
  Returns:       None
-----------------------------------------------------------------------*/
void InitTimer(void)
{
	// CPU Timer 0 -- for dt calculations
	// Initialize address pointers to respective timer registers:
	CpuTimer0.RegsAddr = &CpuTimer0Regs;

	// Initialize timer period to maximum:
	CpuTimer0Regs.PRD.all  = 0xFFFFFFFF;

	// Initialize pre-scale counter to divide by 1 (SYSCLKOUT)
	CpuTimer0Regs.TPR.all  = 0;
	CpuTimer0Regs.TPRH.all = 0;

	// Initialize timer control register:
	CpuTimer0Regs.TCR.bit.TSS = 1;      // 1 = Stop timer, 0 = Start/Restart
										   // Timer
	CpuTimer0Regs.TCR.bit.TRB = 1;      // 1 = reload timer
	CpuTimer0Regs.TCR.bit.SOFT = 0;
	CpuTimer0Regs.TCR.bit.FREE = 0;     // Timer Free Run Disabled
	CpuTimer0Regs.TCR.bit.TIE = 0;      // 0 = Disable/ 1 = Enable Timer
										   // Interrupt

	// CPU Timer 1 -- for interrupt driven control calculations
	CpuTimer1.RegsAddr = &CpuTimer1Regs;

    // Initialize timer period to get 500 Hz rollover:
    CpuTimer1Regs.PRD.all  = 0x493E0; // 150 Mhz * x = 500 Hz -> x = 150,000,000/500 = 300,000

    // Initialize pre-scale counter to divide by 1 (SYSCLKOUT):
    CpuTimer1Regs.TPR.all  = 0;
    CpuTimer1Regs.TPRH.all = 0;

	// Initialize timer control register:
	CpuTimer1Regs.TCR.bit.TSS = 1;      // 1 = Stop timer, 0 = Start/Restart
										   // Timer
	CpuTimer1Regs.TCR.bit.TRB = 1;      // 1 = reload timer
	CpuTimer1Regs.TCR.bit.SOFT = 0;
	CpuTimer1Regs.TCR.bit.FREE = 0;     // Timer Free Run Disabled
	CpuTimer1Regs.TCR.bit.TIE = 1;      // 0 = Disable/ 1 = Enable Timer
										   // Interrupt

    // Reset interrupt counters:
    CpuTimer1.InterruptCount = 0;
}


/*---------------------------------------------------------------------
  Function Name: InitInterrupts
  Description:   Initialize interrupts
  Inputs:        None
  Returns:       None
-----------------------------------------------------------------------*/
void InitInterrupts(void){
	// Enable CPU INT1 which is connected to MTOCIPC INT1-4:
	IER |= M_INT13;	// Enable int13 for CPU1
	IER |= M_INT11;	// Enable int11 for MtoCIPC interrupts

	// Enable MTOCIPC INTn in the PIE: Group 11 interrupts
	PieCtrlRegs.PIEIER11.bit.INTx2 = 1;     // MTOCIPC INT2
	PieCtrlRegs.PIEIER1.bit.INTx1 = 1;  // Enable INT 1.1 in the PIE (ADC interrupt)
	IER |= M_INT1;                      // Enable CPU Interrupt 1 (ADC interrupt)

	// Enable global Interrupts and higher priority real-time debug events:
	EINT;       // Enable Global interrupt INTM
	ERTM;   // Enable Global realtime interrupt DBGM

}

/*---------------------------------------------------------------------
  Function Name: InitADC
  Description:   Initialize ADC
  Inputs:        None
  Returns:       None
-----------------------------------------------------------------------*/
void InitADC(void){

	LoopCount = 0;
	ConversionCount = 0;

	// Configure ADC
	EALLOW;
	Adc2Regs.ADCCTL2.bit.ADCNONOVERLAP = 1;     // Enable non-overlap mode i.e.
												// conversion and future
												// sampling
												// events dont overlap
	Adc2Regs.ADCCTL1.bit.INTPULSEPOS   = 1;     // ADCINT1 trips after
												// AdcResults latch
	Adc2Regs.INTSEL1N2.bit.INT1E       = 1;     // Enabled ADCINT1
	Adc2Regs.INTSEL1N2.bit.INT1CONT    = 0;     // Disable ADCINT1 Continuous
												// mode
	Adc2Regs.INTSEL1N2.bit.INT1SEL     = 0;     // setup EOC0 to trigger ADCINT1
												// to fire
	Adc2Regs.ADCSOC0CTL.bit.CHSEL      = 8;     // set SOC0 channel select to
												// ADC1B0
	//Adc2Regs.ADCSOC1CTL.bit.CHSEL      = 2;     // set SOC1 channel select to
												// ADC1A2
	AnalogSysctrlRegs.TRIG1SEL.all     = 12;     // Assigning EPWM3SOCB to
												// ADC TRIGGER 1 of the ADC module
	Adc2Regs.ADCSOC0CTL.bit.TRIGSEL    = 5;     // Set SOC0 start trigger to
												// ADC Trigger 1(EPWM3 SOCB) of the
												// adc
   // Adc2Regs.ADCSOC1CTL.bit.TRIGSEL    = 5;     // set SOC1 start trigger to
												// ADC Trigger 1(EPWM3 SOCB) of the
												// adc
	Adc2Regs.ADCSOC0CTL.bit.ACQPS      = 6;     // set SOC0 S/H Window to 7 ADC
												// Clock Cycles, (6 ACQPS plus
												// 1)
   // Adc2Regs.ADCSOC1CTL.bit.ACQPS      = 6;     // set SOC1 S/H Window to 7 ADC
												// Clock Cycles, (6 ACQPS plus
												// 1)
	EDIS;

//// Assumes ePWM1 clock is already enabled in InitSysCtrl();

	//Set event triggers (SOCA) for ADC SOC1
	EPwm3Regs.ETSEL.bit.SOCBEN         = 1;      // Enable SOC on B group
	EPwm3Regs.ETSEL.bit.SOCBSEL = ET_CTRU_CMPA;  // Select SOC from CMPA on
												 // upcount
	EPwm3Regs.ETPS.bit.SOCBPRD         = 3;      // Generate pulse on every 3rd
												 // event

	// Time-base registers
	EPwm3Regs.TBPRD = PERIOD;                   // Set timer period, PWM
												// frequency = 1 / period
	EPwm3Regs.TBPHS.all = 0;                    // Time-Base Phase Register
	EPwm3Regs.TBCTR = 0;                        // Time-Base Counter Register
	EPwm3Regs.TBCTL.bit.PRDLD = TB_IMMEDIATE;   // Set Immediate load
	EPwm3Regs.TBCTL.bit.CTRMODE = TB_COUNT_UP;  // Count-up mode: used for
												// asymmetric PWM
	EPwm3Regs.TBCTL.bit.PHSEN = TB_DISABLE;     // Disable phase loading
	EPwm3Regs.TBCTL.bit.SYNCOSEL = TB_SYNC_DISABLE;
	EPwm3Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;
	EPwm3Regs.TBCTL.bit.CLKDIV = 0x7;			// 128 clk prescaler

	// Setup shadow register load on ZERO

	EPwm3Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
	//EPwm3Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
	EPwm3Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO; // load on CTR=Zero
   // EPwm3Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO; // load on CTR=Zero

	// Set Compare values

	EPwm3Regs.CMPA.half.CMPA = DUTY_CYCLE_A;     // Set duty 50% initially
	//EPwm3Regs.CMPB = DUTY_CYCLE_B;               // Set duty 50% initially

	// Set actions

	EPwm3Regs.AQCTLA.bit.ZRO = AQ_SET;           // Set PWM2A on Zero
	EPwm3Regs.AQCTLA.bit.CAU = AQ_CLEAR;         // Clear PWM2A on event A, up
												 // count

	//EPwm3Regs.AQCTLB.bit.ZRO = AQ_CLEAR;         // Set PWM2B on Zero
	//EPwm3Regs.AQCTLB.bit.CBU = AQ_SET;           // Clear PWM2B on event B, up
												 // count
}
