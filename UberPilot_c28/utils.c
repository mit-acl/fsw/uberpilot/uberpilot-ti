/*
 * utils.c
 *
 *  Created on: Jan 22, 2013
 *      Author: mark
 */

#include "defines_c28.h"

void satvoid(float32 * val, float32 hi, float32 low){
	if (*val > hi){
		*val = hi;
	}
	else if (*val < low){
		*val  = low;
	}
}

float32 satreturn(float32 val, float32 hi, float32 low){
	if (val > hi)
	{
		val = hi;
	}
	else if (val < low)
	{
		val  = low;
	}

	return val;
}


