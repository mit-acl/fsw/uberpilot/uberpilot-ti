################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_headers/cmd/F28M35x_Headers_nonBIOS.cmd \
../F28M35x_UberPilot_C28_FLASH.cmd 

ASM_SRCS += \
C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_common/source/F28M35x_CodeStartBranch.asm \
C:/ti/controlSUITE/device_support/f28m35x/v160/F28M35x_common/source/F28M35x_usDelay.asm 

C_SRCS += \
../AHRS.c \
../AttitudeControl.c \
C:/ti/controlSUITE/device_support/f28m35x/v160/F28M35x_common/source/F28M35x_Adc.c \
C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_common/source/F28M35x_DefaultIsr.c \
C:/ti/controlSUITE/device_support/f28m35x/v160/F28M35x_common/source/F28M35x_EPwm.c \
C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_headers/source/F28M35x_GlobalVariableDefs.c \
C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_common/source/F28M35x_Ipc_Util.c \
C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_common/source/F28M35x_PieCtrl.c \
C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_common/source/F28M35x_PieVect.c \
C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_common/source/F28M35x_SysCtrl.c \
../PositionControl.c \
../main_c28.c \
../quaternion.c \
../setup_c28.c \
../utils.c \
../wii.c 

OBJS += \
./AHRS.obj \
./AttitudeControl.obj \
./F28M35x_Adc.obj \
./F28M35x_CodeStartBranch.obj \
./F28M35x_DefaultIsr.obj \
./F28M35x_EPwm.obj \
./F28M35x_GlobalVariableDefs.obj \
./F28M35x_Ipc_Util.obj \
./F28M35x_PieCtrl.obj \
./F28M35x_PieVect.obj \
./F28M35x_SysCtrl.obj \
./F28M35x_usDelay.obj \
./PositionControl.obj \
./main_c28.obj \
./quaternion.obj \
./setup_c28.obj \
./utils.obj \
./wii.obj 

ASM_DEPS += \
./F28M35x_CodeStartBranch.pp \
./F28M35x_usDelay.pp 

C_DEPS += \
./AHRS.pp \
./AttitudeControl.pp \
./F28M35x_Adc.pp \
./F28M35x_DefaultIsr.pp \
./F28M35x_EPwm.pp \
./F28M35x_GlobalVariableDefs.pp \
./F28M35x_Ipc_Util.pp \
./F28M35x_PieCtrl.pp \
./F28M35x_PieVect.pp \
./F28M35x_SysCtrl.pp \
./PositionControl.pp \
./main_c28.pp \
./quaternion.pp \
./setup_c28.pp \
./utils.pp \
./wii.pp 

C_DEPS__QUOTED += \
"AHRS.pp" \
"AttitudeControl.pp" \
"F28M35x_Adc.pp" \
"F28M35x_DefaultIsr.pp" \
"F28M35x_EPwm.pp" \
"F28M35x_GlobalVariableDefs.pp" \
"F28M35x_Ipc_Util.pp" \
"F28M35x_PieCtrl.pp" \
"F28M35x_PieVect.pp" \
"F28M35x_SysCtrl.pp" \
"PositionControl.pp" \
"main_c28.pp" \
"quaternion.pp" \
"setup_c28.pp" \
"utils.pp" \
"wii.pp" 

OBJS__QUOTED += \
"AHRS.obj" \
"AttitudeControl.obj" \
"F28M35x_Adc.obj" \
"F28M35x_CodeStartBranch.obj" \
"F28M35x_DefaultIsr.obj" \
"F28M35x_EPwm.obj" \
"F28M35x_GlobalVariableDefs.obj" \
"F28M35x_Ipc_Util.obj" \
"F28M35x_PieCtrl.obj" \
"F28M35x_PieVect.obj" \
"F28M35x_SysCtrl.obj" \
"F28M35x_usDelay.obj" \
"PositionControl.obj" \
"main_c28.obj" \
"quaternion.obj" \
"setup_c28.obj" \
"utils.obj" \
"wii.obj" 

ASM_DEPS__QUOTED += \
"F28M35x_CodeStartBranch.pp" \
"F28M35x_usDelay.pp" 

C_SRCS__QUOTED += \
"../AHRS.c" \
"../AttitudeControl.c" \
"C:/ti/controlSUITE/device_support/f28m35x/v160/F28M35x_common/source/F28M35x_Adc.c" \
"C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_common/source/F28M35x_DefaultIsr.c" \
"C:/ti/controlSUITE/device_support/f28m35x/v160/F28M35x_common/source/F28M35x_EPwm.c" \
"C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_headers/source/F28M35x_GlobalVariableDefs.c" \
"C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_common/source/F28M35x_Ipc_Util.c" \
"C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_common/source/F28M35x_PieCtrl.c" \
"C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_common/source/F28M35x_PieVect.c" \
"C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_common/source/F28M35x_SysCtrl.c" \
"../PositionControl.c" \
"../main_c28.c" \
"../quaternion.c" \
"../setup_c28.c" \
"../utils.c" \
"../wii.c" 

ASM_SRCS__QUOTED += \
"C:/ti/controlSUITE/device_support/f28m35x/v200/F28M35x_common/source/F28M35x_CodeStartBranch.asm" \
"C:/ti/controlSUITE/device_support/f28m35x/v160/F28M35x_common/source/F28M35x_usDelay.asm" 


