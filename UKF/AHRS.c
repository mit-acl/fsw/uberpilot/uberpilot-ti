/*
 * AHRS.c
 *
 *  Created on: Nov 19, 2012
 *      Author: mark
 */

#include "AHRS.h"

/* globals */
extern volatile tSensorCal SensorCal;
extern volatile tAHRSdata AHRSdata;
extern volatile tIMUPacket IMU;
extern volatile tLoopFlags loop;

#define ACCEL_FILT_ORDER	3
float32 axBuff[ACCEL_FILT_ORDER+1];
float32 axFilt[ACCEL_FILT_ORDER+1];
float32 ayBuff[ACCEL_FILT_ORDER+1];
float32 ayFilt[ACCEL_FILT_ORDER+1];
float32 azBuff[ACCEL_FILT_ORDER+1];
float32 azFilt[ACCEL_FILT_ORDER+1];

// low pass filter -- naive implementation
void lowPass(float32 * a, float32 * b, float32 * x, float32 * y, int N)
{
	y[N] = b[0]*x[N];

	int i;
	for (i = 1; i < N+1; i++)
	{
		float32 tmp1 = b[i]*x[N-i] - a[i]*y[N-i];
	  y[N] += tmp1;
	}
	// shift vectors back one
	for (i=0; i<N+1 ; i++)
	  {
	    x[i] = x[i+1];
	    y[i] = y[i+1];
	  }
}


// Initialize
void AHRS_init(void){

    // Setup calibration struct
    SensorCal.biasCount = 0;
    SensorCal.biasTotal = 2000;
    SensorCal.blankReads = 200;
    SensorCal.pBias = 0.0;
    SensorCal.qBias = 0.0;
    SensorCal.rBias = 0.0;

    // Initialize attitude
    AHRSdata.q_est.o = 1.0;
    AHRSdata.q_est.x = 0.0;
    AHRSdata.q_est.y = 0.0;
    AHRSdata.q_est.z = 0.0;
    AHRSdata.q_meas.o = 1.0;
    AHRSdata.q_meas.x = 0.0;
    AHRSdata.q_meas.y = 0.0;
    AHRSdata.q_meas.z = 0.0;

    // Parameters
    SensorCal.gyroScale = PI/14.375/180.0*1.0; // Default value, may be overwritten by SensorPacket
    SensorCal.gyroPropDT = 0.001; // nominal dt
    SensorCal.tprev = 0;

    SensorCal.accelScale = 0.003906; // at full resolution, sensitivity is 256 128 LSB/g
    SensorCal.acc_window_min = 0.5;
    SensorCal.acc_window_max = 1.5;

    SensorCal.K_AttFilter = 2*0.01*0.7; // Default value, may be overwritten by SensorPacket
    SensorCal.K_GyroBias = 10*0.01*0.05; // TODO: find this value
}

void AHRS_GyroProp(void){
    // $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    // Gyro scaling, to rad/sec
    // $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    AHRSdata.p = -IMU.gyroY*SensorCal.gyroScale;
    AHRSdata.q =  IMU.gyroX*SensorCal.gyroScale;
    AHRSdata.r =  IMU.gyroZ*SensorCal.gyroScale;

	// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
	// Accel scaling, to g
	// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
	AHRSdata.ax = IMU.accX*SensorCal.accelScale;
	AHRSdata.ay = IMU.accY*SensorCal.accelScale;
	AHRSdata.az = IMU.accZ*SensorCal.accelScale;

    // $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    // Initial gyro and accel bias calculation
    // $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    if( SensorCal.biasCount < SensorCal.biasTotal){
        // Do some blank reads to clear any garbage in the initial transient
        if(--SensorCal.blankReads > 0)
            return;

        SensorCal.pBias += AHRSdata.p;
        SensorCal.qBias += AHRSdata.q;
        SensorCal.rBias += AHRSdata.r;

        SensorCal.axBiasInit += AHRSdata.ax;
        SensorCal.ayBiasInit += AHRSdata.ay;
        SensorCal.azBiasInit += (AHRSdata.az - 1.0);

        if( ++SensorCal.biasCount == SensorCal.biasTotal ){
            float32 tmp = 1.0 / (float32)SensorCal.biasTotal;
            SensorCal.pBias = SensorCal.pBias*tmp;
            SensorCal.qBias = SensorCal.qBias*tmp;
            SensorCal.rBias = SensorCal.rBias*tmp;

            SensorCal.axBiasInit = SensorCal.axBiasInit*tmp;
            SensorCal.ayBiasInit = SensorCal.ayBiasInit*tmp;
            SensorCal.azBiasInit = SensorCal.azBiasInit*tmp;

			//todo finish bias cal, turn off led
        }

        return;
    }

    // $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    // Gyro bias correction
    // $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    AHRSdata.p -= SensorCal.pBias;
    AHRSdata.q -= SensorCal.qBias;
    AHRSdata.r -= SensorCal.rBias;

    // $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
	// Accel initial bias correction
	// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    AHRSdata.ax -= SensorCal.axBiasInit;
    AHRSdata.ay -= SensorCal.ayBiasInit;
    AHRSdata.az -= SensorCal.azBiasInit;

    // $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
	// low pass filter accelerometer data
	// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

//    static float32 filtb[ACCEL_FILT_ORDER+1] = {0.0277e-6, 0.1384e-6, 0.2769e-6, 0.2769e-6, 0.1384e-6, 0.0277e-6};//{0.0498e-3,    0.1493e-3,    0.1493e-3, 0.0498e-3};//{0.0009, 0.0019, 0.0009};
//    static float32 filta[ACCEL_FILT_ORDER+1] = {1.0, -4.7967,    9.2072,   -8.8404,    4.2458, -0.8160};//{1.0, -2.8492,  2.7096, -0.8600};//{1.0, -1.9112, 0.915};
    static float32 filtb[ACCEL_FILT_ORDER+1] = {0.2196e-3,    0.6588e-3,    0.6588e-3, 0.2196e-3};//{0.0009, 0.0019, 0.0009}; // filter parameters from matlab butterworth filter, 1000 sample freq, 10 cut off freq
    static float32 filta[ACCEL_FILT_ORDER+1] = {1.0, -2.7488,  2.5282, -0.7776};//{1.0, -1.9112, 0.915};

    // MATLAB Code
//    fs=1000;                    % Sample Freuency
//    fn=fs/2;                    % Nyquist Frequency
//    fc=10;                      % Desired Cutoff frequency (example)
//    [b,a]=butter(3,2*fc/fn)      % IIR Filter coefficients, b=num, a=denom


    axBuff[ACCEL_FILT_ORDER] = AHRSdata.ax;
    lowPass(filta, filtb, axBuff, axFilt, ACCEL_FILT_ORDER);
    AHRSdata.ax = axFilt[ACCEL_FILT_ORDER-1];

    ayBuff[ACCEL_FILT_ORDER] = AHRSdata.ay;
    lowPass(filta, filtb, ayBuff, ayFilt, ACCEL_FILT_ORDER);
    AHRSdata.ay = ayFilt[ACCEL_FILT_ORDER-1];

    azBuff[ACCEL_FILT_ORDER] = AHRSdata.az - 1.0;
    lowPass(filta, filtb, azBuff, azFilt, ACCEL_FILT_ORDER);
    AHRSdata.az = azFilt[ACCEL_FILT_ORDER-1] + 1.0;

//     axBuffSum += AHRSdata.ax - axBuff[axInd];
//     axBuff[axInd] = AHRSdata.ax;
//     axInd++;
//     if (axInd > ACCEL_BUFF_LENGTH-1)
//     	axInd = 0;
//     AHRSdata.ax = axBuffSum/ACCEL_BUFF_LENGTH;
//
//     ayBuffSum += AHRSdata.ay - ayBuff[ayInd];
//     ayBuff[ayInd] = AHRSdata.ay;
//     ayInd++;
//     if (ayInd > ACCEL_BUFF_LENGTH-1)
//     	ayInd = 0;
//     AHRSdata.ay = ayBuffSum/ACCEL_BUFF_LENGTH;
//
//     azBuffSum += AHRSdata.az - azBuff[azInd];
//     azBuff[azInd] = AHRSdata.az;
//     azInd++;
//     if (azInd > ACCEL_BUFF_LENGTH-1)
//     	azInd = 0;
//     AHRSdata.az = azBuffSum/ACCEL_BUFF_LENGTH;

    // $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    // Gyro propagation
    // $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    //getDT(&SensorCal.gyroPropDT,&SensorCal.tprev);
    SensorCal.gyroPropDT = IMUDT;
    SensorCal.gyroPropDT *= 0.5;
    AHRSdata.q_est.o -=  ( AHRSdata.q_est.x*AHRSdata.p +  AHRSdata.q_est.y*AHRSdata.q +  AHRSdata.q_est.z*AHRSdata.r ) * SensorCal.gyroPropDT;
    AHRSdata.q_est.x +=  ( AHRSdata.q_est.o*AHRSdata.p -  AHRSdata.q_est.z*AHRSdata.q +  AHRSdata.q_est.y*AHRSdata.r ) * SensorCal.gyroPropDT;
    AHRSdata.q_est.y +=  ( AHRSdata.q_est.z*AHRSdata.p +  AHRSdata.q_est.o*AHRSdata.q -  AHRSdata.q_est.x*AHRSdata.r ) * SensorCal.gyroPropDT;
    AHRSdata.q_est.z +=  ( AHRSdata.q_est.x*AHRSdata.q -  AHRSdata.q_est.y*AHRSdata.p +  AHRSdata.q_est.o*AHRSdata.r ) * SensorCal.gyroPropDT;

    // Run filter time propagation only if measurement propogation is not in progress
    //if (!loop.MeasurementUpdate)
    	//loop.TimeProp = 1;

    TimeProp();
}

/*---------------------------------------------------------------------
  Function Name: AHRS_AccMagCorrect
  Description:   Correct integrated gyro attitude estimate with gravity vector measurement
  Inputs:        None
  Returns:       Negative value if not correctly calculated, postive otherwise
-----------------------------------------------------------------------*/
// todo: pitch is not being calculated correctly near 90 degrees pitch
int AHRS_AccMagCorrect(void)
{
	// Quit if the biases are still being calculated
	if( SensorCal.biasCount < SensorCal.biasTotal)
		return -1;

	// todo: try using accelerometer values minus biases here -- didn't make much difference with test data
	float32 ax = -AHRSdata.ax;
	float32 ay = -AHRSdata.ay;
	float32 az = -AHRSdata.az;

	float32 root = sqrt(ax*ax + ay*ay + az*az);
	if ((root < SensorCal.acc_window_min) || (root > SensorCal.acc_window_max))
	    return -2;

	ax /= root;
	ay /= root;
	az /= root;

	if ((ax > 0.998) || (-ax > 0.998))
	    return -3;

	root = sqrt(ay*ay + az*az);
	if (root < 0.0001)
	    root = 0.0001;

	float32 sinR = -ay/root;
	float32 cosR = -az/root;

	float32 sinP = ax;
	float32 cosP = -(ay*sinR + az*cosR);

	float32 cosR2 = sqrt((cosR+1)*0.5);
	if (cosR2 < 0.0001)
	    cosR2 = 0.0001;

	float32 sinR2 = sinR/cosR2*0.5; //WARNING: This step is numerically ill-behaved!

	float32 cosP2 = sqrt((cosP+1)*0.5);
	if (cosP2 < 0.0001)
	    cosP2 = 0.0001;

	float32 sinP2 = sinP/cosP2*0.5; //WARNING: This step is numerically ill-behaved!

	if (((cosR2*cosR2 + sinR2*sinR2) > 1.1) || ((cosP2*cosP2 + sinP2*sinP2) > 1.1))
	    return -4;

    // Yaw calculation
    // Normalize the magnetometer vector to length 1
/*	magx = (float)AHRSdata.magY;
    magy = -(float)AHRSdata.magX;
    magz = (float)AHRSdata.magZ;
    // Todo: magx*magx can be done in fixed pt
    root = sqrt( magx*magx + magy*magy + magz*magz );
    magx /= root;
    magy /= root;
    magz /= root;
    yaw = atan2(-cosR*magy - sinR*magz  ,  cosP*magx+sinP*sinR*magy-sinP*cosR*magz);
    yaw += PI;
    if(yaw > PI){
            yaw -= 2*PI;
    }
    sinY2 = sin(yaw/2.0);
    cosY2 = cos(yaw/2.0);
    */

	float32 cosY2 = 1.0;
	float32 sinY2 = 0;

	// Finally get quaternion
	tQuaternion qroll,qpitch,qyaw;
	qyaw.o   = cosY2; qyaw.x = 0;      qyaw.y = 0;       qyaw.z = sinY2;
	qpitch.o = cosP2; qpitch.x = 0;    qpitch.y = sinP2; qpitch.z = 0;
	qroll.o  = cosR2; qroll.x = sinR2; qroll.y = 0;      qroll.z = 0;

	AHRSdata.q_meas = qprod(qyaw,qpitch);
	AHRSdata.q_meas = qprod(AHRSdata.q_meas, qroll);

	// Check if flipped from last measurement
	if( (AHRSdata.q_meas.x*AHRSdata.q_est.x + AHRSdata.q_meas.y*AHRSdata.q_est.y + AHRSdata.q_meas.z*AHRSdata.q_est.z + AHRSdata.q_meas.o*AHRSdata.q_est.o) < 0.0 )
	{
		AHRSdata.q_meas.o = - AHRSdata.q_meas.o;
		AHRSdata.q_meas.x = - AHRSdata.q_meas.x;
		AHRSdata.q_meas.y = - AHRSdata.q_meas.y;
		AHRSdata.q_meas.z = - AHRSdata.q_meas.z;
	}

	// Estimate the gyro bias
	tQuaternion Qbias;
	Qbias = qprod(qconj(AHRSdata.q_est), AHRSdata.q_meas);
	SensorCal.pBias -= SensorCal.K_GyroBias*Qbias.x;
	SensorCal.qBias -= SensorCal.K_GyroBias*Qbias.y;
	SensorCal.rBias -= SensorCal.K_GyroBias*Qbias.z;

	// Make the correction
	AHRSdata.q_est.o -= (AHRSdata.q_est.o-AHRSdata.q_meas.o) * SensorCal.K_AttFilter;
	AHRSdata.q_est.x -= (AHRSdata.q_est.x-AHRSdata.q_meas.x) * SensorCal.K_AttFilter;
	AHRSdata.q_est.y -= (AHRSdata.q_est.y-AHRSdata.q_meas.y) * SensorCal.K_AttFilter;
	AHRSdata.q_est.z -= (AHRSdata.q_est.z-AHRSdata.q_meas.z) * SensorCal.K_AttFilter;

	return 1;
}
