/*
 * main.c
 *
 *  Created on: Jan 31, 2013
 *      Author: mark
 */

#include <stdio.h>
#include <math.h>
#include "meschach/matrix.h"
#include "meschach/matrix2.h"

#include "SRCDKF.h"

/* globals */
volatile tSensorCal SensorCal;
volatile tAHRSdata AHRSdata;
volatile tIMUPacket IMU;
volatile tLoopFlags loop;
volatile tWiiIPCPacket WiiPacket;
volatile tPosPacket PosPacket;
volatile tPosVelPacket PosVelPacket;
volatile tPosYawCmdPacket PosYawPacket;
volatile tAttCmdPacket AttCmdPacket;
volatile tAttZCmdPacket AttZCmdPacket;
volatile tStatePacket StatePacket;

volatile tGoal Goal;
volatile tState State;

double GlobalTime;

extern VEC * x; // State vector
VEC * u;
VEC * Y;
VEC * Sn;
VEC * Y_xy;
VEC * Sn_xy;
VEC * Y_z;
VEC * Sn_z;

int processLine(FILE * infile, int * line_number)
{
	char line_buffer[BUFSIZ];
	int endOfFile = fgets(line_buffer, sizeof(line_buffer), infile);
	(*line_number)++;

	// first get the string identifying what kind of the data the line contains
	char type[BUFSIZ];
	sscanf(line_buffer, "%[^',']", type);

	if (strcmp(type,"IMU") == 0)
	{
		sscanf(line_buffer, "%*[^','],%d,%d,%d,%d,%d,%d\n",&IMU.gyroX,&IMU.gyroY,&IMU.gyroZ,&IMU.accX,&IMU.accY,&IMU.accZ);
		GlobalTime += IMUDT;
		AHRS_GyroProp();
	} else if (strcmp(type,"POSVEL") == 0)
	{

		sscanf(line_buffer,"%*[^','],%d,%d,%d,%d,%d,%d\n",&PosVelPacket.x,&PosVelPacket.y,&PosVelPacket.z,
				&PosVelPacket.dx,&PosVelPacket.dy,&PosVelPacket.dz);
		static int cnt = 0;
//		MeasProp();
		MeasPropXY();
		if (cnt % 5 == 0)
			MeasPropZ();
		cnt++;
	} else if (strcmp(type,"POS") == 0)
	{

		sscanf(line_buffer,"%*[^','],%d,%d,%d\n",&PosPacket.x,&PosPacket.y,&PosPacket.z);
		MeasProp();
//		static int cnt = 0;
//		MeasPropXY();
//		if (cnt % 5 == 0)
//			MeasPropZ();
//		cnt++;
	} else if (strcmp(type,"ATT") == 0)
	{

		sscanf(line_buffer,"%*[^','],%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n",
				&AttCmdPacket.p_cmd,&AttCmdPacket.q_cmd,&AttCmdPacket.r_cmd,
				&AttCmdPacket.qo_cmd,&AttCmdPacket.qx_cmd,&AttCmdPacket.qy_cmd,&AttCmdPacket.qz_cmd,
				&AttCmdPacket.qo_meas,&AttCmdPacket.qx_meas, &AttCmdPacket.qy_meas,&AttCmdPacket.qz_meas,
				&AttCmdPacket.AttCmd);
	} else if (strcmp(type,"STATE") == 0)
	{
		sscanf(line_buffer,"%*[^','],%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n",
				&StatePacket.x, &StatePacket.y,&StatePacket.z,
				&StatePacket.dx,&StatePacket.dy,&StatePacket.dz,
				&StatePacket.ddx,&StatePacket.ddy,&StatePacket.ddz,
				&StatePacket.ddx_bias,&StatePacket.ddy_bias,&StatePacket.ddz_bias,
				&StatePacket.qw,&StatePacket.qx,&StatePacket.qy,&StatePacket.qz,
				&StatePacket.p,&StatePacket.q,&StatePacket.r,
				&StatePacket.x_cmd,&StatePacket.y_cmd,&StatePacket.z_cmd,
				&StatePacket.dx_cmd,&StatePacket.dy_cmd,&StatePacket.dz_cmd,
				&StatePacket.qw_cmd,&StatePacket.qx_cmd,&StatePacket.qy_cmd,&StatePacket.qz_cmd,
				&StatePacket.p_cmd,&StatePacket.q_cmd,&StatePacket.r_cmd,
				&StatePacket.dt);
	} else if (strcmp(type,"ATTZ") == 0)
	{
		sscanf(line_buffer,"%*[^','],%d,%d,%d,%d,%d,%d,%d,%d,%d\n",
				&AttZCmdPacket.p_cmd,&AttZCmdPacket.q_cmd,&AttZCmdPacket.r_cmd,
				&AttZCmdPacket.qo_cmd,&AttZCmdPacket.qx_cmd,&AttZCmdPacket.qy_cmd,&AttZCmdPacket.qz_cmd,&AttZCmdPacket.z,&AttZCmdPacket.AttCmd);
	} else if (strcmp(type,"POSYAW") == 0)
	{
//		sscanf(line_buffer,"%*[^','],%d,%d,%d,%d,%d,%d,%d,%d\n",
//				&PosYawPacket.x_cmd,&PosYawPacket.y_cmd,&PosYawPacket.z_cmd,
//				&PosYawPacket.dx_cmd,&PosYawPacket.dy_cmd,&PosYawPacket.dz_cmd,
//				&PosYawPacket.yaw,&PosYawPacket.AttCmd);
	}
	else
	{
		printf("Command header not recognized!\n");
	}

	return endOfFile;

}

int main() {

	GlobalTime = 0.0;

		/* Open test data */
	char *inname = "/home/swarm/fuerte_workspace/UberPilot/UKF/quadOnBoardLog.txt";//"/home/mark/UberPilot/UKF/quadOnBoardLog.txt";
	FILE *infile;
	char line_buffer[BUFSIZ]; /* BUFSIZ is defined if you include stdio.h */
	int line_number;

	infile = fopen(inname, "r");
	if (!infile) {
		printf("Couldn't open file %s for reading.\n", inname);
		return 0;
	}
	printf("Opened file %s for reading.\n", inname);

	// Open file for human readable format textfile
	FILE * filterFile;
	FILE * sensorFile;
	FILE * viconFile;
	FILE * onBoardFilterFile;
	FILE * cmdFile;
	filterFile = fopen("filter_output.txt", "w");
	sensorFile = fopen("sensor_output.txt", "w");
	viconFile = fopen("vicon_output.txt", "w");
	onBoardFilterFile = fopen("onBoardFilter_output.txt", "w");
	cmdFile = fopen("cmd_output.txt", "w");
	char outStr[512];

	line_number = 0;
	InitFilter();
    AHRS_init();


	while (processLine(infile,&line_number)){

		if (GlobalTime > 20.13)
			__asm__(" NOP");

		if( SensorCal.biasCount >= SensorCal.biasTotal){
			// write to file
			sprintf(outStr, "%f,%f,%f,%f,%f,%f,%f,%f,%f,%f\r\n", GlobalTime,
					State.x,State.y,State.z,
					State.dx,State.dy,State.dz,
					x->ve[AXBIAS], x->ve[AYBIAS], x->ve[AZBIAS]);
			fputs(outStr, filterFile);

			sprintf(outStr, "%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f\r\n", GlobalTime,
					AHRSdata.q_est.o,AHRSdata.q_est.x,AHRSdata.q_est.y,AHRSdata.q_est.z,
					AHRSdata.p, AHRSdata.q, AHRSdata.r,
					AHRSdata.ax, AHRSdata.ay, AHRSdata.az,
					SensorCal.pBias, SensorCal.qBias, SensorCal.rBias);
			fputs(outStr, sensorFile);

			sprintf(outStr, "%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f\r\n", GlobalTime,
					PosPacket.x / POS_SCALE, PosPacket.y / POS_SCALE, PosPacket.z / POS_SCALE,
					PosVelPacket.dx / VEL_SCALE, PosVelPacket.dy / VEL_SCALE, PosVelPacket.dz / VEL_SCALE,
					AttCmdPacket.qo_meas / QUAT_SCALE,AttCmdPacket.qx_meas / QUAT_SCALE,
					AttCmdPacket.qy_meas / QUAT_SCALE,AttCmdPacket.qz_meas / QUAT_SCALE);
			fputs(outStr, viconFile);

			sprintf(outStr, "%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f\r\n", GlobalTime,
					StatePacket.x / ROT_SCALE, StatePacket.y / ROT_SCALE, StatePacket.z / ROT_SCALE,
					StatePacket.dx / ROT_SCALE, StatePacket.dy / ROT_SCALE, StatePacket.dz / ROT_SCALE,
					StatePacket.ddx / ROT_SCALE, StatePacket.ddy / ROT_SCALE, StatePacket.ddz / ROT_SCALE,
					StatePacket.ddx_bias / ROT_SCALE, StatePacket.ddy_bias / ROT_SCALE, StatePacket.ddz_bias / ROT_SCALE,
					StatePacket.qw / QUAT_SCALE, StatePacket.qx / QUAT_SCALE, StatePacket.qy / QUAT_SCALE, StatePacket.qz / QUAT_SCALE,
					StatePacket.p / ROT_SCALE, StatePacket.q / ROT_SCALE, StatePacket.r / ROT_SCALE,
					StatePacket.dt / VEL_SCALE);
			fputs(outStr, onBoardFilterFile);

			sprintf(outStr, "%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f\r\n", GlobalTime,
					StatePacket.x_cmd / ROT_SCALE, StatePacket.y_cmd / ROT_SCALE, StatePacket.z_cmd / ROT_SCALE,
					StatePacket.dx_cmd / ROT_SCALE, StatePacket.dy_cmd / ROT_SCALE, StatePacket.dz_cmd / ROT_SCALE,
					StatePacket.qw_cmd / QUAT_SCALE, StatePacket.qx_cmd / QUAT_SCALE, StatePacket.qy_cmd / QUAT_SCALE, StatePacket.qz_cmd / QUAT_SCALE,
					StatePacket.p_cmd / ROT_SCALE, StatePacket.q_cmd / ROT_SCALE, StatePacket.r_cmd / ROT_SCALE);
			fputs(outStr, cmdFile);
		}
	}

	printf("Filter done \n");

	return 0;
}

void MeasPropXY(void)
{
	Y_xy->ve[0] = PosPacket.x / POS_SCALE;
	Y_xy->ve[1] = PosPacket.y / POS_SCALE;
	SRCDKF_measUpdate(Y_xy, &h_pos_xy, Sn_xy);
}

void MeasPropZ(void)
{
	Y_z->ve[0] = PosPacket.z / POS_SCALE;
	SRCDKF_measUpdate(Y_z, &h_pos_z, Sn_z);
}

void MeasProp(void)
{
	Y->ve[0] = PosPacket.x / POS_SCALE;
	Y->ve[1] = PosPacket.y / POS_SCALE;
	Y->ve[2] = PosPacket.z / POS_SCALE;
#ifdef USE_VELOCITY_MEASUREMENT
	Y->ve[3] = PosVelPacket.dx / VEL_SCALE;
	Y->ve[4] = PosVelPacket.dy / VEL_SCALE;
	Y->ve[5] = PosVelPacket.dz / VEL_SCALE;
#endif
	SRCDKF_measUpdate(Y, &h_pos, Sn);
}

void TimeProp(void)
{
	static int cnt;
	float32 dt = IMUDT;

	// Approximately 50 Hz
	if (cnt % 20 == 1)
		AHRS_AccMagCorrect();
	cnt++;

	//getDT(&dt,&tprev);
	u->ve[0] = AHRSdata.ax*GRAVITY;	// ax (convert from g to m/s^2)
	u->ve[1] = AHRSdata.ay*GRAVITY;	// ay (convert from g to m/s^2)
	u->ve[2] = AHRSdata.az*GRAVITY;	// az (convert from g to m/s^2)
//	u->ve[3] = 1.0;//AHRSdata.q_est.o;	// qw
//	u->ve[4] = 0.0;//AHRSdata.q_est.x;	// qx
//	u->ve[5] = 0.0;//AHRSdata.q_est.y;	// qy
//	u->ve[6] = 0.0;//AHRSdata.q_est.z;	// qz
	u->ve[3] = AHRSdata.q_est.o;	// qw
	u->ve[4] = AHRSdata.q_est.x;	// qx
	u->ve[5] = AHRSdata.q_est.y;	// qy
	u->ve[6] = AHRSdata.q_est.z;	// qz

	//GpioG1DataRegs.GPADAT.bit.GPIO13 = 1;
	SRCDKF_timeUpdate(u, &dt, &f_kinematic);

	State.x = x->ve[XPOS];
	State.y = x->ve[YPOS];
	State.z = x->ve[ZPOS];
	State.dx = x->ve[XVEL];
	State.dy = x->ve[YVEL];
	State.dz = x->ve[ZVEL];
}



/*---------------------------------------------------------------------
  Function Name: InitFilter
  Description:   Initialize Filter
  Inputs:        None
  Returns:       None
-----------------------------------------------------------------------*/
void InitFilter(void)
{
    /* Filter Initialization */
	float32 process_noise = 1.0;
	float32 measurement_noise = 0.001;

	VEC * x0 = v_get(9);
	x0->ve[0] = 0.0;	// xpos
	x0->ve[1] = 0.0;	// ypos
	x0->ve[2] = 0.0;	// zpos
	x0->ve[3] = 0.0;	// dx
	x0->ve[4] = 0.0;	// dy
	x0->ve[5] = 0.0;	// dz
	x0->ve[6] = 0.0;	// ax_bias
	x0->ve[7] = 0.0;	// ay_bias
	x0->ve[8] = 0.0;	// az_bias
	VEC * Rx = v_get(9);
	Rx->ve[0] = 0.1;
	Rx->ve[1] = 0.1;
	Rx->ve[2] = 0.1;
	Rx->ve[3] = 0.1;
	Rx->ve[4] = 0.1;
	Rx->ve[5] = 0.1;
	Rx->ve[6] = 0.1;
	Rx->ve[7] = 0.1;
	Rx->ve[8] = 0.1;
	VEC * Rv = v_get(3);
	Rv->ve[0] = process_noise;
	Rv->ve[1] = process_noise;
	Rv->ve[2] = process_noise;

#ifdef USE_VELOCITY_MEASUREMENT
	Sn = v_get(6);
	Sn->ve[0] = sqrt(measurement_noise);
	Sn->ve[1] = sqrt(measurement_noise);
	Sn->ve[2] = sqrt(measurement_noise);
	Sn->ve[3] = sqrt(measurement_noise);
	Sn->ve[4] = sqrt(measurement_noise);
	Sn->ve[5] = sqrt(measurement_noise);
#elif defined USE_SEPARATE_Z_MEASUREMENT
	VEC * Sn_xy = v_get(2);
	VEC * Sn_z = v_get(1);
	Sn_xy->ve[0] = sqrt(measurement_noise);
	Sn_xy->ve[1] = sqrt(measurement_noise);
	Sn_z->ve[0] = sqrt(measurement_noise);
#else
	Sn = v_get(3);
	Sn->ve[0] = sqrt(measurement_noise);
	Sn->ve[1] = sqrt(measurement_noise);
	Sn->ve[2] = sqrt(measurement_noise);

	Sn_xy = v_get(2);
	Sn_xy->ve[0] = sqrt(measurement_noise);
	Sn_xy->ve[1] = sqrt(measurement_noise);
	Sn_z = v_get(1);
	Sn_z->ve[0] = sqrt(measurement_noise);
#endif

	VEC * RWvar = NULL;
	float32 h_width = sqrt(3);



	// initialize
	SRCDKF_initialize(x0, Rx, Rv, RWvar, h_width);

	u = NULL;
	u = v_resize(u,7);
	u->ve[0] = 0.0;	// ax
	u->ve[1] = 0.0;	// ay
	u->ve[2] = 9.81;	// az
	u->ve[3] = 1.0;	// qw
	u->ve[4] = 0.0;	// qx
	u->ve[5] = 0.0;	// qy
	u->ve[6] = 0.0;	// qz

#ifdef USE_VELOCITY_MEASUREMENT
	Y = v_get(6);
	Y->ve[0] = 0.0;	// x
	Y->ve[1] = 0.0;	// y
	Y->ve[2] = 0.0;	// z
	Y->ve[3] = 0.0;	// dx
	Y->ve[4] = 0.0;	// dy
	Y->ve[5] = 0.0;	// dz
#elif defined USE_SEPARATE_Z_MEASUREMENT
	Y = v_get(2);
	Y->ve[0] = 0.0;	// x/z
	Y->ve[1] = 0.0;	// y
#else
	Y = v_get(3);	// mark -- change
	Y->ve[0] = 0.0;	// x
	Y->ve[1] = 0.0;	// y
	Y->ve[2] = 0.0;	// z

	Y_xy = v_get(2);	// mark -- change
	Y_xy->ve[0] = 0.0;	// x
	Y_xy->ve[1] = 0.0;	// y
	Y_z = v_get(1);	// mark -- change
	Y_z->ve[0] = 0.0;	// z
#endif

}
