/*
 * AHRS.h
 *
 *  Created on: Nov 19, 2012
 *      Author: mark
 */

#ifndef AHRS_H_
#define AHRS_H_

#include "defines_c28.h"

// Prototypes
void AHRS_init(void);
void AHRS_GyroProp(void);
int AHRS_AccMagCorrect(void);
void Controller_Update(void);
void Controller_Init(void);

typedef struct sSensorCal{

    float32 pBias;
    float32 qBias;
    float32 rBias;
    float32 K_GyroBias;
    float32 gyroScale;
    float32 gyroPropDT;
    Uint32 tprev;

    float32 axBiasInit;
    float32 ayBiasInit;
    float32 azBiasInit;

    float32 accelScale;
    float32 acc_window_min;
    float32 acc_window_max;

    float32 K_AttFilter;

    int16_t biasCount;
    int16_t biasTotal;
    int16_t blankReads;

}tSensorCal;

typedef struct strQuaternion{
    float32 o;
    float32 x;
    float32 y;
    float32 z;
}tQuaternion;

tQuaternion qprod(tQuaternion a, tQuaternion b);
tQuaternion qconj(tQuaternion a);
void qnormalize(tQuaternion * a);
float32 qdot(tQuaternion a, tQuaternion b);
tQuaternion qmult(tQuaternion a, float32 b);

typedef struct sAHRSdata{
    float32 p;
    float32 q;
    float32 r;

    float32 ax;
    float32 ay;
    float32 az;

    tQuaternion q_est;
    tQuaternion q_meas;


}tAHRSdata;

typedef struct sAttitudeCmd{
    float32 p;
    float32 q;
    float32 r;

    tQuaternion q_cmd;
    tQuaternion qmin_old;

    float32 throttle;
    uint8_t AttCmd;


}tAttitudeCmd;

typedef struct  sMotorData{

    // motors
    uint8_t m1;
    uint8_t m2;
    uint8_t m3;
    uint8_t m4;

    // servos
    uint8_t s1;
    uint8_t s2;
    uint8_t s3;
    uint8_t s4;

}tMotorData;

typedef struct sGains
{
    float32 Kp_roll;
    float32 Ki_roll;
    float32 Kd_roll;

    float32 Kp_pitch;
    float32 Ki_pitch;
    float32 Kd_pitch;

    float32 Kp_yaw;
    float32 Ki_yaw;
    float32 Kd_yaw;

    float32 PWM1_trim;
    float32 PWM2_trim;
    float32 PWM3_trim;
    float32 PWM4_trim;

    float32 IntRoll;
    float32 IntPitch;
    float32 IntYaw;
    float32 AttitudeDT;
    float32 PositionDT;

    float32 maxang;
    float32 motorFF;
    uint16_t lowBatt;

    uint8_t stream_data;

    float32 I_x;
    float32 I_y;
    float32 I_z;

    float32 maxPosErr_xy;
    float32 maxPosErr_z;
    float32 maxVelErr_xy;
    float32 maxVelErr_z;

    float32 Kp_xy;
    float32 Ki_xy;
    float32 Kd_xy;

    float32 Kp_z;
    float32 Ki_z;
    float32 Kd_z;

    float32 I_xy_int_thresh;
    float32 I_z_int_thresh;

    float32 a;
    float32 b;
    float32 c;

    float32 rmag;
    float32 minRmagint;
    float32 minRmag;

}tGains;

typedef struct sGoal
{
	float32 x;
	float32 y;
	float32 z;

	float32 dx;
	float32 dy;
	float32 dz;

	float32 d2x;
	float32 d2y;
	float32 d2z;

	float32 d2x_fb;
	float32 d2y_fb;
	float32 d2z_fb;

	float32 d2x_fb_old;
	float32 d2y_fb_old;
	float32 d2z_fb_old;

	float32 d3x;
	float32 d3y;
	float32 d3z;

	float32 d3x_fb_old;
	float32 d3y_fb_old;
	float32 d3z_fb_old;

	float32 d3x_fb;
	float32 d3y_fb;
	float32 d3z_fb;

	float32 f_total;

	float32 yaw;
	tQuaternion qyaw_des;

	uint8_t useFeedback;
}tGoal;

typedef struct sState
{
	float32 x;
	float32 y;
	float32 z;

	float32 dx;
	float32 dy;
	float32 dz;
}tState;

#endif /* AHRS_H_ */
