function plotEmbeddedFilter( )

clc;

%% Set params
global param
param.DT = 1/1000;
param.G = 9.81;
param.gyro_scale = pi/14.375/180.0*1.0; % Raw gyro -> rad/sec
param.accel_scale = 1/256*param.G; % Raw accel -> m/s^2
param.nAttMeas = 50; % Do attitude measurement every 10*DT seconds
param.K_AttFilter = param.DT*param.nAttMeas*0.1;
param.K_GyroBias = param.DT*param.nAttMeas*0.05;

param.nPosVelMeas = 100; % Do position/velocity measurement every 100*DT seconds

%% IMPORT DATA
% file = 'sensorlog1.txt';
% %file = 'sensorlog2.txt';
% [sensor, truth] = getData(file);

x = 0;
y = 0;
z = 0;
dx = 0;
dy = 0;
dz = 0;

cdata = importdata('filter_output.txt');
t = cdata(:,1);
cfilter.x = cdata(:,2);
cfilter.y = cdata(:,3);
cfilter.z = cdata(:,4);
cfilter.dx = cdata(:,5);
cfilter.dy = cdata(:,6);
cfilter.dz = cdata(:,7);
cfilter.ax = cdata(:,8);
cfilter.ay = cdata(:,9);
cfilter.az = cdata(:,10);

cdata = importdata('onBoardFilter_output.txt');
t = cdata(:,1);
onfilter.x = cdata(:,2);
onfilter.y = cdata(:,3);
onfilter.z = cdata(:,4);
onfilter.dx = cdata(:,5);
onfilter.dy = cdata(:,6);
onfilter.dz = cdata(:,7);
onfilter.ax = cdata(:,8)/param.G;
onfilter.ay = cdata(:,9)/param.G;
onfilter.az = cdata(:,10)/param.G;
onfilter.ax_bias = cdata(:,11);
onfilter.ay_bias = cdata(:,12);
onfilter.az_bias = cdata(:,13);
onfilter.qw = cdata(:,14);
onfilter.qx = cdata(:,15);
onfilter.qy = cdata(:,16);
onfilter.qz = cdata(:,17);
onfilter.p = cdata(:,18);
onfilter.q = cdata(:,19);
onfilter.r = cdata(:,20);
onfilter.dt = cdata(:,21);

[onfilter.yaw, onfilter.roll, onfilter.pitch] = quat2angle([onfilter.qw, onfilter.qx, onfilter.qy, onfilter.qz]);

iEnd = length(t);
% iEnd = 1000;

sdata = importdata('sensor_output.txt');
sensor.qw = sdata(:,2);
sensor.qx = sdata(:,3);
sensor.qy = sdata(:,4);
sensor.qz = sdata(:,5);
sensor.p = sdata(:,6);
sensor.q = sdata(:,7);
sensor.r = sdata(:,8);
sensor.ax = sdata(:,9);
sensor.ay = sdata(:,10);
sensor.az = sdata(:,11);
sensor.pBias = sdata(:,12);
sensor.qBias = sdata(:,13);
sensor.rBias = sdata(:,14);

[sensor.yaw, sensor.roll, sensor.pitch] = quat2angle([sensor.qw, sensor.qx, sensor.qy, sensor.qz]);

vdata = importdata('vicon_output.txt');
truth.x = vdata(:,2);
truth.y = vdata(:,3);
truth.z = vdata(:,4);
truth.dx = vdata(:,5);
truth.dy = vdata(:,6);
truth.dz = vdata(:,7);
truth.qw = vdata(:,8);
truth.qx = vdata(:,9);
truth.qy = vdata(:,10);
truth.qz = vdata(:,11);

[truth.yaw, truth.roll, truth.pitch] = quat2angle([truth.qw, truth.qx, truth.qy, truth.qz]);

cmddata = importdata('cmd_output.txt');
cmd.x = cmddata(:,2);
cmd.y = cmddata(:,3);
cmd.z = cmddata(:,4);
cmd.dx = cmddata(:,5);
cmd.dy = cmddata(:,6);
cmd.dz = cmddata(:,7);
cmd.qw = cmddata(:,8);
cmd.qx = cmddata(:,9);
cmd.qy = cmddata(:,10);
cmd.qz = cmddata(:,11);
cmd.p = cmddata(:,12);
cmd.q = cmddata(:,13);
cmd.r = cmddata(:,14);

[cmd.yaw, cmd.roll, cmd.pitch] = quat2angle([cmd.qw, cmd.qx, cmd.qy, cmd.qz]);




% low pass filter accelerometers
fs=1000;                    % Sample Freuency
fn=fs/2;                    % Nyquist Frequency
fc=10;                      % Desired Cutoff frequency (example)
[b,a]=butter(3,2*fc/fn)      % IIR Filter coefficients, b=num, a=denom
sensor.axfilter=filter(b,a,sensor.ax);
sensor.ayfilter=filter(b,a,sensor.ay);
sensor.azfilter=filter(b,a,sensor.az);% apply filter to x



%% try doing simple dead recogning
for i=1:length(sensor.ax)
    fkinematic(i);
end

f(1) = figure(1);clf; 
subplot(4,1,1);hold all;
plot(t(1:iEnd), cfilter.x(1:iEnd),'r',t(1:iEnd), truth.x(1:iEnd),'r--');
plot(t(1:iEnd), cfilter.y(1:iEnd),'g',t(1:iEnd), truth.y(1:iEnd),'g--');
plot(t(1:iEnd), cfilter.z(1:iEnd),'b',t(1:iEnd), truth.z(1:iEnd),'b--');
% plot(t(1:iEnd), x(1:iEnd),'r-.');
% plot(t(1:iEnd), y(1:iEnd),'g-.');
% plot(t(1:iEnd), z(1:iEnd),'b-.');
plot(t(1:iEnd), onfilter.x(1:iEnd),'r-.');
plot(t(1:iEnd), onfilter.y(1:iEnd),'g-.');
plot(t(1:iEnd), onfilter.z(1:iEnd),'b-.');
plot(t(1:iEnd), cmd.x(1:iEnd),'r:');
plot(t(1:iEnd), cmd.y(1:iEnd),'g:');
plot(t(1:iEnd), cmd.z(1:iEnd),'b:');
title('Position');legend('xfilt', 'xvicon','yfilt', 'yvicon','zfilt', 'zvicon','xon','yon','zon','xcmd','ycmd','zcmd');
subplot(4,1,2);hold all;
plot(t(1:iEnd), cfilter.dx(1:iEnd),'r',t(1:iEnd), truth.dx(1:iEnd),'r--');
plot(t(1:iEnd), cfilter.dy(1:iEnd),'g',t(1:iEnd), truth.dy(1:iEnd),'g--');
plot(t(1:iEnd), cfilter.dz(1:iEnd),'b',t(1:iEnd), truth.dz(1:iEnd),'b--');
% plot(t(1:iEnd), dx(1:iEnd),'r-.');
% plot(t(1:iEnd), dy(1:iEnd),'g-.');
% plot(t(1:iEnd), dz(1:iEnd),'b-.');
plot(t(1:iEnd), onfilter.dx(1:iEnd),'r-.');
plot(t(1:iEnd), onfilter.dy(1:iEnd),'g-.');
plot(t(1:iEnd), onfilter.dz(1:iEnd),'b-.');
plot(t(1:iEnd), cmd.dx(1:iEnd),'r:');
plot(t(1:iEnd), cmd.dy(1:iEnd),'g:');
plot(t(1:iEnd), cmd.dz(1:iEnd),'b:');
title('Velocity');legend('dxfilt', 'dxvicon','dyfilt', 'dyvicon','dzfilt', 'dzvicon','dxdead','dydead','dzdead','dxcmd','dycmd','dzcmd');
subplot(4,1,3);hold all;
plot(t(1:iEnd), sensor.ax(1:iEnd),'r');
plot(t(1:iEnd), sensor.ay(1:iEnd),'g');
plot(t(1:iEnd), sensor.az(1:iEnd),'b');
% plot(t(1:iEnd), sensor.axfilter(1:iEnd));
% plot(t(1:iEnd), sensor.ayfilter(1:iEnd));
% plot(t(1:iEnd), sensor.azfilter(1:iEnd));
plot(t(1:iEnd), onfilter.ax(1:iEnd),'r-.');
plot(t(1:iEnd), onfilter.ay(1:iEnd),'g-.');
plot(t(1:iEnd), onfilter.az(1:iEnd),'b-.');
title('Acceleration');legend('x', 'y','z');
subplot(4,1,4);hold all;
plot(t(1:iEnd), cfilter.ax(1:iEnd),'r');
plot(t(1:iEnd), cfilter.ay(1:iEnd),'g');
plot(t(1:iEnd), cfilter.az(1:iEnd),'b');
plot(t(1:iEnd), onfilter.ax_bias(1:iEnd),'r-.');
plot(t(1:iEnd), onfilter.ay_bias(1:iEnd),'g-.');
plot(t(1:iEnd), onfilter.az_bias(1:iEnd),'b-.');
title('Estimated Accel Biases');legend('x', 'y','z');

f(2) = figure(2); clf; 
subplot(3,1,1);hold all;
plot(t(1:iEnd), sensor.roll(1:iEnd)*180/pi,'r');
plot(t(1:iEnd), sensor.pitch(1:iEnd)*180/pi,'g');
plot(t(1:iEnd), sensor.yaw(1:iEnd)*180/pi,'b');
plot(t(1:iEnd), truth.roll(1:iEnd)*180/pi,'r--');
plot(t(1:iEnd), truth.pitch(1:iEnd)*180/pi,'g--');
plot(t(1:iEnd), truth.yaw(1:iEnd)*180/pi,'b--');
plot(t(1:iEnd), onfilter.roll(1:iEnd)*180/pi,'r-.');
plot(t(1:iEnd), onfilter.pitch(1:iEnd)*180/pi,'g-.');
plot(t(1:iEnd), onfilter.yaw(1:iEnd)*180/pi,'b-.');
plot(t(1:iEnd), cmd.roll(1:iEnd)*180/pi,'r:','LineWidth',3');
plot(t(1:iEnd), cmd.pitch(1:iEnd)*180/pi,'g:','LineWidth',3');
plot(t(1:iEnd), cmd.yaw(1:iEnd)*180/pi,'b:','LineWidth',3');
title('AHRS Attitude');legend('of roll','of pitch','of yaw',...
    'v roll','v pitch','v yaw','on roll','on pitch','on yaw','cmd roll','cmd pitch','cmd yaw');
subplot(3,1,2);hold all;
plot(t(1:iEnd), sensor.p(1:iEnd)*180/pi,'r');
plot(t(1:iEnd), sensor.q(1:iEnd)*180/pi,'g');
plot(t(1:iEnd), sensor.r(1:iEnd)*180/pi,'b');
plot(t(1:iEnd), onfilter.p(1:iEnd)*180/pi,'r-.');
plot(t(1:iEnd), onfilter.q(1:iEnd)*180/pi,'g-.');
plot(t(1:iEnd), onfilter.r(1:iEnd)*180/pi,'b-.');
plot(t(1:iEnd), cmd.p(1:iEnd)*180/pi,'r:','LineWidth',3');
plot(t(1:iEnd), cmd.q(1:iEnd)*180/pi,'g:','LineWidth',3');
plot(t(1:iEnd), cmd.r(1:iEnd)*180/pi,'b:','LineWidth',3');
title('AHRS Rate');legend('of p','of q','of r', 'on p','on q','on r','cmd p','cmd q','cmd r');
subplot(3,1,3);hold all;
plot(t(1:iEnd), sensor.pBias(1:iEnd)*180/pi,'r');
plot(t(1:iEnd), sensor.qBias(1:iEnd)*180/pi,'g');
plot(t(1:iEnd), sensor.rBias(1:iEnd)*180/pi,'b');
title('AHRS Gyro Bias');legend('p','q','r');


f(3) = figure(3);clf;
plot(t(1:iEnd), onfilter.dt(1:iEnd));

%% Link x axes
ax = [];
for ii=1:length(f)
    ax = [ax; get(f(ii),'children')];
end
linkaxes(ax,'x');


function fkinematic(index)

	% Position update
    x(index+1) = x(index) + param.DT*dx(index);
    y(index+1) = y(index) + param.DT*dy(index);
    z(index+1) = z(index) + param.DT*dz(index);

    az = sensor.az(index)*param.G - param.G;
    
    dx(index+1) = dx(index) + param.DT*sensor.ax(index)*param.G;
    dy(index+1) = dy(index) + param.DT*sensor.ay(index)*param.G;
    dz(index+1) = dz(index) + param.DT*az;

end

end

