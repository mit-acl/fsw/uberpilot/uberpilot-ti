/*
 * quaternion.c
 *
 *  Created on: Nov 19, 2012
 *      Author: mark
 */

#include "AHRS.h"

tQuaternion qprod(tQuaternion a, tQuaternion b)
{
    tQuaternion result;
    result.o = a.o*b.o - a.x*b.x - a.y*b.y - a.z*b.z;
    result.x = a.o*b.x + a.x*b.o + a.y*b.z - a.z*b.y;
    result.y = a.o*b.y - a.x*b.z + a.y*b.o + a.z*b.x;
    result.z = a.o*b.z + a.x*b.y - a.y*b.x + a.z*b.o;
    return result;
}

//tQuaternion qprodconj(tQuaternion a_conj, tQuaternion b){
//    tQuaternion result;
//    a_conj.x = -a_conj.x;
//    a_conj.y = -a_conj.y;
//    a_conj.z = -a_conj.z;
//    result = qprod(a_conj,b);
//    return result;
//}

tQuaternion qconj(tQuaternion a)
{
	a.x = -a.x;
	a.y = -a.y;
	a.z = -a.z;
	return a;
}

void qnormalize(tQuaternion * a){
	float32 den = 1.0/sqrt(qdot(*a,*a));//a->o*a->o + a->x*a->x + a->y*a->y + a->z*a->z);
	if (den > 0.001)
	{
		a->o *= den;
		a->x *= den;
		a->y *= den;
		a->z *= den;
	}
}

float32 qdot(tQuaternion a, tQuaternion b){
	return a.o*b.o + a.x*b.x + a.y*b.y + a.z*b.z;
}

tQuaternion qmult(tQuaternion a, float32 b){
	a.o *= b;
	a.x *= b;
	a.y *= b;
	a.z *= b;
	return a;
}
