
/**************************************************************************
**
** Copyright (C) 1993 David E. Steward & Zbigniew Leyk, all rights reserved.
**
**			     Meschach Library
**
** This Meschach Library is provided "as is" without any express
** or implied warranty of any kind with respect to this software.
** In particular the authors shall not be liable for any direct,
** indirect, special, incidental or consequential damages arising
** in any way from use of the software.
**
** Everyone is granted permission to copy, modify and redistribute this
** Meschach Library, provided:
**  1.  All copies contain this copyright notice.
**  2.  All modified copies shall carry a notice stating who
**      made the last modification and the date of such modification.
**  3.  No charge is made for this software or works derived from it.
**      This clause shall not be construed as constraining other software
**      distributed on the same medium as this software, nor is a
**      distribution fee considered a charge.
**
***************************************************************************/


/*
	A collection of functions for computing norms: scaled and unscaled
*/
//static	char	rcsid[] = "$Id: norm.c,v 1.6 1994/01/13 05:34:35 des Exp $";

#include	<stdio.h>
#include	<math.h>
#include	"matrix.h"

/* square -- returns x^2 */
#ifndef ANSI_C
double	square(x)
double	x;
#else
double	square(double x)
#endif
{	return x*x;	}



/* _v_norm2 -- computes (scaled) 2-norm (Euclidean norm) of vectors */
#ifndef ANSI_C
double	_v_norm2(x,scale)
VEC	*x, *scale;
#else
double	_v_norm2(const VEC *x, const VEC *scale)
#endif
{
	int	i, dim;
	Real	s, sum;

//	if ( x == (VEC *)NULL )
//		error(E_NULL,"_v_norm2");
	dim = x->dim;

	sum = 0.0;
	if ( scale == (VEC *)NULL )
		for ( i = 0; i < dim; i++ )
			sum += square(x->ve[i]);
//	else if ( scale->dim < dim )
//		error(E_SIZES,"_v_norm2");
	else
		for ( i = 0; i < dim; i++ )
		{	s = scale->ve[i];
			sum += ( s== 0.0 ) ? square(x->ve[i]) :
							square(x->ve[i]/s);
		}

	return sqrt(sum);
}

