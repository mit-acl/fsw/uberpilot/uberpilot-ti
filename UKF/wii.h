/*
 * wii.h
 *
 *  Created on: Jan 16, 2013
 *      Author: mark
 */

#ifndef WII_H_
#define WII_H_

#include "defines_c28.h"

typedef struct sPix{
	int x;
	int y;
}tPix;

void processWiiData(void);
int comparex(const void * a, const void * b);
int comparey(const void * a, const void * b);
void wrap(float * ang);


#endif /* WII_H_ */
