/*******************************
 * UberPilot -- autopilot software for the Aerospace Controls Lab at MIT
 * Written by Mark Cutler
 * acl.mit.edu
 * M3 Core Code
 *******************************/


/*
 * Helpful links:
 * http://processors.wiki.ti.com/index.php/Concerto_Dual_Core_Boot
 */

/*
 * Debugging notes:
 *
 * 1) When developing code, work with the RAM configuration.
 *              a) To restart the application you have to disconnect the two cores, power cycle,
 *                              reconnect, and then reload the RAM
 * 2) To load the code in FLASH for running without the debugger:
 *              a) Load the flash to each core
 *              b) Power cycle the device (don't run from the debugger)
 * 3) Don't call the MtoC function before the c28 core is initialized.  Doing
 *              so will lead to issues when running in the debugger from RAM
 * 4) The wii camera is very sensitive to other i2c interference.  Make sure other sensors
 *              are not being read at the same time or right next to the wii camera reads
 * 5) At least for the C28 core, don't initialize static variables inside interrupt
 * 				service routines
 */

/*
 * Major TODOs:
 *
 * 1) Figure out how to copy needed functions from flash to ram to speed up processing,
 *                      especially for the c28x core. -- DONE for c28x core
 * 2) Implement better uart receive function that will check whether or not there is data on the buffer
 * 3) Implement time delay handling in the filtering function
 */

/*
 * Known Issues:
 *
 * 1) Weird bug with data_structs.h when running from RAM -- the ID's need to be in
 *              order, 0x01 0x02 0x03 ...  ???   Not sure what the real issue is.
 */

#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_i2c.h"
#include "inc/hw_nvic.h"
#include "inc/hw_sysctl.h"
#include "driverlib/i2c.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "utils/uartstdio.h"

// local includes
#include "defines_m3.h"

// global variables
volatile tSensorData SensorData;
volatile tLoopFlags loop;
volatile tStatePacket StatePacket;
volatile tVoltagePacket VoltageData;
volatile tM3SensorCal SensorCal;
volatile tSensorCalPacket SensorCalPacket;
volatile unsigned long timerCount;
volatile uint8_t pressureConversionCount;

volatile unsigned int SDWriteInProgress = 0;

int
main(void)
{

        volatile unsigned long ulLoop;

        InitCnsts();
        InitHardware();
        InitTimers();
        InitIPC();
        I2C0_Init();
        I2C1_Init();
        InitLEDs();
        InitDBG();

        sleep_M3(500000); // sleep for 1/2 second before initializing sensors

        InitIMU();
        InitPressure();
        InitWiiCam();

//
//      // Set GPIO E1 debugging pin.
//      GPIOPinTypeGPIOOutput(GPIO_PORTE_BASE, GPIO_PIN_1);
//      GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_1, 0); // GPIO25

        // Enable processor interrupts.
        IntMasterEnable();

        // Spin here until C28 is ready
//#ifdef _STANDALONE
        while (!IPCCtoMFlagBusy(IPC_FLAG17)) ;
        IPCCtoMFlagAcknowledge(IPC_FLAG17);
//#endif


        InitUDMA4UART();
        InitUART0(57600); // communication uart
        InitUART1(9600); // UART1 reads in sonar at 9600 bits/sec

        InitSDCard();

        // Enable main loop timer
        TimerEnable(TIMER0_BASE, TIMER_A);

        while(1)
        {

                if (SDWriteInProgress && !uDMAChannelIsEnabled(UDMA_CHANNEL_SSI0TX))
                {
                        SDWriteInProgress = 0;
                        DWORD rcvdat;
                        SSIDataPut(SSI0_BASE, 0xFF); /* Write the data to the tx fifo */
                        SSIDataGet(SSI0_BASE, &rcvdat); /* flush data read during the write */
                        SSIDataPut(SSI0_BASE, 0xFF); /* Write the data to the tx fifo */
                        SSIDataGet(SSI0_BASE, &rcvdat); /* flush data read during the write */
                        SSIDataPut(SSI0_BASE, 0xFF); /* Write the data to the tx fifo */
                        SSIDataGet(SSI0_BASE, &rcvdat); /* flush data read during the write */
                        SSIDataPut(SSI0_BASE, 0xFF); /* Write the data to the tx fifo */
                        SSIDataGet(SSI0_BASE, &rcvdat); /* flush data read during the write */
                }


                // ~1 Hz
                if (loop.ToggleLED)
                {
                        loop.ToggleLED = 0;
                        toggle_led(LED_RED);

                        // also send voltage data
                        UART_SendVoltage(UART0_BASE);

                }

                // todo: check for i2c bus crashes

                if (loop.I2C0Recover)
                {
                        loop.I2C0Recover = 0;
                        I2C0_Recover();
                }

                if (loop.I2C1Recover)
                {
                        loop.I2C1Recover = 0;
                        I2C1_Recover();
                }

                if (loop.ReadGyro)
                {
                        loop.ReadGyro = 0;

                        // TODO: look into update rate of accelerometer data -- maybe change to 3200 Hz for effective 1600 Hz bandwidth?
                        BYTE addr = ACC_DATA_ADDR; // Address of the first data byte
                        I2C_WriteData(I2C0_MASTER_BASE, ACC_SLAVE_ADDR, &addr, 1);
                        I2C_ReadData(I2C0_MASTER_BASE, ACC_SLAVE_ADDR, 6);

                        // read gyro
                        addr = GYRO_DATA_ADDR; // Address of the first data byte
                        I2C_WriteData(I2C0_MASTER_BASE, GYRO_SLAVE_ADDR, &addr, 1);
                        I2C_ReadData(I2C0_MASTER_BASE, GYRO_SLAVE_ADDR, 6);

                        //toggle_dbg(DBG_PB3);

                }

                if (loop.ReadMag)
                {
                        loop.ReadMag = 0;
                        BYTE addr = MAG_DATA_ADDR;
                        I2C_WriteData(I2C1_MASTER_BASE, MAG_SLAVE_ADDR, &addr, 1);
                        I2C_ReadData(I2C1_MASTER_BASE, MAG_SLAVE_ADDR, 6);
                }

                if (loop.ReadWii)
                {
                        loop.ReadWii = 0;

                        // read wii
                        BYTE addr = WII_DATA_ADDR;
                        I2C_WriteData(I2C1_MASTER_BASE, WII_SLAVE_ADDR, &addr, 1);
                        I2C_ReadData(I2C1_MASTER_BASE, WII_SLAVE_ADDR, 16);

                        // simulate sending position update // todo: this will happen from within i2c parser
//                      volatile tPosPacket posPacket;
//                      posPacket.x = 0;
//              posPacket.y = 0;
//              posPacket.z = 0;
////                    positionPacket.dx = 0;
////                    positionPacket.dy = 0;
////                    static float tmp1 = 0.0;
////                    float x1 = cos(tmp1);
////                    float x2 = sin(tmp1);
////                    positionPacket.x = (int32_t) (x1 * POS_SCALE);
////                    //positionPacket.z = (int32_t) (x2 * POS_SCALE);
////                    positionPacket.dx = (int32_t) (x2 * VEL_SCALE);
////                    //positionPacket.dz = (int32_t) (x1 * VEL_SCALE);
////                    tmp1 += 0.01;
//              MtoCIPCBlockWrite(PACKETID_POS, (void*) &posPacket.x, sizeof(tPosPacket), 0);

//                      volatile tPosVelPacket posVelPacket;
//                      posVelPacket.x = 0;
//              posVelPacket.y = 0;
//              posVelPacket.z = 0;
//              posVelPacket.dx = 0;
//              posVelPacket.dy = 0;
//              posVelPacket.dz = 0;
//              MtoCIPCBlockWrite(PACKETID_POSVEL, (void*) &posVelPacket.x, sizeof(tPosVelPacket), 0);

//                      volatile tPosYawCmdPacket posYawCmdPacket;
//                      posYawCmdPacket.x_cmd = 1 * ROT_SCALE;
//                      posYawCmdPacket.y_cmd = 2 * ROT_SCALE;
//                      posYawCmdPacket.z_cmd = 1 * ROT_SCALE;
//                      posYawCmdPacket.dx_cmd = 0 * ROT_SCALE;
//                      posYawCmdPacket.dy_cmd = 0 * ROT_SCALE;
//                      posYawCmdPacket.dz_cmd = 0 * ROT_SCALE;
//                      posYawCmdPacket.yaw = 0 * ROT_SCALE;
//                      posYawCmdPacket.AttCmd = POSITION;
//                      MtoCIPCBlockWrite(PACKETID_POS_YAW_CMD, (void*) &posYawCmdPacket.x_cmd, sizeof(tPosYawCmdPacket), 0);


                }

                // Start reading Pressure
                if(loop.StartPressure){
                        loop.StartPressure = 0;
                        BYTE data[2];
                        data[0] = PRES_DATA_ADDR;
                        data[1] = PRES_PRESSURE+(SensorCal.oss<<6);
                        I2C_WriteData(I2C1_MASTER_BASE, PRES_SLAVE_ADDR, data, 2);
                }

                // Read Pressure
                if(loop.ReadPressure){
                        loop.ReadPressure = 0;
                        BYTE addr;
                        addr = PRES_DATA_ADDR+0x02; // temp read address is 0xF6
                        I2C_WriteData(I2C1_MASTER_BASE, PRES_SLAVE_ADDR, &addr, 1);
                        I2C_ReadData(I2C1_MASTER_BASE, PRES_SLAVE_ADDR, 3);
                }

                if(loop.StartTemperature){
                        loop.StartTemperature = 0;
                        BYTE data[2];
                        data[0] = PRES_DATA_ADDR;
                        data[1] = PRES_TEMPERATURE;
                        I2C_WriteData(I2C1_MASTER_BASE, PRES_SLAVE_ADDR, data, 2);
                        loop.StartWait = 1;
                        SensorCal.conversionTime = SensorCal.temperatureConversionTime;
                        pressureConversionCount = 0; // reset 5ms wait in case we are waiting for a pressure measurement right now
                }

                // Read temperature
                if(loop.ReadTemperature){
                        loop.ReadTemperature = 0;
                        BYTE addr;
                        addr = PRES_DATA_ADDR+0x02; // temp read address is 0xF6
                        I2C_WriteData(I2C1_MASTER_BASE, PRES_SLAVE_ADDR, &addr, 1);
                        I2C_ReadData(I2C1_MASTER_BASE, PRES_SLAVE_ADDR, 2);     // end of this starts pressure read
                }

                // ~100 Hz
                if (loop.SendSerial)
                {
                        loop.SendSerial = 0;
                        UART_SendState(UART0_BASE);
                        //toggle_dbg(DBG_PB3);
                        //UART_SendSensors(UART0_BASE);
//                        UART_SendWii(UART0_BASE);

                        // flush tx no matter what
                        UART_FlushTX(UART0_BASE);

                }


        }

}



