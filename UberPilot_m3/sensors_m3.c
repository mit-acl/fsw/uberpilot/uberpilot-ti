/*
 * sensors_m3.c
 *
 *  Created on: Jan 14, 2013
 *      Author: mark
 */

#include "sensors_m3.h"

extern volatile tM3SensorCal SensorCal;
extern volatile tSensorData SensorData;

// directly from data sheet
void BMP180_calc_pressure(void)
{
    SensorCal.B6 = SensorCal.B5 - 4000;

    // calculate B3
    SensorCal.X1 = (SensorCal.B6*SensorCal.B6) >> 12;
    SensorCal.X1 *= SensorCal.B2;
    SensorCal.X1 >>= 11;

    SensorCal.X2 = SensorCal.AC2*SensorCal.B6;
    SensorCal.X2 >>= 11;

    SensorCal.X3 = SensorCal.X1 + SensorCal.X2;

    SensorCal.B3 = (((((int32_t)SensorCal.AC1)*4 + SensorCal.X3) << SensorCal.oss) + 2) >> 2;

    // calculate B4
    SensorCal.X1 = (SensorCal.AC3*SensorCal.B6) >> 13;
    SensorCal.X2 = (SensorCal.B1*((SensorCal.B6*SensorCal.B6) >> 12)) >> 16;
    SensorCal.X3 = ((SensorCal.X1 + SensorCal.X2) + 2) >> 2;
    SensorCal.B4 = (SensorCal.AC4*((uint32_t) SensorCal.X3 + 32768)) >> 15;

    SensorCal.B7 = ((uint32_t)(SensorCal.UP - SensorCal.B3))*(50000 >> SensorCal.oss);
    if (SensorCal.B7 < 0x80000000){
        SensorData.pressure = (SensorCal.B7 << 1)/SensorCal.B4;
    } else {
        SensorData.pressure = (SensorCal.B7/SensorCal.B4) << 1;
    }
    SensorCal.X1 = (SensorData.pressure >> 8);
    SensorCal.X1 *= SensorCal.X1;
    SensorCal.X1 = (SensorCal.X1*3038) >> 16;
    SensorCal.X2 = (-7357*SensorData.pressure) >> 16;
    SensorData.pressure += (SensorCal.X1 + SensorCal.X2 + 3791) >> 4;

    // filter pressure reading
    SensorData.pressure_buff_sum += SensorData.pressure - SensorData.pressure_buff[SensorData.pressure_index];
    SensorData.pressure_filt = SensorData.pressure_buff_sum >> 5; // divide by 32
    SensorData.pressure_buff[SensorData.pressure_index] = SensorData.pressure;
    SensorData.pressure_index++;
    if (SensorData.pressure_index > 31)
        SensorData.pressure_index = 0; // reset to beginning of buffer

}

// directly from data sheet
void BMP180_calc_tmperature(void)
{
    SensorCal.X1 = ((SensorCal.UT - SensorCal.AC6)*SensorCal.AC5) >> 15;
    SensorCal.X2 = ((int32_t)SensorCal.MC << 11)/(SensorCal.X1 + SensorCal.MD);
    SensorCal.B5 = SensorCal.X1 + SensorCal.X2;
    SensorData.temperature = (SensorCal.B5 + 8) >> 4;

    // filter temperature reading
    SensorData.temperature_buff_sum += SensorData.temperature - SensorData.temperature_buff[SensorData.temperature_index];
    SensorData.temperature_filt = SensorData.temperature_buff_sum >> 2; // divide by 4
    SensorData.temperature_buff[SensorData.temperature_index] = SensorData.temperature;
    SensorData.temperature_index++;
    if (SensorData.temperature_index > 3)
        SensorData.temperature_index = 0; // reset to beginning of buffer
}
