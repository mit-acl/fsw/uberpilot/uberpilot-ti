/*
 * setup.c
 *
 *  Created on: Oct 26, 2012
 *      Author: mark
 */

#include "defines_m3.h"
#include "comm_m3/I2C_m3.h"

/* globals */
extern volatile unsigned long timerCount;
extern volatile tLoopFlags loop;
extern volatile tM3SensorCal SensorCal;
extern volatile uint8_t pressureConversionCount;
extern volatile tSensorCalPacket SensorCalPacket;

#ifdef _FLASH
// These are defined by the linker (see device linker command file)
extern unsigned long RamfuncsLoadStart;
extern unsigned long RamfuncsRunStart;
extern unsigned long RamfuncsLoadSize;
#endif

/*---------------------------------------------------------------------
  Function Name: InitHarware
  Description:   Initialize system clock and other hardware settings
  Inputs:        None
  Returns:       None
-----------------------------------------------------------------------*/
void InitHardware(void)
{
	/*******************************************************************

	M3

	PB0_GPIO8 - U1RX
	PB1_GPIO9 - U1TX

	PB2_GPIO10 - ADCSOCB

	PB3_GPIO11 - DBG1
	PB4_GPIO12 - DBG2

	PB6_GPIO14 - I2C0SDA
	PB7_GPIO15 - I2C0SCL

	PD0_GPIO16 - M3_SSIOTX
	PD1_GPIO17 - M3_SSIORX
	PD2_GPIO18 - M3_SSIOCLK
	PD3_GPIO19 - M3_SSIOFSS


	PD6_GPIO22 - I2C1SDA
	PD7_GPIO23 - I2C1SCL

	PC4_GPIO68 - BLUE LED
	PC5_GPIO69 - YELLOW LED
	PC6_GPIO70 - GREEN LED
	PC7_GPIO71 - RED LED

	PJ3_GPIO59 - U0RX
	PJ2_GPIO58 - U0TX

	PF2_GPIO34 - BOOT
	PF3_GPIO35 - BOOT
	PG3_GPIO43 - BOOT
	PG7_GPIO47 - BOOT

	C28

	PA0_GPIO0 - EPwm1A, PWM1
	PA0_GPIO1 - EPwm1B, PWM2
	PA0_GPIO2 - EPwm2A, PWM3
	PA0_GPIO3 - EPwm2B, PWM4
	PA0_GPIO4 - EPwm3A, PWM5 -- currently used for adc SOC
	PA0_GPIO5 - EPwm3B, PWM6
	PA0_GPIO6 - EPwm4A, PWM7
	PA0_GPIO7 - EPwm4B, PWM8

	PB5_GPIO13 - DBG3

	PF0_GPIO32 - I2C2SDA
	PF1_GPIO33 - I2C2SCL

	ADC

	BATT_VOLT - ADC2INB0

	*****************************************************************/



	/*****************************************************************
	 * Battery voltage scaling:
	 *
	 * 	Volts	ADC readout
	 *	4.084	1182
	 *	5.012	1453
	 *	5.949	1725
	 *	7.01	2034
	 *	8.12	2357
	 *	9.09	2636
	 *	10.05	2914
	 *	11.51	3337
	 *	12.03	3486
	 *	12.44	3603
	 *	12.99	3765
	 *
	 *	Linear fit coefficients: [3.4514, -4.7237] (converts adc readings to mV)
	 *
	 *	Matlab code:
	 *
	 *	% TI_uberpilot_voltage_scaling.m

		clc; clear all;

		mV = 1000*[4.084 5.012 5.949 7.01 8.12 9.09 10.05 11.51 12.03 12.44 12.99];
		adc = [1182 1453 1725 2034 2357 2636 2914 3337 3486 3603 3765];

		coeffs = polyfit(adc,mV,1);

		figure(1); clf;
		plot(adc,mV,adc,adc*coeffs(1)+coeffs(2));
		legend('data','fit');
	 *
	 *
	 *
	 ****************************************************************/

    // Setup main clock tree for 75MHz - M3 and 150MHz - C28x
    SysCtlClockConfigSet(SYSCTL_SYSDIV_1 | SYSCTL_M3SSDIV_2 | SYSCTL_USE_PLL |
                         (SYSCTL_SPLLIMULT_M & 0x0F));


	#ifdef _FLASH
	// Copy time critical code and Flash setup code to RAM
	// This includes the following functions:  InitFlash();
	// The  RamfuncsLoadStart, RamfuncsLoadSize, and RamfuncsRunStart
	// symbols are created by the linker. Refer to the device .cmd file.
		memcpy(&RamfuncsRunStart, &RamfuncsLoadStart, (size_t)&RamfuncsLoadSize);

	// Call Flash Initialization to setup flash waitstates
	// This function must reside in RAM
		FlashInit();
	#endif

	#ifdef _STANDALONE
	#ifdef _FLASH
		//  Send boot command to allow the C28 application to begin execution
		IPCMtoCBootControlSystem(CBROM_MTOC_BOOTMODE_BOOT_FROM_FLASH);
	#else
		//  Send boot command to allow the C28 application to begin execution
		IPCMtoCBootControlSystem(CBROM_MTOC_BOOTMODE_BOOT_FROM_RAM);
	#endif
	#endif

    // Enable GPIOs
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOJ);

	// Enable Pullups on EPWM(1-4)A/B capable pins
	GPIOPadConfigSet(GPIO_PORTA_BASE, 0xFF, GPIO_PIN_TYPE_STD_WPU);

	// Give C28 control of port A GPIOs
	GPIOPinConfigureCoreSelect(GPIO_PORTA_BASE, 0xFF, GPIO_PIN_C_CORE_SELECT);

	// Give C28 control of pb2_gpio10 -- used for adc
	// Give C28 control of pb5_gpio13 for debugging  0x24 = (00100100)
	GPIOPinConfigureCoreSelect(GPIO_PORTB_BASE, 0x24, GPIO_PIN_C_CORE_SELECT);

	// Initialize M3toC28 message RAM and Sx SARAM and wait until initialized
	RAMMReqSharedMemAccess(S0_ACCESS, SX_C28MASTER);
	RAMMReqSharedMemAccess(S1_ACCESS, SX_C28MASTER);
	RAMMReqSharedMemAccess(S2_ACCESS, SX_C28MASTER);
	RAMMReqSharedMemAccess(S3_ACCESS, SX_C28MASTER);
	RAMMReqSharedMemAccess(S4_ACCESS, SX_C28MASTER);
	RAMMReqSharedMemAccess(S5_ACCESS, SX_C28MASTER);
	RAMMReqSharedMemAccess(S6_ACCESS, SX_M3MASTER);
	RAMMReqSharedMemAccess(S7_ACCESS, SX_M3MASTER);

}

/*---------------------------------------------------------------------
  Function Name: InitTimers
  Description:   Initialize timers
  Inputs:        None
  Returns:       None
-----------------------------------------------------------------------*/
void InitTimers(void)
{
	 // Enable clock supply for Timer0 and Timer1
	SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER1);

	// Configure a 32-bit periodic timer.
	TimerConfigure(TIMER0_BASE, TIMER_CFG_32_BIT_PER);
	TimerLoadSet(TIMER0_BASE, TIMER_A, SysCtlClockGet(SYSTEM_CLOCK_SPEED) / 1000); // 1khz

	// Configure a 32-bit one-shot timer for sleep function
	TimerConfigure(TIMER1_BASE, TIMER_CFG_32_BIT_OS);

	// Setup the interrupts for the timer timeouts.
	IntEnable(INT_TIMER0A);
	TimerIntEnable(TIMER0_BASE, TIMER_TIMA_TIMEOUT);
	IntRegister(INT_TIMER0A, MainSystemTimerInterrupt);

	// Enable the timers.
	//TimerEnable(TIMER0_BASE, TIMER_A);
}

/*---------------------------------------------------------------------
  Function Name: sleep_M3
  Description:   sleep the M3
  Inputs:        us -- time in microseconds to sleep
  Returns:       None
-----------------------------------------------------------------------*/
void sleep_M3(unsigned long us)
{
	unsigned long sys_clk = SysCtlClockGet(SYSTEM_CLOCK_SPEED) / 1000000;
	TimerLoadSet(TIMER1_BASE, TIMER_A, us * sys_clk);
	TimerEnable(TIMER1_BASE, TIMER_A);
	while(HWREG(TIMER1_BASE + TIMER_O_RIS) != 1)
		asm(" nop"); // wait
	HWREG(TIMER1_BASE + TIMER_O_ICR) = 1; // clear RIS done flag

}

/*---------------------------------------------------------------------
  Function Name: InitLED
  Description:   Initialize LEDs
  Inputs:        None
  Returns:       None
-----------------------------------------------------------------------*/
void InitLEDs(void)
{

	// Set up the Pin for LEDs
	GPIOPinTypeGPIOOutput(LED_RED);
	GPIOPinTypeGPIOOutput(LED_GREEN);
	GPIOPinTypeGPIOOutput(LED_BLUE);
	GPIOPinTypeGPIOOutput(LED_YELLOW);
	GPIOPinWrite(LED_RED, 1);
	GPIOPinWrite(LED_GREEN, 1);
	GPIOPinWrite(LED_BLUE, 1);
	GPIOPinWrite(LED_YELLOW, 1);
}

/*---------------------------------------------------------------------
  Function Name: InitDGB
  Description:   Initialize debug pins
  Inputs:        None
  Returns:       None
-----------------------------------------------------------------------*/
void InitDBG(void)
{

	// Set up the Pin for Red LED
	GPIOPinTypeGPIOOutput(DBG_PB3);
	GPIOPinTypeGPIOOutput(DBG_PB4);
	GPIOPinWrite(DBG_PB3, ~1);
	GPIOPinWrite(DBG_PB4, ~1);
}


/*---------------------------------------------------------------------
  Function Name: InitCnsts
  Description:   Initialize constants
  Inputs:        None
  Returns:       None
-----------------------------------------------------------------------*/
void InitCnsts(void)
{
	// initialize variables
	SensorCal.axBias = 0;
	SensorCal.ayBias = 0;
	SensorCal.azBias = 0;
	SensorCal.blankReads = 100;
	SensorCal.calibration_cnts = 0;

	loop.AccMagCorrect = 0;
	loop.AttCtl = 0;
	loop.GyroProp = 0;
	loop.I2C0Recover = 0;
	loop.I2C1Recover = 0;
	loop.LogData = 0;
	loop.ReadAccel = 0;
	loop.ReadGyro = 0;
	loop.ReadMag = 0;
	loop.ReadSerial = 0;
	loop.SendSensors = 0;
	loop.SendSerial = 0;
	loop.ToggleLED = 0;
	loop.ViconCorrect = 0;
}

/*---------------------------------------------------------------------
  Function Name: MainSystemTimerInterrupt
  Description:   Fires at 2kHz and controls the main while loop
  Inputs:        None
  Returns:       None
-----------------------------------------------------------------------*/
uint8_t pb0_state = 0;
void MainSystemTimerInterrupt(void)
{
    // Clear the timer interrupt.
    TimerIntClear(TIMER0_BASE, TIMER_TIMA_TIMEOUT);

    timerCount++;

    // 1Hz
    if ( timerCount % 1000 == 0){
    	loop.ToggleLED = 1;
//    	loop.StartTemperature = 1;
    }

    // 1000 Hz
    if ( timerCount % 1 == 0 )
    	loop.ReadGyro = 1;
    // At 50Hz, signal for a mag read
    if ( timerCount % 20 == 10 )
		loop.ReadMag = 1;
    // At 100Hz, read wii
	if ( timerCount % 10 == 5 ){ // the 10 and 2 here are important -- don't want the wii, mag, and pressure sensors reading at the same time
		loop.ReadWii = 1;
	}
	// at 25Hz, read pressure sensor
	if ( timerCount % 40 == 0){
		loop.ReadPressure = 1;
		if (timerCount % 1000 == 0){
			loop.StartTemperature = 1;
		}
	}
#ifdef SERIAL_SEND_10HZ
	// at 10 Hz, send serial data
	if ( timerCount % 100 == 0)
		loop.SendSerial = 1;
#elif defined SERIAL_SEND_100HZ
	// at 100 Hz, send serial data
	if ( timerCount % 10 == 0)
		loop.SendSerial = 1;
#endif

	// Wait 5 ms until you can actually read the temperature or pressure.
	if (loop.StartWait == 1){
		pressureConversionCount++;
		if (pressureConversionCount > SensorCal.conversionTime){ // == 1 when 0ms have passed
			pressureConversionCount = 0;
			loop.ReadTemperature = 1;
			loop.StartWait = 0;
		}
	}



}

/*---------------------------------------------------------------------
  Function Name: InitConsole
  Description:   Initialize UART0 for console debugging
  Inputs:        None
  Returns:       None
-----------------------------------------------------------------------*/
void InitConsole(void)
{
    // Configure the pin muxing for UART0 functions on port E4 and E5.
//    GPIOPinConfigure(GPIO_PE4_U0RX);
//    GPIOPinConfigure(GPIO_PE5_U0TX);

    // Select the alternate (UART) function for these pins.
//    GPIOPinTypeUART(GPIO_PORTE_BASE, GPIO_PIN_4 | GPIO_PIN_5);

	GPIOPinConfigure(GPIO_PJ3_U0RX);
	GPIOPinConfigure(GPIO_PJ2_U0TX);
	GPIOPinTypeUART(GPIO_PORTJ_BASE, GPIO_PIN_2 | GPIO_PIN_3);

    // Initialize the UART for console I/O.
    UARTStdioInit(0);
}

/*---------------------------------------------------------------------
  Function Name: InitIMU
  Description:   Initialize rate gyro, accel, and mag if needed
  Inputs:        None
  Returns:       None
-----------------------------------------------------------------------*/
void InitIMU()
{
    BYTE data[6];

    // Configure the gyros (ITG-3200)
    data[0] = 0x16;                             // DLPF_FS register
    data[1] = 0x18;                       // Full scale, 8KHz sample rate
    I2C_WriteData(I2C0_MASTER_BASE, GYRO_SLAVE_ADDR, data, 2);   // Write to the ITG-3200
   
    // Configure the accelerometer (ADXL345)
	data[0] = 0x1E; // OFSX register (offset) // write zeros here in case we didn't power cycle
	data[1] = 0;
	data[2] = 0;
	data[3] = 0;
	I2C_WriteData(I2C0_MASTER_BASE, ACC_SLAVE_ADDR, data, 4); // Write to the ADXL-345
    data[0] = 0x31; // DATA_FORMAT register
    data[1] = 0x09; // Range = +- 4g, full resolution
    I2C_WriteData(I2C0_MASTER_BASE, ACC_SLAVE_ADDR, data, 2); // Write to the ADXL-345
    data[0] = 0x2D; // PWR_CTL register
    data[1] = 0x08; // Measurement mode
    I2C_WriteData(I2C0_MASTER_BASE, ACC_SLAVE_ADDR, data, 2); // Write to the ADXL-345
    data[0] = 0x2C; // BW_RATE register
	data[1] = 0x0E; // High power, 1600Hz data rate
	I2C_WriteData(I2C0_MASTER_BASE, ACC_SLAVE_ADDR, data, 2); // Write to the ADXL-345

    // Configure the magnetometer (HMC5883L)
    data[0] = 0x00; // Config Reg A
    data[1] = 0x14; // 30Hz, normal measurement mode
    data[2] = 0x20; // 1.3Ga range (default)
    data[3] = 0x00; // Continuous mode
    I2C_WriteData(I2C1_MASTER_BASE, MAG_SLAVE_ADDR, data, 4); // Write to the HMC5883 -- register automatically updates

    // Get the calibration data from the eeprom
    data[0] = EEPROM_SENSORCAL_ADDR >> 8;
    data[1] = EEPROM_SENSORCAL_ADDR;
    I2C_WriteData(I2C1_MASTER_BASE, EEPROM_SLAVE_ADDR, data, 2); // set internal pointer to correct address
    I2C_ReadData(I2C1_MASTER_BASE, EEPROM_SLAVE_ADDR, sizeof(tSensorCalPacket)); // read data

}

/*---------------------------------------------------------------------
  Function Name: InitWiiCam
  Description:   Initialize the wii ir camera
  Inputs:        None
  Returns:       None
-----------------------------------------------------------------------*/
void InitWiiCam()
{
    BYTE data[6];
//    data[0] = 0x30;
//    data[1] = 0x01;
//    I2C_WriteData(I2C1_MASTER_BASE, WII_SLAVE_ADDR, data, 2);   // Write to the wii ir camera
//    sleep_M3(200000);

    // initialize
    data[0] = 0x30;
    data[1] = 0x08;
    I2C_WriteData(I2C1_MASTER_BASE, WII_SLAVE_ADDR, data, 2);   // Write to the wii ir camera
    sleep_M3(200000);

    // first byte is register
    // see http://wiibrew.org/wiki/Wiimote#IR_Camera

    // full sensitivity
    BYTE block1a[8] = {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xFF};
    BYTE block1b[3] = {0x07,0x00,0x0C};
    BYTE block2[3] = {0x1A,0x00,0x00};

        // wii level 1
//    BYTE block1a[8] = {0x00,0x02,0x00,0x00,0x71,0x01,0x00,0x64};
//    BYTE block1b[3] = {0x07,0x00,0xFE};
//    BYTE block2[3] = {0x1A,0xFD,0x05};

        // wii level 2
//    BYTE block1a[8] = {0x00,0x02,0x00,0x00,0x71,0x01,0x00,0x96};
//    BYTE block1b[3] = {0x07,0x00,0xB4};
//    BYTE block2[3] = {0x1A,0xB3,0x04};

        // wii level 3
//    BYTE block1a[8] = {0x00,0x02,0x00,0x00,0x71,0x01,0x00,0xAA};
//    BYTE block1b[3] = {0x07,0x00,0x64};
//    BYTE block2[3] = {0x1A,0x63,0x03};

        // wii level 4
//    BYTE block1a[8] = {0x00,0x02,0x00,0x00,0x71,0x01,0x00,0xC8};
//    BYTE block1b[3] = {0x07,0x00,0x36};
//    BYTE block2[3] = {0x1A,0x35,0x02};

        // wii level 5
//    BYTE block1a[8] = {0x00,0x02,0x00,0x00,0x71,0x01,0x00,0x72};
//    BYTE block1b[3] = {0x07,0x00,0x20};
//    BYTE block2[3] = {0x1A,0x1F,0x01};

    // write block 1a
    I2C_WriteData(I2C1_MASTER_BASE, WII_SLAVE_ADDR, block1a, sizeof(block1a));
    sleep_M3(200000);

    // write block 1b
    I2C_WriteData(I2C1_MASTER_BASE, WII_SLAVE_ADDR, block1b, sizeof(block1b));
    sleep_M3(200000);

    // write block 2
    I2C_WriteData(I2C1_MASTER_BASE, WII_SLAVE_ADDR, block2, 3);
    sleep_M3(200000);

    // write mode
    data[0] = 0x33;
    data[1] = WII_MODE;
    I2C_WriteData(I2C1_MASTER_BASE, WII_SLAVE_ADDR, data, 2);
    sleep_M3(200000);

    // finish initialization
    data[0] = 0x30;
    data[1] = 0x08;
    I2C_WriteData(I2C1_MASTER_BASE, WII_SLAVE_ADDR, data, 2);   // Write to the wii ir camera
    sleep_M3(200000);


//    data[0] = 0x06;
//    data[1] = 0x90;
//    I2C_WriteData(I2C1_MASTER_BASE, WII_SLAVE_ADDR, data, 2);   // Write to the wii ir camera
//    sleep_M3(200000);
//
//    data[0] = 0x08;
//    data[1] = 0xC0;
//    I2C_WriteData(I2C1_MASTER_BASE, II_SLAVE_ADDR, data, 2);   // Write to the wii ir camera
//    sleep_M3(200000);
//
//    data[0] = 0x1A;
//    data[1] = 0x40;
//    I2C_WriteData(I2C1_MASTER_BASE, WII_SLAVE_ADDR, data, 2);   // Write to the wii ir camera
//    sleep_M3(200000);
//
//    data[0] = 0x33;
//    data[1] = 0x33;
//    I2C_WriteData(I2C1_MASTER_BASE, WII_SLAVE_ADDR, data, 2);   // Write to the wii ir camera
//    sleep_M3(200000);
}

/*---------------------------------------------------------------------
  Function Name: InitPressure
  Description:   Initialize the pressure sensor
  Inputs:        None
  Returns:       None
-----------------------------------------------------------------------*/
void InitPressure()
{
    BYTE addr = PRES_CAL;
    SensorCal.oss = 3;  // ultra high resolution
    SensorCal.pressureConversionTime = 26;  // conversion time in milliseconds
    SensorCal.temperatureConversionTime = 5;  // conversion time in milliseconds
    I2C_WriteData(I2C1_MASTER_BASE, PRES_SLAVE_ADDR, &addr, 1);
    I2C_ReadData(I2C1_MASTER_BASE, PRES_SLAVE_ADDR, 22);	// read calibration data
}
