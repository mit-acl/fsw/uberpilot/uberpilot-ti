/*
 * sensors_m3.h
 *
 *  Created on: Jan 14, 2013
 *      Author: mark
 */

#ifndef SENSORS_M3_H_
#define SENSORS_M3_H_

#include "defines_m3.h"

void BMP180_calc_pressure(void);
void BMP180_calc_tmperature(void);


#endif /* SENSORS_M3_H_ */
