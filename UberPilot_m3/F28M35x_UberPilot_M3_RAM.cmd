/*
//###########################################################################
// FILE:    F28M35x_generic_wshared_M3_RAM.cmd
// TITLE:   Linker Command File For F28M35x examples that run out of RAM
//          This does not include flash or OTP.
//          Keep in mind that C0 and C1 are protected by the code
//          security module.
//          What this means is in most cases you will want to move to
//          another memory map file which has more memory defined.
//###########################################################################
// $TI Release: F28M35x Support Library v150 $
// $Release Date: Mon Sep 17 09:14:32 CDT 2012 $
//###########################################################################
*/

--retain=g_pfnVectors

/* The following command line options are set as part of the CCS project.    */
/* If you are building using the command line, or for some reason want to    */
/* define them here, you can uncomment and modify these lines as needed.     */
/* If you are using CCS for building, it is probably better to make any such */
/* modifications in your CCS project and leave this file alone.              */
/*                                                                           */
/* --heap_size=0                                                             */
/* --stack_size=256                                                          */
/* --library=rtsv7M3_T_le_eabi.lib                                           */


/* System memory map */

MEMORY
{
    INTVECS (RWX)    : origin = 0x20000000, length = 0x01B0
    C0 (RWX)         : origin = 0x200001B0, length = 0x1E50
    C1 (RWX)         : origin = 0x20002000, length = 0x2000
    BOOT_RSVD (RX)   : origin = 0x20004000, length = 0x0900
    C2_1 (RWX)       : origin = 0x20004900, length = 0x0700
    RESETISR (RWX)   : origin = 0x20005000, length = 0x0008
    C2_2 (RWX)       : origin = 0x20005008, length = 0x0FF8
    C3 (RWX)         : origin = 0x20006000, length = 0x2000

    S0 (RWX)         : origin = 0x20008000, length = 0xA000
    //S4 (RWX)         : origin = 0x20010000, length = 0x2000
    S5 (RWX)         : origin = 0x20012000, length = 0x2000
    S6 (RWX)         : origin = 0x20014000, length = 0x2000
    S7 (RWX)         : origin = 0x20016000, length = 0x2000

    CTOMRAM (RX)     : origin = 0x2007F000, length = 0x0800
    MTOCRAM (RWX)    : origin = 0x2007F800, length = 0x0800
}

/* Section allocation in memory */

SECTIONS
{
    .intvecs  :   > INTVECS
    .resetisr :   > RESETISR
    .text     :   >> C0 | C1 | C2_1 | C2_2 | C3 | S6 | S7
    .const    :   >> C0 | C1 | C2_1 | C2_2 | C3 | S6 | S7
    .cinit    :   >  C0 | C1 | C2_1 | C2_2 | C3 | S6 | S7
    .pinit    :   >> C0 | C1 | C2_1 | C2_2 | C3 | S6 | S7

    .vtable :   >> C0 | C1 | C2_1 | C2_2 | C3 | S6 | S7
    .data   :   >> C2_1 | C2_2 | C3 | S6 | S7
    .bss    :   >> C2_1 | C2_2 | C3 | S6 | S7
    .sysmem :   >> C0 | C1 | C2_1 | C2_2 | C3 | S6 | S7
    .stack  :   >  C0 | C1 | C2_1 | C2_2 | C3 | S6 | S7

    SHARERAMS0  : > S0
    //SHARERAMS4  : > S4
    SHARERAMS5  : > S5
    SHARERAMS6  : > S6
    SHARERAMS7  : > S7

    
    GROUP : > MTOCRAM
    {
        PUTBUFFER  
        PUTWRITEIDX
        GETREADIDX  
    }

    GROUP : > CTOMRAM 
    {
        GETBUFFER : TYPE = DSECT
        GETWRITEIDX : TYPE = DSECT
        PUTREADIDX : TYPE = DSECT
    }    
}



