#ifndef DEFINES_H_
#define DEFINES_H_

#include <math.h>
#include <string.h>

#include "inc/hw_ints.h"
#include "inc/hw_types.h"
#include "inc/hw_timer.h"
#include "inc/hw_memmap.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_nvic.h"
#include "inc/hw_gpio.h"
#include "inc/hw_ram.h"
#include "inc/hw_ipc.h"
#include "inc/hw_ssi.h"
#include "utils/uartstdio.h"
#include "driverlib/gpio.h"
#include "driverlib/debug.h"
#include "driverlib/interrupt.h"
#include "driverlib/systick.h"
#include "driverlib/timer.h"
#include "driverlib/ssi.h"
#include "driverlib/sysctl.h"
#include "driverlib/cpu.h"
#include "driverlib/ipc.h"
#include "driverlib/ram.h"
#include "driverlib/flash.h"
#include "utils/ustdlib.h"
#include "third_party/fatfs/src/ff.h"
#include "third_party/fatfs/src/diskio.h"


// local includes
#define _M3_ // define used in shared files for compiling proper code
#undef _C28_ // undefine just in case
#include "../UberPilot_shared/data_structs.h"
#include "comm_m3/I2C_m3.h"
#include "comm_m3/UART_m3.h"
#include "comm_m3/IPC_m3.h"
#include "comm_m3/SDCard.h"
#include "sensors_m3.h"

void InitHardware(void);
void InitTimers(void);
void InitIPC(void);
void InitConsole(void);
void InitIMU(void);
void InitWiiCam(void);
void InitPressure(void);
void InitCnsts(void);
void InitLEDs(void);
void InitDBG(void);
void MainSystemTimerInterrupt(void);
void sleep_M3(unsigned long ms);


//#define SERIAL_SEND_100HZ
#define SERIAL_SEND_10HZ

#define USE_SONAR_XL 	// xl-maxsonar-ez (default -- HRLV-maxsonar-ez0

#define toggle_led(x)	GPIOPinWrite(x,~GPIOPinRead(x))
#define toggle_dbg(x)	GPIOPinWrite(x,~GPIOPinRead(x))

#define LED_RED 		GPIO_PORTC_BASE,GPIO_PIN_7
#define LED_GREEN		GPIO_PORTC_BASE,GPIO_PIN_6
#define LED_YELLOW		GPIO_PORTC_BASE,GPIO_PIN_5
#define LED_BLUE		GPIO_PORTC_BASE,GPIO_PIN_4

#define DBG_PB3 		GPIO_PORTB_BASE,GPIO_PIN_3
#define DBG_PB4 		GPIO_PORTB_BASE,GPIO_PIN_4
#define DBG_PB5 		GPIO_PORTB_BASE,GPIO_PIN_5

typedef struct sLoopFlags{
    uint8_t ReadGyro;
    uint8_t ReadAccel;
    uint8_t ReadMag;
    uint8_t ReadWii;
    uint8_t GyroProp;
    uint8_t AccMagCorrect;
    uint8_t ViconCorrect;
    uint8_t AttCtl;
    uint8_t SendSerial;
    uint8_t SendSensors;
    uint8_t ReadSerial;
    uint8_t ToggleLED;
    uint8_t I2C0Recover;
    uint8_t I2C1Recover;
    uint8_t LogData;
    uint8_t StartPressure;
    uint8_t StartWait;
    uint8_t CanReadPressure;
    uint8_t ReadTemperature;
    uint8_t ReadPressure;
    uint8_t StartTemperature;
}tLoopFlags;

#endif /*DEFINES_H_*/
