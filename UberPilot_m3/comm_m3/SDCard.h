/*
 * SDCard.h
 *
 *  Created on: Jan 28, 2013
 *      Author: mark
 */

#ifndef SDCARD_H_
#define SDCARD_H_

#include "../defines_m3.h"


//*****************************************************************************
// The number of SysTick ticks per second.
//*****************************************************************************
#define TICKS_PER_SECOND 100

#define SD_TXBUF_SIZE	512	// write 512 byte chunks to SD Card
#define SD_SECTOR_HEADER_SIZE  6  	// four header bytes, two ID bytes

//*****************************************************************************
// The following are data structures used by FatFs.
//*****************************************************************************
static FATFS g_sFatFs;
static FIL g_sFileObject;

void InitSDCard(void);
void SD_StuffPacket(BYTE packetId, BYTE len, BYTE* data);
void SD_PutData(unsigned char * data, unsigned int size);
void SysTickHandler(void);
void SSI0IntHandler(void);
int Cmd_write(const char * data, int len);
int Cmd_open_file(const char * filenamemark);
int Cmd_close_file(void);


#endif /* SDCARD_H_ */
