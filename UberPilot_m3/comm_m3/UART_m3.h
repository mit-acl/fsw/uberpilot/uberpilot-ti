/*
 * UART.h
 *
 *  Created on: Oct 26, 2012
 *      Author: mark
 */

#ifndef UART_H_
#define UART_H_

// local includes
#include "../defines_m3.h"
#include "../../UberPilot_shared/data_structs.h"

// Driver includes
#include "inc/hw_uart.h"
#include "driverlib/uart.h"
#include "driverlib/udma.h"

#include<stdlib.h>


// Defines
#define STXA 0xFF
#define STXB 0xFE
#define RCXA 0x03
#define RCXB 0x12
#define SONAR_XA 0x52 // ascii 'R'
#define SONAR_XX 0x0D // ascii 'carriage return'

#define RXSTATE_STXA			0
#define RXSTATE_STXB			1
#define RXSTATE_PACKETID		2
#define RXSTATE_LEN				3
#define RXSTATE_DATA			4
#define RXSTATE_CHKSUM			5


//*****************************************************************************
// The size of the UART transmit and receive buffers.  They do not need to be
// the same size.
//*****************************************************************************
#define UART_TXBUF_SIZE         1000 //256
#define UART_RXBUF_SIZE         6 //256



// Packet definitions
struct strPacket {
    BYTE id;
    BYTE len;
    BYTE data[256];
};


// Prototypes
void InitUDMA4UART(void);
void uDMAErrorHandler(void);

void InitUART0(unsigned long buadrate);
void InitUART1(unsigned long buadrate);
void UART_FlushTX(unsigned long ulBase);
void UART_FlushTX2(unsigned long ulBase, unsigned int * txInProgress, unsigned int * txPtr, unsigned int * txBuffSel, unsigned long ulUdma_channel, unsigned char * g_ucTxBufA, unsigned char * g_ucTxBufB);
void UART0IntHandler(void);
void UART1IntHandler(void);
void setUpUART(unsigned long ulBase);
void parseUARTData(BYTE * buff, uint8_t *rxState, struct strPacket * rxPacket, uint8_t * rxCount, uint8_t * rxChkSum);

void UART_SendSensors(unsigned long ulBase);
void UART_SendState(unsigned long ulBase);
void UART_SendWii(unsigned long ulBase);
void UART_SendVoltage(unsigned long ulBase);

void UART_PutData(unsigned long ulBase, unsigned char * data, unsigned int size);
void UART_StuffPacket(unsigned long ulBase, BYTE packetId, BYTE len, BYTE* data);
void UART_ParsePacket(struct strPacket * rxPacket);


#endif /* UART_H_ */
