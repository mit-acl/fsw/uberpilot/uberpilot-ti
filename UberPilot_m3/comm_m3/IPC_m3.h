/*
 * IPC.h
 *
 *  Created on: Nov 5, 2012
 *      Author: mark
 */

#ifndef IPC_H_
#define IPC_H_

#include "../defines_m3.h"

//*****************************************************************************
// IPC Definitions
//*****************************************************************************
// see f28m35h32b.pdf for these addresses
#define M3_MTOC_PASSMSG 0x2007F800 //0x2007FFE8  // MTOC MSG RAM offsets for passing
                                    // addresses
#define M3_CTOM_PASSMSG 0x2007F000 //x2007FFE8  // MTOC MSG RAM offsets for passing
                                    // addresses

//*****************************************************************************
// Function Prototypes
//*****************************************************************************
void InitIPC(void);
void MtoCIPCBlockWrite(unsigned char id, BYTE * data, unsigned int size, unsigned int blocking);
void CtoMIPC3IntHandler(void);

#endif /* IPC_H_ */
