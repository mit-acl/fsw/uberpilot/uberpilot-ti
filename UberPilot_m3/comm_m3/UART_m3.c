/*
 * UART.c
 *
 *  Created on: Oct 26, 2012
 *      Author: mark
 */

#include "UART_m3.h"

//*****************************************************************************
// Global Variables
//*****************************************************************************
extern volatile tSensorData SensorData;
extern volatile tStatePacket StatePacket;
extern volatile tVoltagePacket VoltageData;

//*****************************************************************************
// Static Variables
//*****************************************************************************
static unsigned char g_ucTxBuf0A[UART_TXBUF_SIZE];
static unsigned char g_ucTxBuf0B[UART_TXBUF_SIZE];
static unsigned int UART0txBuffSel = 0;
static unsigned int UART0txPtr = 0;
static unsigned int UART0txInProgress = 0;

static unsigned char g_ucRxBuf0A[UART_RXBUF_SIZE];
static unsigned char g_ucRxBuf0B[UART_RXBUF_SIZE];
uint8_t rxState_uart0;
uint8_t rxChkSum_uart0;
uint8_t rxCount_uart0;
struct strPacket rxPacket_uart0;

static unsigned char g_ucTxBuf1A[UART_TXBUF_SIZE];
static unsigned char g_ucTxBuf1B[UART_TXBUF_SIZE];
static unsigned int UART1txBuffSel = 0;
static unsigned int UART1txPtr = 0;
static unsigned int UART1txInProgress = 0;

static unsigned char g_ucRxBuf1A[UART_RXBUF_SIZE];
static unsigned char g_ucRxBuf1B[UART_RXBUF_SIZE];
uint8_t rxState_uart1;
uint8_t rxChkSum_uart1;
uint8_t rxCount_uart1;
struct strPacket rxPacket_uart1;

static unsigned long g_uluDMAErrCount = 0;

//*****************************************************************************
// The control table used by the uDMA controller.  This table must be aligned
// to a 1024 byte boundary.
//*****************************************************************************
#pragma DATA_ALIGN(ucControlTable, 1024)
unsigned char ucControlTable[1024];

//*****************************************************************************
// Initilize DMA for the Uart RX and TX lines
//*****************************************************************************
void InitUDMA4UART(void)
{
    IntRegister(INT_UDMAERR, uDMAErrorHandler);

    // Enable the uDMA controller at the system level.
	SysCtlPeripheralEnable(SYSCTL_PERIPH_UDMA);

	// Enable the uDMA controller error interrupt.  This interrupt will occur
	// if there is a bus error during a transfer.
	IntEnable(INT_UDMAERR);

	// Enable the uDMA controller.
	uDMAEnable();

	// Point at the control table to use for channel control structures.
	uDMAControlBaseSet(ucControlTable);
}

//*****************************************************************************
// Initilize UART0
//*****************************************************************************
void InitUART0(unsigned long buadrate)
{
    // Register interrupt handlers in the RAM vector table
    IntRegister(INT_UART0, UART0IntHandler);

    // Enable the UART peripheral
    SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);

    // Set GPIO J2 and J3 as UART pins.
	GPIOPinTypeUART(GPIO_PORTJ_BASE, GPIO_PIN_2 | GPIO_PIN_3);
	GPIOPinConfigure(GPIO_PJ3_U0RX);
	GPIOPinConfigure(GPIO_PJ2_U0TX);

    // Configure the UART communication parameters.
    UARTConfigSetExpClk(UART0_BASE, SysCtlClockGet(SYSTEM_CLOCK_SPEED), buadrate,
                        UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
                        UART_CONFIG_PAR_NONE);

    setUpUART(UART0_BASE);
}

//*****************************************************************************
// Initilize UART1
//*****************************************************************************
void InitUART1(unsigned long buadrate)
{
    // Register interrupt handlers in the RAM vector table
    IntRegister(INT_UART1, UART1IntHandler);

    // Enable the UART peripheral
    SysCtlPeripheralEnable(SYSCTL_PERIPH_UART1);

    // Set GPIO B0 and B1 as UART pins.
	GPIOPinTypeUART(GPIO_PORTB_BASE, GPIO_PIN_0 | GPIO_PIN_1);
	GPIOPinConfigure(GPIO_PB0_U1RX);
	GPIOPinConfigure(GPIO_PB1_U1TX);

    // Configure the UART communication parameters.
    UARTConfigSetExpClk(UART1_BASE, SysCtlClockGet(SYSTEM_CLOCK_SPEED), buadrate,
                        UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
                        UART_CONFIG_PAR_NONE);

    setUpUART(UART1_BASE);

}

//*****************************************************************************
// finish initializing the UART
//*****************************************************************************
void
setUpUART(unsigned long ulBase)
{
	ASSERT(UARTBaseValid(ulBase));

    // Set both the TX and RX trigger thresholds to 4.  This will be used by
    // the uDMA controller to signal when more data should be transferred.  The
    // uDMA TX and RX channels will be configured so that it can transfer 4
    // bytes in a burst when the UART is ready to transfer more data.
    UARTFIFOLevelSet(ulBase, UART_FIFO_TX4_8, UART_FIFO_RX4_8);

    // Enable the UART for operation, and enable the uDMA interface for both TX
    // and RX channels.
    UARTEnable(ulBase);
    UARTDMAEnable(ulBase, UART_DMA_RX | UART_DMA_TX);// | UART_DMA_ERR_RXSTOP);


    unsigned long ulInt, ulChannelRX, ulChannelTX;
    unsigned char * g_ucRxBufA;
    unsigned char * g_ucRxBufB;
    if (ulBase == UART0_BASE) {
    	ulInt = INT_UART0;
		ulChannelRX = UDMA_CHANNEL_UART0RX;
		ulChannelTX = UDMA_CHANNEL_UART0TX;
		g_ucRxBufA = g_ucRxBuf0A;
		g_ucRxBufB = g_ucRxBuf0B;
    } else if (ulBase == UART1_BASE) {
    	ulInt = INT_UART1;
		ulChannelRX = UDMA_CHANNEL_UART1RX;
		ulChannelTX = UDMA_CHANNEL_UART1TX;
		g_ucRxBufA = g_ucRxBuf1A;
		g_ucRxBufB = g_ucRxBuf1B;
    }

    // Enable the UART peripheral interrupts.  Note that no UART interrupts
	// were enabled, but the uDMA controller will cause an interrupt on the
	// UART interrupt signal when a uDMA transfer is complete.
    IntEnable(ulInt);

    // debug--set up receive timeout interrupt
    //UARTIntEnable(ulBase,  UART_INT_RT);//UART_INT_RX | UART_INT_RT);

    // Put the attributes in a known state for the uDMA UART1RX channel.  These
    // should already be disabled by default.
    uDMAChannelAttributeDisable(ulChannelRX,
                                UDMA_ATTR_ALTSELECT | UDMA_ATTR_USEBURST |
                                UDMA_ATTR_HIGH_PRIORITY | UDMA_ATTR_REQMASK);

    // Configure the control parameters for the primary control structure for
    // the UART RX channel.  The primary control structure is used for the "A"
    // part of the ping-pong receive.  The transfer data size is 8 bits, the
    // source address does not increment since it will be reading from a
    // register.  The destination address increment is byte 8-bit bytes.  The
    // arbitration size is set to 4 to match the RX FIFO trigger threshold.
    // The uDMA controller will use a 4 byte burst transfer if possible.  This
    // will be somewhat more efficient that single byte transfers.
    uDMAChannelControlSet(ulChannelRX | UDMA_PRI_SELECT,
                          UDMA_SIZE_8 | UDMA_SRC_INC_NONE | UDMA_DST_INC_8 |
                          UDMA_ARB_4);

    // Configure the control parameters for the alternate control structure for
    // the UART RX channel.  The alternate control structure is used for the "B"
    // part of the ping-pong receive.  The configuration is identical to the
    // primary/A control structure.
    uDMAChannelControlSet(ulChannelRX | UDMA_ALT_SELECT,
                          UDMA_SIZE_8 | UDMA_SRC_INC_NONE | UDMA_DST_INC_8 |
                          UDMA_ARB_4);

    // Set up the transfer parameters for the UART RX primary control
    // structure.  The mode is set to ping-pong, the transfer source is the
    // UART data register, and the destination is the receive "A" buffer.  The
    // transfer size is set to match the size of the buffer.
    uDMAChannelTransferSet(ulChannelRX | UDMA_PRI_SELECT,
                           UDMA_MODE_PINGPONG,
                           (void *)(ulBase + UART_O_DR),
                           g_ucRxBufA, sizeof(g_ucRxBuf0A));

    // Set up the transfer parameters for the UART RX alternate control
    // structure.  The mode is set to ping-pong, the transfer source is the
    // UART data register, and the destination is the receive "B" buffer.  The
    // transfer size is set to match the size of the buffer.
    uDMAChannelTransferSet(ulChannelRX | UDMA_ALT_SELECT,
                           UDMA_MODE_PINGPONG,
                           (void *)(ulBase + UART_O_DR),
                           g_ucRxBufB, sizeof(g_ucRxBuf0B));

    // Put the attributes in a known state for the uDMA UART1TX channel.  These
    // should already be disabled by default.
    uDMAChannelAttributeDisable(ulChannelTX,
                                UDMA_ATTR_ALTSELECT |
                                UDMA_ATTR_HIGH_PRIORITY | UDMA_ATTR_REQMASK);

    // Set the USEBURST attribute for the uDMA UART TX channel.  This will
    // force the controller to always use a burst when transferring data from
    // the TX buffer to the UART.  This is somewhat more efficient bus usage
    // than the default which allows single or burst transfers.
    uDMAChannelAttributeEnable(ulChannelTX, UDMA_ATTR_USEBURST);

    // Configure the control parameters for the UART TX.  The uDMA UART TX
    // channel is used to transfer a block of data from a buffer to the UART.
    // The data size is 8 bits.  The source address increment is 8-bit bytes
    // since the data is coming from a buffer.  The destination increment is
    // none since the data is to be written to the UART data register.  The
    // arbitration size is set to 4, which matches the UART TX FIFO trigger
    // threshold.
    uDMAChannelControlSet(ulChannelTX | UDMA_PRI_SELECT,
                          UDMA_SIZE_8 | UDMA_SRC_INC_8 | UDMA_DST_INC_NONE |
                          UDMA_ARB_4);

    // Set up the transfer parameters for the uDMA UART TX channel.  This will
    // configure the transfer source and destination and the transfer size.
    // Basic mode is used because the peripheral is making the uDMA transfer
    // request.  The source is the TX buffer and the destination is the UART
    // data register.
    //uDMAChannelTransferSet(ulChannelTX | UDMA_PRI_SELECT,
    //                       UDMA_MODE_BASIC, g_ucTxBuf,
    //                       (void *)(ulBase + UART_O_DR),
    //                       sizeof(g_ucTxBuf));

    // Now both the uDMA UART TX and RX channels are primed to start a
    // transfer.  As soon as the channels are enabled, the peripheral will
    // issue a transfer request and the data transfers will begin.
    uDMAChannelEnable(ulChannelRX);
    //uDMAChannelEnable(ulChannelTX);
}

//*****************************************************************************
// The interrupt handler for uDMA errors.  This interrupt will occur if the
// uDMA encounters a bus error while trying to perform a transfer.  This
// handler just increments a counter if an error occurs.
//*****************************************************************************
void
uDMAErrorHandler(void)
{
    unsigned long ulStatus;

    // Check for uDMA error bit
    ulStatus = uDMAErrorStatusGet();

    // If there is a uDMA error, then clear the error and increment
    // the error counter.
    if(ulStatus)
    {
        uDMAErrorStatusClear();
        g_uluDMAErrCount++;
    }
}


void
UART0IntHandler(void)
{
    unsigned long ulStatus;
    unsigned long ulMode;
    BYTE * buff;
    int data_received = 0;

    // Read the interrupt status of the UART.
    ulStatus = UARTIntStatus(UART0_BASE, 1);

    // Clear any pending status, even though there should be none since no UART
    // interrupts were enabled.  If UART error interrupts were enabled, then
    // those interrupts could occur here and should be handled.  Since uDMA is
    // used for both the RX and TX, then neither of those interrupts should be
    // enabled.
    UARTIntClear(UART0_BASE, ulStatus);

    // Check the DMA control table to see if the ping-pong "A" transfer is
    // complete.  The "A" transfer uses receive buffer "A", and the primary
    // control structure.
    ulMode = uDMAChannelModeGet(UDMA_CHANNEL_UART0RX | UDMA_PRI_SELECT);

    // If the primary control structure indicates stop, that means the "A"
    // receive buffer is done.  The uDMA controller should still be receiving
    // data into the "B" buffer.
    if(ulMode == UDMA_MODE_STOP)
    {

        // Set up the next transfer for the "A" buffer, using the primary
        // control structure.  When the ongoing receive into the "B" buffer is
        // done, the uDMA controller will switch back to this one.  This
        // example re-uses buffer A, but a more sophisticated application could
        // use a rotating set of buffers to increase the amount of time that
        // the main thread has to process the data in the buffer before it is
        // reused.
        uDMAChannelTransferSet(UDMA_CHANNEL_UART0RX | UDMA_PRI_SELECT,
                               UDMA_MODE_PINGPONG,
                               (void *)(UART0_BASE + UART_O_DR),
                               g_ucRxBuf0A, sizeof(g_ucRxBuf0A));

        buff = (BYTE *)g_ucRxBuf0A;
        data_received = 1;

    }

    // Check the DMA control table to see if the ping-pong "B" transfer is
    // complete.  The "B" transfer uses receive buffer "B", and the alternate
    // control structure.
    ulMode = uDMAChannelModeGet(UDMA_CHANNEL_UART0RX | UDMA_ALT_SELECT);

    // If the alternate control structure indicates stop, that means the "B"
    // receive buffer is done.  The uDMA controller should still be receiving
    // data into the "A" buffer.
    if(ulMode == UDMA_MODE_STOP)
    {

        // Set up the next transfer for the "B" buffer, using the alternate
        // control structure.  When the ongoing receive into the "A" buffer is
        // done, the uDMA controller will switch back to this one.  This
        // example re-uses buffer B, but a more sophisticated application could
        // use a rotating set of buffers to increase the amount of time that
        // the main thread has to process the data in the buffer before it is
        // reused.
        uDMAChannelTransferSet(UDMA_CHANNEL_UART0RX | UDMA_ALT_SELECT,
                               UDMA_MODE_PINGPONG,
                               (void *)(UART0_BASE + UART_O_DR),
                               g_ucRxBuf0B, sizeof(g_ucRxBuf0B));

        buff = (BYTE *)g_ucRxBuf0B;
        data_received = 1;
    }

    if (data_received == 1)
    	parseUARTData(buff, &rxState_uart0, &rxPacket_uart0, &rxCount_uart0, &rxChkSum_uart0);

    // If the UART0 DMA TX channel is disabled, that means the TX DMA transfer
    // is done.
    if(!uDMAChannelIsEnabled(UDMA_CHANNEL_UART0TX))
    {
        UART0txInProgress = 0;
    }
}

void
parseUARTData(BYTE * buff, uint8_t *rxState, struct strPacket * rxPacket, uint8_t * rxCount, uint8_t * rxChkSum)
{
	int i;
	BYTE b;
	for(i=0;i<UART_RXBUF_SIZE;i++)
	{
		// Get the next byte
		b = buff[i];

		// Determine what to do with received character
		switch(*rxState){
			case RXSTATE_STXA:
				if (b == STXA){
					*rxState = RXSTATE_STXB;
				} else if (b == SONAR_XA) { // this is a sonar packet
					rxPacket->id = PACKETID_SONAR;
#ifdef USE_SONAR_XL
					rxPacket->len = 0x03; // xl-maxsonar-ez has 3 ascii chars (distance in cm)
#else
					rxPacket->len = 0x04; // hrlv-maxsonar-ez sonar  has 4 ascii chars (distance in mm)
#endif
					*rxCount = 0;
					*rxState = RXSTATE_DATA;
				}
				break;
			case RXSTATE_STXB:
				if (b == STXA)
						*rxState = RXSTATE_STXB;
				else if (b == STXB)
						*rxState = RXSTATE_PACKETID;
				else
						*rxState = RXSTATE_STXA;
				break;
			case RXSTATE_PACKETID:
				rxPacket->id = b;
				*rxState = RXSTATE_LEN;
				*rxChkSum = b;
				break;
			case RXSTATE_LEN:
				rxPacket->len = b;
				*rxState = RXSTATE_DATA;
				*rxCount = 0;
				*rxChkSum += b;
				break;
			case RXSTATE_DATA:
				rxPacket->data[*rxCount] = b;
				(*rxCount)++;
				if (*rxCount == rxPacket->len) *rxState = RXSTATE_CHKSUM;
				*rxChkSum += b;
				break;
			case RXSTATE_CHKSUM:
				*rxState = RXSTATE_STXA;
				*rxChkSum += b;
				if (*rxChkSum == 0 || b == SONAR_XX) { // end if checksum met or if we receive carriage return
					// Good packet received, execute it
					UART_ParsePacket(rxPacket);
				}
				else
					asm("NOP");
				break;
			default:
					break;
		}
	}
}

//*****************************************************************************
// The interrupt handler for UART1.  This interrupt will occur when a DMA
// transfer is complete using the UART1 uDMA channel.  It will also be
// triggered if the peripheral signals an error.  This interrupt handler will
// switch between receive ping-pong buffers A and B.  It will also restart a TX
// uDMA transfer if the prior transfer is complete.  This will keep the UART
// running continuously (looping TX data back to RX).
//*****************************************************************************
// TODO: this implementation won't process data until the full packet is received and dma interrupt is processed -- fix
// TODO: debug overflow issues when debugging.  Sometimes this interrupt handler just stops being called after pausing the
// program but still having the sonar broadcasting
void
UART1IntHandler(void)
{
    unsigned long ulStatus;
    unsigned long ulMode;
    BYTE * buff;
    int data_received = 0;

    // Read the interrupt status of the UART.
    ulStatus = UARTIntStatus(UART1_BASE, 1);

    // Clear any pending status, even though there should be none since no UART
    // interrupts were enabled.  If UART error interrupts were enabled, then
    // those interrupts could occur here and should be handled.  Since uDMA is
    // used for both the RX and TX, then neither of those interrupts should be
    // enabled.
    UARTIntClear(UART1_BASE, ulStatus);

    // debug test -- maybe to a memcpy here to get all the data into a buffer?
    // if not, this will probably work fine
//    long tmp;
//    while(UARTCharsAvail(UART1_BASE)){
//    	tmp = UARTCharGetNonBlocking(UART1_BASE);
//    }

    // Check the DMA control table to see if the ping-pong "A" transfer is
    // complete.  The "A" transfer uses receive buffer "A", and the primary
    // control structure.
    ulMode = uDMAChannelModeGet(UDMA_CHANNEL_UART1RX | UDMA_PRI_SELECT);

    // If the primary control structure indicates stop, that means the "A"
    // receive buffer is done.  The uDMA controller should still be receiving
    // data into the "B" buffer.
    if(ulMode == UDMA_MODE_STOP)
    {

        // Set up the next transfer for the "A" buffer, using the primary
        // control structure.  When the ongoing receive into the "B" buffer is
        // done, the uDMA controller will switch back to this one.  This
        // example re-uses buffer A, but a more sophisticated application could
        // use a rotating set of buffers to increase the amount of time that
        // the main thread has to process the data in the buffer before it is
        // reused.
        uDMAChannelTransferSet(UDMA_CHANNEL_UART1RX | UDMA_PRI_SELECT,
                               UDMA_MODE_PINGPONG,
                               (void *)(UART1_BASE + UART_O_DR),
                               g_ucRxBuf1A, sizeof(g_ucRxBuf1A));

        buff = (BYTE *)g_ucRxBuf1A;
        data_received = 1;

    }

    // Check the DMA control table to see if the ping-pong "B" transfer is
    // complete.  The "B" transfer uses receive buffer "B", and the alternate
    // control structure.
    ulMode = uDMAChannelModeGet(UDMA_CHANNEL_UART1RX | UDMA_ALT_SELECT);

    // If the alternate control structure indicates stop, that means the "B"
    // receive buffer is done.  The uDMA controller should still be receiving
    // data into the "A" buffer.
    if(ulMode == UDMA_MODE_STOP)
    {

        // Set up the next transfer for the "B" buffer, using the alternate
        // control structure.  When the ongoing receive into the "A" buffer is
        // done, the uDMA controller will switch back to this one.  This
        // example re-uses buffer B, but a more sophisticated application could
        // use a rotating set of buffers to increase the amount of time that
        // the main thread has to process the data in the buffer before it is
        // reused.
        uDMAChannelTransferSet(UDMA_CHANNEL_UART1RX | UDMA_ALT_SELECT,
                               UDMA_MODE_PINGPONG,
                               (void *)(UART1_BASE + UART_O_DR),
                               g_ucRxBuf1B, sizeof(g_ucRxBuf1B));

        buff = (BYTE *)g_ucRxBuf1B;
        data_received = 1;
    }

    if (data_received == 1)
    	parseUARTData(buff, &rxState_uart1, &rxPacket_uart1, &rxCount_uart1, &rxChkSum_uart1);


    // If the UART1 DMA TX channel is disabled, that means the TX DMA transfer
    // is done.
    if(!uDMAChannelIsEnabled(UDMA_CHANNEL_UART1TX))
    {
        UART1txInProgress = 0;
    }
}


/*---------------------------------------------------------------------
  Function Name: UART_ParsePacket
  Description:   Parses and rescales a good data packet
  Inputs:        Pointer to the received data packet to be parsed
  Returns:       None
-----------------------------------------------------------------------*/
volatile tPosVelPacket * posVelPacket;
volatile tPosPacket * posPacket;
volatile tAttCmdPacket * attCmdPacket;
volatile tAttZCmdPacket * attZCmdPacket;
volatile tPosYawCmdPacket * posYawCmdPacket;
volatile tSensorCalPacket * sensorCalPacket;
volatile tViconStatePacket * viconStatePacket;
void UART_ParsePacket(struct strPacket * rxPacket){
    switch(rxPacket->id){
    case PACKETID_SENSORCAL:
    {
    	toggle_led(LED_BLUE);
      sensorCalPacket = (tSensorCalPacket*) rxPacket->data; 
      MtoCIPCBlockWrite(PACKETID_SENSORCAL, (void*) sensorCalPacket, sizeof(tSensorCalPacket), 0); // send sensor calibration values to C28

      // write sensor calibration values to eeprom
      BYTE data[2+sizeof(tSensorCalPacket)];
	  data[0] = EEPROM_SENSORCAL_ADDR >> 8;
	  data[1] = EEPROM_SENSORCAL_ADDR;
	  memcpy(data+2, (void*) &sensorCalPacket->gyroXScale, sizeof(tSensorCalPacket));
	  I2C_WriteData(I2C1_MASTER_BASE, EEPROM_SLAVE_ADDR, data, 2+sizeof(tSensorCalPacket));
        break;
    }
    case PACKETID_ATT_CMD:
    {
    	attCmdPacket = (tAttCmdPacket*) rxPacket->data;
    	MtoCIPCBlockWrite(PACKETID_ATT_CMD, (void*) attCmdPacket, sizeof(tAttCmdPacket), 0); // send attitude command to C28
    	SD_StuffPacket(PACKETID_ATT_CMD, sizeof(tAttCmdPacket), (BYTE *)attCmdPacket);
        break;
    }
    case PACKETID_ATT_Z_CMD:
    {
    	attZCmdPacket = (tAttZCmdPacket*) rxPacket->data;
    	MtoCIPCBlockWrite(PACKETID_ATT_Z_CMD, (void*) attZCmdPacket, sizeof(tAttZCmdPacket), 0); // send attitude command to C28
    	SD_StuffPacket(PACKETID_ATT_Z_CMD, sizeof(tAttZCmdPacket), (BYTE *)attZCmdPacket);
        break;
    }
    case PACKETID_POS_YAW_CMD:
    {
    	posYawCmdPacket = (tPosYawCmdPacket*) rxPacket->data;
    	MtoCIPCBlockWrite(PACKETID_POS_YAW_CMD, (void*) posYawCmdPacket, sizeof(tPosYawCmdPacket), 0); // send position command to C28
    	SD_StuffPacket(PACKETID_POS_YAW_CMD, sizeof(tPosYawCmdPacket), (BYTE *)posYawCmdPacket);
    	break;
    }
    case PACKETID_GAINS:
    {
//        gainsPacket = (tGainsPacket*) rxPacket->data;

        /*// gains are scaled by 10
        tmp = GAINS_SCALE;
        int16toQ16(&div, &tmp);
        int16toQ16(&Gains.Kp_roll, &gainsPacket->Kp_roll); Gains.Kp_roll = _IQ16div(Gains.Kp_roll, div);
        int16toQ16(&Gains.Kd_roll, &gainsPacket->Kd_roll); Gains.Kd_roll = _IQ16div(Gains.Kd_roll, div);
        int16toQ16(&Gains.Ki_roll, &gainsPacket->Ki_roll); Gains.Ki_roll = _IQ16div(Gains.Ki_roll, div);

        int16toQ16(&Gains.Kp_pitch, &gainsPacket->Kp_pitch); Gains.Kp_pitch = _IQ16div(Gains.Kp_pitch, div);
        int16toQ16(&Gains.Kd_pitch, &gainsPacket->Kd_pitch); Gains.Kd_pitch = _IQ16div(Gains.Kd_pitch, div);
        int16toQ16(&Gains.Ki_pitch, &gainsPacket->Ki_pitch); Gains.Ki_pitch = _IQ16div(Gains.Ki_pitch, div);

        int16toQ16(&Gains.Kp_yaw, &gainsPacket->Kp_yaw); Gains.Kp_yaw = _IQ16div(Gains.Kp_yaw, div);
        int16toQ16(&Gains.Kd_yaw, &gainsPacket->Kd_yaw); Gains.Kd_yaw = _IQ16div(Gains.Kd_yaw, div);
        int16toQ16(&Gains.Ki_yaw, &gainsPacket->Ki_yaw); Gains.Ki_yaw = _IQ16div(Gains.Ki_yaw, div);

        int16toQ16(&Gains.PWM1_trim, &gainsPacket->Servo1_trim);
        int16toQ16(&Gains.PWM2_trim, &gainsPacket->Servo2_trim);
        int16toQ16(&Gains.PWM3_trim, &gainsPacket->Servo3_trim);
        int16toQ16(&Gains.PWM4_trim, &gainsPacket->Servo4_trim);

        int16toQ16(&Gains.maxang, &gainsPacket->maxang);  Gains.maxang = _IQ16div(Gains.maxang, div);
        int16toQ16(&Gains.motorFF, &gainsPacket->motorFF); Gains.motorFF = _IQ16div(_IQ16div(Gains.motorFF, div),div);
        Gains.lowBatt = gainsPacket->lowBatt;
        Gains.stream_data = gainsPacket->stream_data;*/

        break;
    }
    case PACKETID_SONAR:
    {
        SensorData.sonarZ = atoi(rxPacket->data);
        //toggle_dbg(DBG_PB3);

#ifdef USE_SONAR_XL
        int32_t z = (SensorData.sonarZ*POS_SCALE)/100; // convert to meters and then scale by POS_SCALE
#else
        int32_t z = (SensorData.sonarZ*POS_SCALE)/1000; // convert to meters and then scale by POS_SCALE
#endif
        MtoCIPCBlockWrite(PACKETID_ALT, (void*) &z, sizeof(tAltPacket), 0); // sonar info to c28
        // todo: combine the sonar with the pressure data, filter on M3 side, then send filtered value to C28
        SD_StuffPacket(PACKETID_ALT, sizeof(tAltPacket), (BYTE *)&z);
        break;
    }
    case PACKETID_POSVEL:
    {
    	posVelPacket = (tPosVelPacket*) rxPacket->data;
    	// simulate sending position update // todo: this will happen from within i2c parser
		MtoCIPCBlockWrite(PACKETID_POSVEL, (void*) posVelPacket, sizeof(tPosVelPacket), 0);
		SD_StuffPacket(PACKETID_POSVEL, sizeof(tPosVelPacket), (BYTE *)posVelPacket);
		break;
    }
    case PACKETID_POS:
	{
		posPacket = (tPosPacket*) rxPacket->data;
		// simulate sending position update // todo: this will happen from within i2c parser
		MtoCIPCBlockWrite(PACKETID_POS, (void*) posPacket, sizeof(tPosPacket), 0);
		SD_StuffPacket(PACKETID_POS, sizeof(tPosPacket), (BYTE *)posPacket);
		break;
	}
    case PACKETID_VICON_STATE:
	{
		viconStatePacket = (tViconStatePacket*) rxPacket->data;
		MtoCIPCBlockWrite(PACKETID_VICON_STATE, (void*) viconStatePacket, sizeof(tViconStatePacket), 0);
		SD_StuffPacket(PACKETID_VICON_STATE, sizeof(tViconStatePacket), (BYTE *)viconStatePacket);
		break;
	}
    default:
    {
        break;
    }
    }
}

void UART_SendState(unsigned long ulBase){
	// Check the arguments.
	ASSERT(UARTBaseValid(ulBase));

    UART_StuffPacket(ulBase, PACKETID_STATE, sizeof(tStatePacket), (BYTE *) &StatePacket);
}

void UART_SendSensors(unsigned long ulBase){
	// Check the arguments.
	ASSERT(UARTBaseValid(ulBase));

	tSensorsPacket pkt;
    pkt.gyroX = SensorData.gyroX;
    pkt.gyroY = SensorData.gyroY;
    pkt.gyroZ = SensorData.gyroZ;
    pkt.accelX = SensorData.accX;
    pkt.accelY = SensorData.accY;
    pkt.accelZ = SensorData.accZ;
    pkt.magX = SensorData.magX;
    pkt.magY = SensorData.magY;
    pkt.magZ = SensorData.magZ;
    pkt.sonarZ = SensorData.sonarZ;

    UART_StuffPacket(ulBase, PACKETID_SENSORS, sizeof(tSensorsPacket), (BYTE *) &pkt);
}

void UART_SendWii(unsigned long ulBase){
	// Check the arguments.
	ASSERT(UARTBaseValid(ulBase));

	tWiiPacket pkt;
	pkt.x1 =   (uint16_t) SensorData.blob1.X;
	pkt.y1 =   (uint16_t) SensorData.blob1.Y;
	pkt.size1 = (uint8_t) SensorData.blob1.Size;
	pkt.x2 =   (uint16_t) SensorData.blob2.X;
	pkt.y2 =   (uint16_t) SensorData.blob2.Y;
	pkt.size2 = (uint8_t) SensorData.blob2.Size;
	pkt.x3 =   (uint16_t) SensorData.blob3.X;
	pkt.y3 =   (uint16_t) SensorData.blob3.Y;
	pkt.size3 = (uint8_t) SensorData.blob3.Size;
	pkt.x4 =   (uint16_t) SensorData.blob4.X;
	pkt.y4 =   (uint16_t) SensorData.blob4.Y;
	pkt.size4 = (uint8_t) SensorData.blob4.Size;
	pkt.blobcount =       SensorData.blobcount;

	UART_StuffPacket(ulBase, PACKETID_WII, sizeof(tWiiPacket), (BYTE *) &pkt);
}

void UART_SendVoltage(unsigned long ulBase){
	// Check the arguments.
	ASSERT(UARTBaseValid(ulBase));

	UART_StuffPacket(ulBase, PACKETID_VOLTAGE, sizeof(tVoltagePacket), (BYTE *) &VoltageData);
}

void UART_StuffPacket(unsigned long ulBase, BYTE packetId, BYTE len, BYTE* data)
{
	// Check the arguments.
	ASSERT(UARTBaseValid(ulBase));

    // Start Checksum
    BYTE chksum = 0;
    BYTE ptr = 0;
    BYTE i;
    BYTE TxBuff[256];

    if (len > 240)
        return;

    // STX
    TxBuff[ptr++] = STXA;
    TxBuff[ptr++] = STXB;

    // Packet Id
    TxBuff[ptr++] = packetId;
    chksum += packetId;

    // Packet Data Length
    TxBuff[ptr++] = len;
    chksum += len;

    // Stuff Data
    for(i = 0; i < len; i++)
    {
        // Send Byte
        TxBuff[ptr++] = data[i];
        chksum += data[i];
    }

    // Checksum Calculate and Send
    chksum ^= 0xFF;
    chksum += 1;
    TxBuff[ptr++] = chksum;

    UART_PutData(ulBase, TxBuff, ptr);
}
//======================================================================

void UART_PutData(unsigned long ulBase, unsigned char * data, unsigned int size){
	// Check the arguments.
	ASSERT(UARTBaseValid(ulBase));

	unsigned char * g_ucTxBufA;
	unsigned char * g_ucTxBufB;
	unsigned int txBuffSel;
	unsigned int * txPtr;
	if (ulBase == UART0_BASE){
		g_ucTxBufA = g_ucTxBuf0A;
		g_ucTxBufB = g_ucTxBuf0B;
		txBuffSel = UART0txBuffSel;
		txPtr = &UART0txPtr;
	} else if (ulBase == UART1_BASE){
		g_ucTxBufA = g_ucTxBuf1A;
		g_ucTxBufB = g_ucTxBuf1B;
		txBuffSel = UART1txBuffSel;
		txPtr = &UART1txPtr;
	}

	// Select the active DMA TX buffer
	unsigned char * activeBuff;
	if(txBuffSel == 0){
		activeBuff = g_ucTxBufA;
	}else{
		activeBuff = g_ucTxBufB;
	}


	// Copy data into the DMA TX buffer
	if (*txPtr+size <= UART_TXBUF_SIZE){
		int i;
		for(i=0; i <size; i++)
		{
			activeBuff[*txPtr] = data[i];
			(*txPtr)++;
		}
	}else
	{
		// TODO: handle overflow here, just ignoring data for now if we overflow
	}

}



void UART_FlushTX(unsigned long ulBase){
	// Check the arguments.
	ASSERT(UARTBaseValid(ulBase));

	if (ulBase == UART0_BASE)
	{
		UART_FlushTX2(ulBase, &UART0txInProgress, &UART0txPtr, &UART0txBuffSel, UDMA_CHANNEL_UART0TX, g_ucTxBuf0A, g_ucTxBuf0B);
	} else if (ulBase == UART1_BASE)
	{
		UART_FlushTX2(ulBase, &UART1txInProgress, &UART1txPtr, &UART1txBuffSel, UDMA_CHANNEL_UART1TX, g_ucTxBuf1A, g_ucTxBuf1B);
	}

}


void UART_FlushTX2(unsigned long ulBase, unsigned int * txInProgress, unsigned int * txPtr, unsigned int * txBuffSel, unsigned long ulUdma_channel, unsigned char * g_ucTxBufA, unsigned char * g_ucTxBufB){

    // Transfer in progress?
    if(*txInProgress)
        return;

    // Are there bytes to send?
    if(*txPtr == 0)
        return;

    // Set up the transfer parameters for the uDMA UART TX channel.  This will
	// configure the transfer source and destination and the transfer size.
	// Basic mode is used because the peripheral is making the uDMA transfer
	// request.  The source is the TX buffer and the destination is the UART
	// data register.
    if(*txBuffSel == 0){
    	uDMAChannelTransferSet(ulUdma_channel | UDMA_PRI_SELECT,
    	                       UDMA_MODE_BASIC, g_ucTxBufA,
    	                       (void *)(ulBase + UART_O_DR),
    	                       *txPtr); // number of transfer bytes
    	uDMAChannelEnable(ulUdma_channel);
        *txBuffSel = 1;
    }else{
    	uDMAChannelTransferSet(ulUdma_channel | UDMA_PRI_SELECT,
    	    	                       UDMA_MODE_BASIC, g_ucTxBufB,
    	    	                       (void *)(ulBase + UART_O_DR),
    	    	                       *txPtr); // number of transfer bytes
		uDMAChannelEnable(ulUdma_channel);
        *txBuffSel = 0;
    }

    *txPtr = 0;
    *txInProgress = 1;
}
