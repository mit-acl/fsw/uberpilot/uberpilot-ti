#include "I2C_m3.h"

//======================================================================
//	Variables
//----------------------------------------------------------------------

extern volatile tLoopFlags loop;
extern volatile tM3SensorCal SensorCal;
extern volatile tSensorCalPacket SensorCalPacket;
extern volatile unsigned int fileID;

// I2C
volatile struct strI2Ccmd cmdBuff[2][I2C_CMD_CNT_MAX];
volatile uint32_t _cmdWrPtr[2];
volatile uint32_t _cmdRdPtr[2];
volatile uint32_t _cmdBufCnt[2];

volatile uint32_t _i2c_state[2];
volatile uint32_t _i2c_enabled[2] = {0,0};
volatile uint32_t _data_cnt[2];

void I2C0_Recover(void)
{

	I2C_Recover(I2C0_MASTER_BASE, GPIO_PORTB_BASE, GPIO_PIN_6, GPIO_PIN_7);

    // Re-initialize bus
    I2C0_Init();
}

void I2C1_Recover(void)
{

//	PD6_GPIO22 - I2C1SDA
//	PD7_GPIO23 - I2C1SCL
	I2C_Recover(I2C1_MASTER_BASE, GPIO_PORTD_BASE, GPIO_PIN_6, GPIO_PIN_7);

    // Re-initialize bus
    I2C1_Init();
}

void I2C_Recover(unsigned long ulBase, unsigned long ulPort, unsigned char SDA, unsigned char SCL)
{
    // Disable the I2C interrupt and clear flags
	I2CMasterIntDisable(ulBase);
	I2CMasterIntClear(ulBase);

    // Float the data line and pulse the clock a few times
    // This should get the slave state machine back to normal
    // SDA0 = PB6
    // SCL0 = PB7
	GPIOPinTypeGPIOOutput(ulPort, SCL);
	GPIOPinTypeGPIOInput(ulPort, SDA);

    int i,j;
    for(i=0; i < 32; i++){
    	GPIOPinWrite(ulPort, SCL, 0);
        for(j=0;j<10;j++){}

        GPIOPinWrite(ulPort, SCL, ~0);
        for(j=0;j<10;j++){}

    }

    // Fake an I2C stop condition
    GPIOPinTypeGPIOOutput(ulPort, SDA);
    GPIOPinWrite(ulPort, SDA, 0);
    for(j=0;j<50;j++){}
    GPIOPinWrite(ulPort, SDA, ~0);

    for(j=0;j<50;j++){}

    GPIOPinTypeGPIOInput(ulPort, SDA | SCL);
}

/*---------------------------------------------------------------------
 Function Name: I2C0_Init
 Description:   Initialize i2c_0 communication at ~400kbit/s
 Inputs:        None
 Returns:       None
 -----------------------------------------------------------------------*/
void I2C0_Init(void) {

	// The I2C0 peripheral must be enabled before use.
	SysCtlPeripheralEnable(SYSCTL_PERIPH_I2C0);

	// Configure the pin muxing for I2C0 functions on port B2 and B3.
	GPIOPinUnlock(GPIO_PORTB_BASE, GPIO_PIN_7); // PB7 is by default locked
	GPIOPinConfigure(GPIO_PB6_I2C0SDA);
	GPIOPinConfigure(GPIO_PB7_I2C0SCL);

	// Select the I2C function for these pins.  This function will also
	// configure the GPIO pins pins for I2C operation, setting them to
	// open-drain operation with weak pull-ups.  Consult the data sheet
	// to see which functions are allocated per pin.
	GPIOPinTypeI2C(GPIO_PORTB_BASE, GPIO_PIN_6 | GPIO_PIN_7);

	I2C_Init(I2C0_MASTER_BASE, &I2C0_IntHandler);

}

/*---------------------------------------------------------------------
 Function Name: I2C1_Init
 Description:   Initialize i2c_1 communication at ~400kbit/s
 Inputs:        None
 Returns:       None
 -----------------------------------------------------------------------*/
void I2C1_Init(void) {

	// The I2C1 peripheral must be enabled before use.
	SysCtlPeripheralEnable(SYSCTL_PERIPH_I2C1);

	// Configure the pin muxing for I2C1 functions on port D6 and D7.
	GPIOPinConfigure(GPIO_PD6_I2C1SDA);
	GPIOPinConfigure(GPIO_PD7_I2C1SCL);

	// Select the I2C function for these pins.  This function will also
	// configure the GPIO pins pins for I2C operation, setting them to
	// open-drain operation with weak pull-ups.  Consult the data sheet
	// to see which functions are allocated per pin.
	GPIOPinTypeI2C(GPIO_PORTD_BASE, GPIO_PIN_6 | GPIO_PIN_7);

	I2C_Init(I2C1_MASTER_BASE, &I2C1_IntHandler);

}

/*---------------------------------------------------------------------
 Function Name: I2C_Init
 Description:   Initialize i2c communication at ~400kbit/s
 Inputs:        ulBase - I2C0_MASTER_BASE or I2C1_MASTER_BASE
 	 	 	 	fnHandler - function handle to interrupt
 Returns:       None
 -----------------------------------------------------------------------*/
void I2C_Init(unsigned long ulBase, void(fnHandler) (void)) {

	// Check the arguments.
	ASSERT((ulBase == I2C0_MASTER_BASE) || (ulBase == I2C1_MASTER_BASE));

	int i2cModule;
	if (ulBase == I2C0_MASTER_BASE)
		i2cModule = 0;
	else
		i2cModule = 1;

	// Enable and initialize the I2C master module.  Use the system clock for
	// the I2C module.  The last parameter sets the I2C data transfer rate.
	// If false the data rate is set to 100kbps and if true the data rate will
	// be set to 400kbps.
	I2CMasterInitExpClk(ulBase, SysCtlClockGet(SYSTEM_CLOCK_SPEED),
			true);

	// TODO: figure out why we need to override MTPR here to actually get to 400kpbs
	//HWREG(I2C0_MASTER_BASE + I2C_O_MTPR) = 0x0A;//0x5;  // roughly 400kbps

	// Bind function to interrupt handler
	I2CIntRegister(ulBase, fnHandler);

	// Enable I2C0 interrupts for interrupt based i2c communication
	I2CMasterIntEnable(ulBase);

	// Initialize Variables
	_i2c_enabled[i2cModule] = 1;
	_cmdWrPtr[i2cModule] = 0;
	_cmdRdPtr[i2cModule] = 0;
	_cmdBufCnt[i2cModule] = 0;
	_i2c_state[i2cModule] = I2C_STATE_IDLE;
}

/*---------------------------------------------------------------------
 Function Name: I2C_WriteData
 Description:   Send data over i2c line
 Inputs:		ulBase -  I2C0_MASTER_BASE or I2C1_MASTER_BASE
 slave - address to sent data to
 data - pointer to data array being sent
 len - length of data array
 Returns:       None
 -----------------------------------------------------------------------*/
void I2C_WriteData(unsigned long ulBase, BYTE slave, BYTE* data, BYTE len) {

	if (len > I2C_PACKET_SIZE_MAX)
		return;

	I2C_StuffData(ulBase, slave, data, len, 0);
}


/*---------------------------------------------------------------------
 Function Name: I2C_ReadData
 Description:   Read data over i2c line
 Inputs:		ulBase -  I2C0_MASTER_BASE or I2C1_MASTER_BASE
 	 	 	 	slave - address to read data from
 	 	 	 	len - length of data array
 Returns:       None
 -----------------------------------------------------------------------*/
void I2C_ReadData(unsigned long ulBase, BYTE slave, BYTE len) {

	if (len > I2C_PACKET_SIZE_MAX)
		return;
	if (len < 1)
		return;

	I2C_StuffData(ulBase, slave, 0, len, 1);
}


/*---------------------------------------------------------------------
 Function Name: I2C_StuffData
 Description:   Stuff data for transmission
 Inputs:        ulBase -  I2C0_MASTER_BASE or I2C1_MASTER_BASE
 	 	 	 	addr - address to read from/write to
 	 	 	 	data - pointer to data to send (if applicable)
 	 	 	 	len - length of data array to be read
 	 	 	 	txrx - 1 for receive, 0 for transmit
 Returns:       None
 -----------------------------------------------------------------------*/
void I2C_StuffData(unsigned long ulBase, BYTE addr, BYTE* data, BYTE len, BYTE txrx) {

	// Check the arguments.
	ASSERT((ulBase == I2C0_MASTER_BASE) || (ulBase == I2C1_MASTER_BASE));

	int i2cModule;
	if (ulBase == I2C0_MASTER_BASE)
		i2cModule = 0;
	else
		i2cModule = 1;

	// Disable Interrupt
	I2CMasterIntDisable(ulBase);

	// If command buffer has room
	if (_cmdBufCnt[i2cModule] < I2C_CMD_CNT_MAX) {
		// Stuff Packet
		cmdBuff[i2cModule][_cmdWrPtr[i2cModule]].slaveId = addr;
		cmdBuff[i2cModule][_cmdWrPtr[i2cModule]].len = len;
		cmdBuff[i2cModule][_cmdWrPtr[i2cModule]].receive = txrx;

		if (!txrx){ // transmit data
			int i;
			for (i = 0; i < len; i++)
				cmdBuff[i2cModule][_cmdWrPtr[i2cModule]].data[i] = data[i];
		}

		// Increment counter and pointer
		_cmdBufCnt[i2cModule]++;
		if (++_cmdWrPtr[i2cModule] == I2C_CMD_CNT_MAX) {
			_cmdWrPtr[i2cModule] = 0;
		}

		// If state idle, start the bus (will trigger interrupt)
		// Otherwise, interrupt will take care of transmission
		if (_i2c_state[i2cModule] == I2C_STATE_IDLE) {
			I2C_StartTransfer(ulBase);
		}
	}

	// Re-enable Interrupt
	I2CMasterIntEnable(ulBase);


}

/*---------------------------------------------------------------------
 Function Name: I2C_ParseData
 Description:   parse data received over I2C lines
 Inputs:        slave - address data was sent from
 	 	 	 	data - pointer to data being parsed
 	 	 	 	len - length in bytes of data to be parsed
 Returns:       None
 -----------------------------------------------------------------------*/
BYTE blobcount;
BYTE s;
void I2C_ParseData(BYTE slave, BYTE* data, BYTE len) {
	BYTE * ptr;
	switch (slave) {
	case GYRO_SLAVE_ADDR:
	{
//		GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_1, ~0);
		ptr = (BYTE*) &SensorData.gyroX;
		ptr[0] = data[1];
		ptr[1] = data[0];
		ptr[2] = data[3];
		ptr[3] = data[2];
		ptr[4] = data[5];
		ptr[5] = data[4];
		 // Signal gyro propagation to start, send data to c28
		if (SensorCal.calibration_cnts >= ACCEL_CAL_COUNTS)
		{
			MtoCIPCBlockWrite(PACKETID_IMU, (void*) &SensorData.gyroX, sizeof(tIMUPacket), 0);
			SD_StuffPacket(PACKETID_IMU, sizeof(tIMUPacket), (BYTE *)&SensorData.gyroX);
		}
//		GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_1, 0);
		break;
	}
	case ACC_SLAVE_ADDR:
	{
		memcpy( (void*)&SensorData.accX, data, 6);
		//MtoCIPCBlockWrite(PACKETID_ACCEL, (void*) &SensorData.accX, sizeof(tAccelPacket), 0);	// todo: combine gyro and accel sending to c28

		// write calibration values to accelerometer
		if (SensorCal.calibration_cnts < ACCEL_CAL_COUNTS)
		{
			if(--SensorCal.blankReads > 0)
				break;

			SensorCal.axBias += SensorData.accX;
			SensorCal.ayBias += SensorData.accY;
			SensorCal.azBias += (SensorData.accZ - ACCEL_Z1G);

			// this only happens once
			if( ++SensorCal.calibration_cnts == ACCEL_CAL_COUNTS )
			{
				int div = ACCEL_CAL_COUNTS * ACCEL_RESOLULTION;
				SensorCal.axBias = (int) -roundf((float)SensorCal.axBias/div);
				SensorCal.ayBias = (int) -roundf((float)SensorCal.ayBias/div);
				SensorCal.azBias = (int) -roundf((float)SensorCal.azBias/div);

				BYTE data[4];

				// Configure the accelerometer (ADXL345)
				data[0] = 0x1E; // OFSX register (offset)
				data[1] = (BYTE) SensorCal.axBias;
				data[2] = (BYTE) SensorCal.ayBias;
				data[3] = (BYTE) SensorCal.azBias;
				I2C_WriteData(I2C0_MASTER_BASE, ACC_SLAVE_ADDR, data, 4); // Write to the ADXL-345

				GPIOPinWrite(LED_GREEN, ~1);
				GPIOPinWrite(LED_BLUE, ~1);
				GPIOPinWrite(LED_YELLOW, ~1);
			}
		}
		// TODO: check for valid data -- not 0xFFFF
		break;
	}
	case MAG_SLAVE_ADDR:
	{
		ptr = (BYTE*) &SensorData.magX;  // reads in order: x, z, y
		ptr[0] = data[1];
		ptr[1] = data[0];
		ptr[2] = data[5];
		ptr[3] = data[4];
		ptr[4] = data[3];
		ptr[5] = data[2];
		// TODO: check for valid data -- not 0xFFFF

		 // Send data to c28
		if (SensorCal.calibration_cnts >= ACCEL_CAL_COUNTS)
		{
			MtoCIPCBlockWrite(PACKETID_MAG, (void*) &SensorData.magX, sizeof(tMagPacket), 0);
			SD_StuffPacket(PACKETID_MAG, sizeof(tMagPacket), (BYTE *)&SensorData.magX);
		}

		break;
	}
    case WII_SLAVE_ADDR:
    {
        //code comes from http://www.bot-thoughts.com/2010/12/connecting-mbed-to-wiimote-ir-camera.html
        // info on ir camera: http://wiibrew.org/wiki/Wiimote#IR_Camera

        blobcount = 0;
        s = 0;

        SensorData.blob1.X = data[1];
        SensorData.blob1.Y = data[2];
        s   = data[3];
        SensorData.blob1.X += (s & 0x30) <<4;
        SensorData.blob1.Y += (s & 0xC0) <<2;
        SensorData.blob1.Size = (s & 0x0F);

        // At the moment we're using the size of the blob to determine if one is detected, either X,Y, or size could be used.
        //blobcount |= (SensorData.blob1.Size < 15)? BLOB1 : 0;
        blobcount += (SensorData.blob1.Size < 15)? 1 : 0;

        SensorData.blob2.X = data[4];
        SensorData.blob2.Y = data[5];
        s   = data[6];
        SensorData.blob2.X += (s & 0x30) <<4;
        SensorData.blob2.Y += (s & 0xC0) <<2;
        SensorData.blob2.Size = (s & 0x0F);

//        blobcount |= (SensorData.blob2.Size < 15)? BLOB2 : 0;
        blobcount += (SensorData.blob2.Size < 15)? 1 : 0;

        SensorData.blob3.X = data[7];
        SensorData.blob3.Y = data[8];
        s   = data[9];
        SensorData.blob3.X += (s & 0x30) <<4;
        SensorData.blob3.Y += (s & 0xC0) <<2;
        SensorData.blob3.Size = (s & 0x0F);

//        blobcount |= (SensorData.blob3.Size < 15)? BLOB3 : 0;
        blobcount += (SensorData.blob3.Size < 15)? 1 : 0;

        SensorData.blob4.X = data[10];
        SensorData.blob4.Y = data[11];
        s   = data[12];
        SensorData.blob4.X += (s & 0x30) <<4;
        SensorData.blob4.Y += (s & 0xC0) <<2;
        SensorData.blob4.Size = (s & 0x0F);

//        blobcount |= (SensorData.blob4.Size < 15)? BLOB4 : 0;
        blobcount += (SensorData.blob4.Size < 15)? 1 : 0;

        SensorData.blobcount = blobcount;

        // send to C28 here
        int i = 0;
        if (SensorData.blobcount == 3) {
        	tWiiIPCPacket pkt;
        	if (SensorData.blob1.Size < 15) {
        		pkt.x[i] = SensorData.blob1.X;
        		pkt.y[i] = SensorData.blob1.Y;
        		i++;
        	}
        	if (SensorData.blob2.Size < 15) {
				pkt.x[i] = SensorData.blob2.X;
				pkt.y[i] = SensorData.blob2.Y;
				i++;
			}
        	if (SensorData.blob3.Size < 15) {
				pkt.x[i] = SensorData.blob3.X;
				pkt.y[i] = SensorData.blob3.Y;
				i++;
			}
        	if (SensorData.blob4.Size < 15) {
				pkt.x[i] = SensorData.blob4.X;
				pkt.y[i] = SensorData.blob4.Y;
				i++;
			}

        	MtoCIPCBlockWrite(PACKETID_WII_IPC, (void*) &pkt.x[0], sizeof(tWiiIPCPacket), 0);
        	SD_StuffPacket(PACKETID_WII_IPC, sizeof(tWiiIPCPacket), (BYTE *) &pkt.x[0]);
        }

        //loop.SendSerial = 1; // this should probably be set in the main interrupt timer, not here.

        break;
    }

	case PRES_SLAVE_ADDR:
	{
		if (len == 3) // pressure
		{
			ptr = (BYTE*) &SensorCal.UP;
			ptr[0] = data[2];
			ptr[1] = data[1];
			ptr[2] = data[0];
			SensorCal.UP = SensorCal.UP >> (8-SensorCal.oss);
			BMP180_calc_pressure();
			loop.StartPressure = 1;
			loop.SendSensors = 1;
		} else if (len == 2) // temperature
		{
			ptr = (BYTE*) &SensorCal.UT;
			ptr[0] = data[1];
			ptr[1] = data[0];
			loop.StartPressure = 1;
			BMP180_calc_tmperature();
		} else if (len == 22) // calibration data
		{
			ptr = (BYTE*) &SensorCal.AC1;
			ptr[0] = data[1]; // AC1
			ptr[1] = data[0];
			ptr[2] = data[3]; // AC2
			ptr[3] = data[2];
			ptr[4] = data[5]; // AC3
			ptr[5] = data[4];
			ptr[6] = data[7]; // AC4
			ptr[7] = data[6];
			ptr[8] = data[9]; // AC5
			ptr[9] = data[8];
			ptr[10] = data[11]; // AC6
			ptr[11] = data[10];
			ptr[12] = data[13]; // B1
			ptr[13] = data[12];
			ptr[14] = data[15]; // B2
			ptr[15] = data[14];
			ptr[16] = data[17]; // MB
			ptr[17] = data[16];
			ptr[18] = data[19]; // MC
			ptr[19] = data[18];
			ptr[20] = data[21]; // MD
			ptr[21] = data[20];
		}
		break;
	}
	case EEPROM_SLAVE_ADDR:
	{
		// todo: make this more robust (maybe use packet_ids just for eeprom, or same as from data structs)
		if (len == 2) {
			fileID = data[0] << 8;
			fileID += data[1];
		} else if (len == (BYTE)sizeof(tSensorCalPacket)) {
			// sensor cal data
			// TODO: make sure values here are correct
			memcpy( (void*)&SensorCalPacket.gyroXScale, data, sizeof(tSensorCalPacket));
			MtoCIPCBlockWrite(PACKETID_SENSORCAL, (void*) &SensorCalPacket.gyroXScale, sizeof(tSensorCalPacket), 0); // send sensor calibration values to C28
		}
		break;
	}
    default:
    {
        break;
    }
	}
}

/*---------------------------------------------------------------------
 Function Name: I2C_StartTransfer
 Description:   Start transfer, either read or write over I2C
 Inputs:        ulBase -  I2C0_MASTER_BASE or I2C1_MASTER_BASE
 Returns:       None
 -----------------------------------------------------------------------*/
void I2C_StartTransfer(unsigned long ulBase) {

	// Check the arguments.
	ASSERT((ulBase == I2C0_MASTER_BASE) || (ulBase == I2C1_MASTER_BASE));

	int i2cModule;
	if (ulBase == I2C0_MASTER_BASE)
		i2cModule = 0;
	else
		i2cModule = 1;

	_data_cnt[i2cModule] = 0;

	if (cmdBuff[i2cModule][_cmdRdPtr[i2cModule]].receive) {

		// Set the slave address and setup for a transmit operation.
		I2CMasterSlaveAddrSet(ulBase, ((cmdBuff[i2cModule][_cmdRdPtr[i2cModule]].slaveId | 1) >> 1),
			cmdBuff[i2cModule][_cmdRdPtr[i2cModule]].receive);

		// Set up to receive data
		if (cmdBuff[i2cModule][_cmdRdPtr[i2cModule]].len == 1) {
			// Perform a single byte read.
			I2CMasterControl(ulBase, I2C_MASTER_CMD_SINGLE_RECEIVE);

			_i2c_state[i2cModule] = I2C_STATE_READ_WAIT;
		} else if (cmdBuff[i2cModule][_cmdRdPtr[i2cModule]].len == 2) {
			// Start burst read
			I2CMasterControl(ulBase,
					I2C_MASTER_CMD_BURST_RECEIVE_START);

			_i2c_state[i2cModule] = I2C_STATE_READ_FINAL;
		} else if (cmdBuff[i2cModule][_cmdRdPtr[i2cModule]].len > 2) {
			// Start burst read
			I2CMasterControl(ulBase,
					I2C_MASTER_CMD_BURST_RECEIVE_START);

			_i2c_state[i2cModule] = I2C_STATE_READ_NEXT;
		} else {
			// Decrement counters and increment pointers
			if (++_cmdRdPtr[i2cModule] == I2C_CMD_CNT_MAX) {
				_cmdRdPtr[i2cModule] = 0;
			}
			_cmdBufCnt[i2cModule]--;
		}
	} else {
		// Set up to write data
		// Set the slave address and setup for a transmit operation.
		I2CMasterSlaveAddrSet(ulBase, (cmdBuff[i2cModule][_cmdRdPtr[i2cModule]].slaveId >> 1),
			cmdBuff[i2cModule][_cmdRdPtr[i2cModule]].receive);

		// Write the first byte to the data register.
		I2CMasterDataPut(ulBase,
				cmdBuff[i2cModule][_cmdRdPtr[i2cModule]].data[_data_cnt[i2cModule]]);
		_data_cnt[i2cModule]++;

		if (cmdBuff[i2cModule][_cmdRdPtr[i2cModule]].len == 1) {
			// Start the burst cycle, writing the address as the first byte.
			I2CMasterControl(ulBase, I2C_MASTER_CMD_SINGLE_SEND);

			// Decrement counters and increment pointers
			if (++_cmdRdPtr[i2cModule] == I2C_CMD_CNT_MAX) {
				_cmdRdPtr[i2cModule] = 0;
			}
			_cmdBufCnt[i2cModule]--;

			_i2c_state[i2cModule] = I2C_STATE_STOPPED;

		} else if (cmdBuff[i2cModule][_cmdRdPtr[i2cModule]].len == 2) {
			// Start the burst cycle, writing the address as the first byte.
			I2CMasterControl(ulBase, I2C_MASTER_CMD_BURST_SEND_START);

			_i2c_state[i2cModule] = I2C_STATE_WRITE_FINAL;
		} else if (cmdBuff[i2cModule][_cmdRdPtr[i2cModule]].len > 2) {
			// Start the burst cycle, writing the address as the first byte.
			I2CMasterControl(ulBase, I2C_MASTER_CMD_BURST_SEND_START);

			_i2c_state[i2cModule] = I2C_STATE_WRITE_NEXT;
		} else // len <= 0
		{
			// Decrement counters and increment pointers
			if (++_cmdRdPtr[i2cModule] == I2C_CMD_CNT_MAX) {
				_cmdRdPtr[i2cModule] = 0;
			}
			_cmdBufCnt[i2cModule]--;
			// stay in state idle

		}
	}

}

/*---------------------------------------------------------------------
 Function Name: I2C1_IntHandler
 Description:   i2c1 interrupt handler
 Inputs:        None
 Returns:       None
 -----------------------------------------------------------------------*/
void I2C1_IntHandler(void) {

	//
	// Clear the I2C interrupt.
	//
	I2CMasterIntClear(I2C1_MASTER_BASE);

	I2C_ProcessInterrupt(I2C1_MASTER_BASE);

}


/*---------------------------------------------------------------------
 Function Name: I2C0_IntHandler
 Description:   i2c0 interrupt handler
 Inputs:        None
 Returns:       None
 -----------------------------------------------------------------------*/
void I2C0_IntHandler(void) {

	//
	// Clear the I2C interrupt.
	//
	I2CMasterIntClear(I2C0_MASTER_BASE);

	I2C_ProcessInterrupt(I2C0_MASTER_BASE);

}


/*---------------------------------------------------------------------
 Function Name: I2C_ProcessInterrupt
 Description:   Process i2c interrupt -- state machine
 Inputs:        ulBase -  I2C0_MASTER_BASE or I2C1_MASTER_BASE
 Returns:       None
 Comments:      Based on code from http://e2e.ti.com/support/microcontrollers/stellaris_arm_cortex-m3_microcontroller/f/471/t/169882.aspx
 -----------------------------------------------------------------------*/
void I2C_ProcessInterrupt(unsigned long ulBase) {

	// Check the arguments.
	ASSERT((ulBase == I2C0_MASTER_BASE) || (ulBase == I2C1_MASTER_BASE));

	int i2cModule;
	if (ulBase == I2C0_MASTER_BASE)
		i2cModule = 0;
	else
		i2cModule = 1;

	unsigned long error_state = I2CMasterErr(ulBase);
	if (error_state == I2C_MASTER_ERR_NONE) {
		//
		// Determine what to do based on the current state.
		//
		switch (_i2c_state[i2cModule]) {
		//
		// The idle state.
		//
		case I2C_STATE_IDLE: {
			//
			// There is nothing to be done.
			//
			break;
		}

			//
			// The state for writing one byte.
			//
		case I2C_STATE_WRITE_ONE: {
			//
			// Write the next byte to the data register.
			//
			I2CMasterDataPut(ulBase,
					cmdBuff[i2cModule][_cmdRdPtr[i2cModule]].data[_data_cnt[i2cModule]]);
			_data_cnt[i2cModule]++;

			//
			// Write one byte
			//
			I2CMasterControl(ulBase, I2C_MASTER_CMD_SINGLE_SEND);

			// Decrement counters and increment pointers
			if (++_cmdRdPtr[i2cModule] == I2C_CMD_CNT_MAX) {
				_cmdRdPtr[i2cModule] = 0;
			}
			_cmdBufCnt[i2cModule]--;

			_i2c_state[i2cModule] = I2C_STATE_STOPPED;

			//
			// This state is done.
			//
			break;
		}

			//
			// The state for the middle of a burst write.
			//
		case I2C_STATE_WRITE_NEXT: {
			//
			// Write the next byte to the data register.
			//
			I2CMasterDataPut(ulBase,
					cmdBuff[i2cModule][_cmdRdPtr[i2cModule]].data[_data_cnt[i2cModule]]);
			_data_cnt[i2cModule]++;

			//
			// Continue the burst write.
			//
			I2CMasterControl(ulBase, I2C_MASTER_CMD_BURST_SEND_CONT);

			//
			// If there is one byte left, set the next state to the final write
			// state.
			//
			if ((cmdBuff[i2cModule][_cmdRdPtr[i2cModule]].len - _data_cnt[i2cModule]) == 1) // single data byte left (remember c array indexing)
					{
				_i2c_state[i2cModule] = I2C_STATE_WRITE_FINAL;
			}

			//
			// This state is done.
			//
			break;
		}

			//
			// The state for the final write of a burst sequence.
			//
		case I2C_STATE_WRITE_FINAL: {
			//
			// Write the final byte to the data register.
			//
			I2CMasterDataPut(ulBase,
					cmdBuff[i2cModule][_cmdRdPtr[i2cModule]].data[_data_cnt[i2cModule]]);

			//
			// Finish the burst write.
			//
			I2CMasterControl(ulBase,
					I2C_MASTER_CMD_BURST_SEND_FINISH);

			// Decrement counters and increment pointers
			if (++_cmdRdPtr[i2cModule] == I2C_CMD_CNT_MAX) {
				_cmdRdPtr[i2cModule] = 0;
			}
			_cmdBufCnt[i2cModule]--;

			//
			// The next state is to wait for the burst write to complete.
			//
			_i2c_state[i2cModule] = I2C_STATE_STOPPED;

			//
			// This state is done.
			//
			break;
		}

			//
			// The state for a single byte read.
			//
		case I2C_STATE_READ_ONE: {
			//
			// Put the I2C master into receive mode.
			//
			I2CMasterSlaveAddrSet(ulBase,
					(cmdBuff[i2cModule][_cmdRdPtr[i2cModule]].slaveId | 1), true);

			//
			// Perform a single byte read.
			//
			I2CMasterControl(ulBase, I2C_MASTER_CMD_SINGLE_RECEIVE);

			//
			// The next state is the wait for final read state.
			//
			_i2c_state[i2cModule] = I2C_STATE_READ_WAIT;

			//
			// This state is done.
			//
			break;
		}

			//
			// The state for the start of a burst read.
			//
		case I2C_STATE_READ_FIRST: {
			//
			// Put the I2C master into receive mode.
			//
			I2CMasterSlaveAddrSet(ulBase,
					(cmdBuff[i2cModule][_cmdRdPtr[i2cModule]].slaveId | 1), true);

			//
			// Start the burst receive.
			//
			I2CMasterControl(ulBase,
					I2C_MASTER_CMD_BURST_RECEIVE_START);

			//
			// The next state is the middle of the burst read.
			//
			_i2c_state[i2cModule] = I2C_STATE_READ_NEXT;

			//
			// This state is done.
			//
			break;
		}

			//
			// The state for the middle of a burst read.
			//
		case I2C_STATE_READ_NEXT: {
			//
			// Read the received character.
			//
			cmdBuff[i2cModule][_cmdRdPtr[i2cModule]].data[_data_cnt[i2cModule]] = I2CMasterDataGet(
					ulBase);
			_data_cnt[i2cModule]++;

			//
			// Continue the burst read.
			//
			I2CMasterControl(ulBase,
					I2C_MASTER_CMD_BURST_RECEIVE_CONT);

			//
			// If there are two characters left to be read, make the next
			// state be the end of burst read state.
			//
			if ((cmdBuff[i2cModule][_cmdRdPtr[i2cModule]].len - _data_cnt[i2cModule]) == 2) {
				_i2c_state[i2cModule] = I2C_STATE_READ_FINAL;
			}

			//
			// This state is done.
			//
			break;
		}

			//
			// The state for the end of a burst read.
			//
		case I2C_STATE_READ_FINAL: {
			//
			// Read the received character.
			//
			cmdBuff[i2cModule][_cmdRdPtr[i2cModule]].data[_data_cnt[i2cModule]] = I2CMasterDataGet(
					ulBase);
			_data_cnt[i2cModule]++;

			//
			// Finish the burst read.
			//
			I2CMasterControl(ulBase,
					I2C_MASTER_CMD_BURST_RECEIVE_FINISH);

			//
			// The next state is the wait for final read state.
			//
			_i2c_state[i2cModule] = I2C_STATE_READ_WAIT;

			//
			// This state is done.
			//
			break;
		}

			//
			// This state is for the final read of a single or burst read.
			//
		case I2C_STATE_READ_WAIT: {
			//
			// Read the received character.
			//
			cmdBuff[i2cModule][_cmdRdPtr[i2cModule]].data[_data_cnt[i2cModule]] = I2CMasterDataGet(
					ulBase);
			_data_cnt[i2cModule]++;

			I2C_ParseData(cmdBuff[i2cModule][_cmdRdPtr[i2cModule]].slaveId,
					cmdBuff[i2cModule][_cmdRdPtr[i2cModule]].data, cmdBuff[i2cModule][_cmdRdPtr[i2cModule]].len);

			// Decrement counters and increment pointers
			if (++_cmdRdPtr[i2cModule] == I2C_CMD_CNT_MAX) {
				_cmdRdPtr[i2cModule] = 0;
			}
			_cmdBufCnt[i2cModule]--;

			//
			// The state machine is now idle.
			//
			//_i2c_state[i2cModule] = I2C_STATE_IDLE;

			if (_cmdBufCnt[i2cModule] == 0) {
				_i2c_state[i2cModule] = I2C_STATE_IDLE;
			} else {
				I2CMasterIntDisable(ulBase);
				I2C_StartTransfer(ulBase);
				I2CMasterIntEnable(ulBase);
			}

			//
			// This state is done.
			//
			break;
		}

		case I2C_STATE_WRITE_ADDRESS: {
			/*//
			 // Write the next byte to the data register.
			 //
			 I2CMasterDataPut(I2C0_MASTER_BASE, *g_pucData++);
			 g_ulCount--;

			 //
			 // Continue the burst write.
			 //
			 I2CMasterControl(I2C0_MASTER_BASE, I2C_MASTER_CMD_BURST_SEND_CONT);

			 //
			 // The state machine is now get the data byte.
			 //
			 g_ulState = STATE_READ_ONE;*/

			//
			// This state is done.
			//
			break;
		}

		case I2C_STATE_STOPPED: {
			if (_cmdBufCnt[i2cModule] == 0) {
				_i2c_state[i2cModule] = I2C_STATE_IDLE;
			} else {
				I2CMasterIntDisable(ulBase);
				//I2C0_StartTransfer();
				I2C_StartTransfer(ulBase);
				I2CMasterIntEnable(ulBase);
			}

			//
			// This state is done.
			//
			break;
		}

		}
	}
	// Error handling
	else if (error_state & I2C_MASTER_ERR_ARB_LOST) { // arbitration lost

		if (i2cModule == 0)
			loop.I2C0Recover = 1;
		else
			loop.I2C1Recover = 1;

	} else { // data nack'ed (error_state & (I2C_MASTER_ERR_ADDR_ACK | I2C_MASTER_ERR_DATA_ACK))


		// Decrement counters and increment pointers
		if (++_cmdRdPtr[i2cModule] == I2C_CMD_CNT_MAX) {
			_cmdRdPtr[i2cModule] = 0;
		}
		_cmdBufCnt[i2cModule]--;

		if (_cmdBufCnt[i2cModule] == 0) {
			_i2c_state[i2cModule] = I2C_STATE_IDLE;
		} else {
			I2CMasterIntDisable(ulBase);
			I2C_StartTransfer(ulBase);
			I2CMasterIntEnable(ulBase);
		}
	}
}


