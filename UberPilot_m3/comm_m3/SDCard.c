/*
 * SDCar.c
 *
 *  Created on: Jan 28, 2013
 *      Author: mark
 */

#include "SDCard.h"

//*****************************************************************************
// Static Variables
//*****************************************************************************
static unsigned char g_ucSDTxBufA[SD_TXBUF_SIZE];
static unsigned char g_ucSDTxBufB[SD_TXBUF_SIZE];
static unsigned int SDTxBuffSel = 0;
static unsigned int SDTxPtr = SD_SECTOR_HEADER_SIZE;
static unsigned long SDSector = 40976; // starting sector of sd card
volatile unsigned int fileID = 0;

extern volatile unsigned int SDWriteInProgress;


void InitSDCard(void)
{
        FRESULT fresult;

        // Configure SysTick for a 100Hz interrupt.
        SysTickPeriodSet(SysCtlClockGet(SYSTEM_CLOCK_SPEED) / TICKS_PER_SECOND);
        SysTickEnable();
        SysTickIntEnable();

        // Register interrupt handlers in the RAM vector table
        IntRegister(FAULT_SYSTICK, SysTickHandler);

        // Mount the file system, using logical disk 0.
        fresult = f_mount(0, &g_sFatFs);
        if(fresult != FR_OK)
        {
                //ERROR
                while(1);
                //return(1);
        }


        char filename[6] = "hi.txt";
        Cmd_open_file(filename);

        // read file ID from EEProm, increment and then rewrite
        BYTE addr[2];
        addr[0] = EEPROM_FILEID_ADDR >> 8; // Address of the first data byte
        addr[1] = EEPROM_FILEID_ADDR;
        I2C_WriteData(I2C1_MASTER_BASE, EEPROM_SLAVE_ADDR, addr, 2);
        I2C_ReadData(I2C1_MASTER_BASE, EEPROM_SLAVE_ADDR, 2);

        sleep_M3(5000); // wait 5 ms for the file ID to be read

        if (fileID > 30000)
                fileID = 0;

        // fill sector headers on both TxBuffers
        g_ucSDTxBufA[0] = SD_XA;
        g_ucSDTxBufA[1] = SD_XB;
        g_ucSDTxBufA[2] = SD_XC;
        g_ucSDTxBufA[3] = SD_XD;
        g_ucSDTxBufA[4] = fileID >> 8;
        g_ucSDTxBufA[5] = fileID;
        g_ucSDTxBufB[0] = SD_XA;
        g_ucSDTxBufB[1] = SD_XB;
        g_ucSDTxBufB[2] = SD_XC;
        g_ucSDTxBufB[3] = SD_XD;
        g_ucSDTxBufB[4] = fileID >> 8;
        g_ucSDTxBufB[5] = fileID;

        // increment fileID and rewrite it to the EEPROM
        fileID++;
        BYTE data[4];
        data[0] = EEPROM_FILEID_ADDR >> 8;
        data[1] = EEPROM_FILEID_ADDR;
        data[2] = fileID >> 8;
        data[3] = fileID;
        I2C_WriteData(I2C1_MASTER_BASE, EEPROM_SLAVE_ADDR, data, 4);
}



void SD_StuffPacket(BYTE packetId, BYTE len, BYTE* data) // todo: combine this with uart stuff packet function
{

        // Start Checksum
        BYTE chksum = 0;
        BYTE ptr = 0;
        BYTE i;
        BYTE TxBuff[256];

        if (len > 240)
                return;

        // STX
        TxBuff[ptr++] = STXA;
        TxBuff[ptr++] = STXB;

        // Packet Id
        TxBuff[ptr++] = packetId;
        chksum += packetId;

        // Packet Data Length
        TxBuff[ptr++] = len;
        chksum += len;

        // Stuff Data
        for(i = 0; i < len; i++)
        {
                // Send Byte
                TxBuff[ptr++] = data[i];
                chksum += data[i];
        }

        // Checksum Calculate and Send
        chksum ^= 0xFF;
        chksum += 1;
        TxBuff[ptr++] = chksum;

        SD_PutData(TxBuff, ptr);
}
//======================================================================

void SD_PutData(unsigned char * data, unsigned int size){

        // Select the active DMA TX buffer
        unsigned char * activeBuff;
        if(SDTxBuffSel == 0){
                activeBuff = g_ucSDTxBufA;
        }else{
                activeBuff = g_ucSDTxBufB;
        }

        // Copy data into the DMA TX buffer
        if (SDTxPtr+size <= SD_TXBUF_SIZE){
                int i;
                for(i=0; i <size; i++)
                {
                        activeBuff[SDTxPtr] = data[i];
                        SDTxPtr++;
                }
        }else
        {
                // filled buffer -- write it to SDCard
                disk_write(0,(const BYTE*)activeBuff,SDSector,1);
                SDSector++;
                SDWriteInProgress = 1;

                // need to switch buffers
                SDTxBuffSel = !SDTxBuffSel;
                SDTxPtr = SD_SECTOR_HEADER_SIZE;

                // Select the active DMA TX buffer
                unsigned char * activeBuff;
                if(SDTxBuffSel == 0){
                        activeBuff = g_ucSDTxBufA;
                }else{
                        activeBuff = g_ucSDTxBufB;
                }

                // Copy data into the DMA TX buffer
                if (SDTxPtr+size <= SD_TXBUF_SIZE){ // this should never be false
                        int i;
                        for(i=0; i <size; i++)
                        {
                                activeBuff[SDTxPtr] = data[i];
                                SDTxPtr++;
                        }
                }
        }

}


//*****************************************************************************
// Interrupt handler for SSI0 TX and RX.
//*****************************************************************************
void
SSI0IntHandler(void)
{
//    unsigned long ulStatus;
//      unsigned long readVal, res;
//      int WordIndex;
//
//    // Read the interrupt status of the SSI0
//    ulStatus = SSIIntStatus(SSI0_BASE, true);
//    ulStatusSSI0 = SSIIntStatus(SSI0_BASE, true);
//
//    // Clear any pending status
//    SSIIntClear(SSI0_BASE, ulStatus);
//
//      // ******** We received a "FIFO RX SSI0 half full" interrupt *********
//      if (ulStatus & SSI_RXFF)
//      {
//              // read the 4 words received in RxFIFO
//              for (WordIndex=0; WordIndex<4; WordIndex++)
//              {
//                      // Read single word
//                      res = SSIDataGetNonBlocking(SSI0_BASE, &readVal);
//                      if (res != 0)
//                      {
//                              // there was something to read in the Rx FIFO
//                              SSI0_RxTable[SSI0_RxTableIndex++] = readVal;
//                      }
//              }
//
//      }

}

//*****************************************************************************
// This is the handler for this SysTick interrupt.  FatFs requires a
// timer tick every 10 ms for internal timing purposes.
//*****************************************************************************
void
SysTickHandler(void)
{
        // Call the FatFs tick timer.
        disk_timerproc();
}


//*****************************************************************************
int
Cmd_open_file(const char * filenamemark)
{
        FRESULT fresult;

        //char name[6] = "hw.txt";

        //int i;
        //for (i=0;i<1000000;i++){}

        // Open the file for reading.
        fresult = f_open(&g_sFileObject, filenamemark, FA_OPEN_ALWAYS | FA_WRITE);

        // If there was some problem opening the file, then return
        // an error.
        if(fresult != FR_OK)
        {
                return(fresult);
        }

        // Return success.
        return(0);
}

//*****************************************************************************
int
Cmd_close_file()
{
        FRESULT fresult;

        // Close file
        fresult = f_close(&g_sFileObject);

        // If there was an error reading, then print a newline and
        // return the error to the user.
        if(fresult != FR_OK)
        {
                return(fresult);
        }

        // Return success.
        return(0);
}



//*****************************************************************************
int
Cmd_write(const char * data, int len)
{
        FRESULT fresult;
        unsigned short usBytesWritten;

        // Write a block of data
        fresult = f_write(&g_sFileObject, data, len,
                          &usBytesWritten);

        // If there was an error writing, then print a newline and
        // return the error to the user.
        if(fresult != FR_OK)
        {
                return(fresult);
        }

        // Return success.
        return(0);
}

