#ifndef I2C_H_
#define I2C_H_

#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_i2c.h"
#include "driverlib/i2c.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"

// local includes
#include "../defines_m3.h"

// globals
extern volatile tSensorData SensorData;

#define ACCEL_CAL_COUNTS 1000 // one second worth of data
#define ACCEL_Z1G	256	// theoretical z reading in 1 g field (full resolution mode)
#define ACCEL_RESOLULTION 	4 // 4 mg per LSB

#define I2C_PACKET_CNT_MAX			8
#define I2C_CMD_CNT_MAX				16
#define I2C_PACKET_SIZE_MAX			64

// Devices
#define GYRO_SLAVE_ADDR 0xD0
#define GYRO_DATA_ADDR 0x1D

#define PRES_SLAVE_ADDR 0xEE
#define PRES_DATA_ADDR 0xF4
#define PRES_CAL 0xAA
#define PRES_TEMPERATURE 0x2E
#define PRES_PRESSURE 0x34

#define ACC_SLAVE_ADDR 0xA6
#define ACC_DATA_ADDR 0x32

#define MAG_SLAVE_ADDR 0x3C
#define MAG_DATA_ADDR 0x03

#define WII_SLAVE_ADDR 0xB0
#define WII_DATA_ADDR 0x36
#define BASIC_MODE 0x01
#define EXTENDED_MODE 0x03
#define FULL_MODE 0x05
#define WII_MODE EXTENDED_MODE

// bit flags for blobs
#define BLOB1 0x01
#define BLOB2 0x02
#define BLOB3 0x04
#define BLOB4 0x08

#define MOTOR1_ADDR		0x52
#define MOTOR2_ADDR		0x54
#define MOTOR3_ADDR		0x56
#define MOTOR4_ADDR		0x58

#define EEPROM_SLAVE_ADDR 0xA0

// Data on eeprom -- 16 bit address defining start of data
#define EEPROM_FILEID_ADDR 0x0000 // length - 2 bytes
#define EEPROM_SENSORCAL_ADDR	0x0002 // length - 46 bytes


// State Machine
/*
#define I2C_STATE_IDLE				0
#define I2C_STATE_STARTED			1
#define I2C_STATE_IDSENT			2
#define I2C_STATE_WRITING			3
#define I2C_STATE_READING			4
#define I2C_STATE_ACKED				5
#define I2C_STATE_STOP				6
#define I2C_STATE_STOPPED			7
*/

#define I2C_STATE_IDLE                 0
#define I2C_STATE_WRITE_ONE            1
#define I2C_STATE_WRITE_NEXT           2
#define I2C_STATE_WRITE_FINAL          3
#define I2C_STATE_WAIT_ACK             4
#define I2C_STATE_SEND_ACK             5
#define I2C_STATE_READ_ONE             6
#define I2C_STATE_READ_FIRST           7
#define I2C_STATE_READ_NEXT            8
#define I2C_STATE_READ_FINAL           9
#define I2C_STATE_READ_WAIT            10
#define I2C_STATE_STOPPED	    	   11
#define I2C_STATE_WRITE_ADDRESS    	   12


// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
// I2C0
// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

// Prototypes
void I2C_ParseData(BYTE slave, BYTE* data, BYTE len);
void I2C_Init(unsigned long ulBase, void(fnHandler) (void));
void I2C_WriteData(unsigned long ulBase, BYTE slave, BYTE* data, BYTE len);
void I2C_ReadData(unsigned long ulBase, BYTE slave, BYTE len);
void I2C_StuffData(unsigned long ulBase, BYTE addr, BYTE* data, BYTE len, BYTE txrx);
void I2C_StartTransfer(unsigned long ulBase);
void I2C_ProcessInterrupt(unsigned long ulBase);
void I2C_Recover(unsigned long ulBase, unsigned long ulPort, unsigned char SDA, unsigned char SCL);

void I2C0_Recover(void);
void I2C0_Init(void);
void I2C0_IntHandler(void);

void I2C1_Recover(void);
void I2C1_Init();
void I2C1_IntHandler(void);

// Structs
struct strI2Ccmd {
    BYTE slaveId;
    BYTE len;
    BYTE receive;
    BYTE data[I2C_PACKET_SIZE_MAX];
};

#endif /*I2C_H_*/
