/*
 * IPC.c
 *
 *  Created on: Nov 5, 2012
 *      Author: mark
 */

#include "IPC_m3.h"

extern volatile tStatePacket StatePacket;
extern volatile tVoltagePacket VoltageData;

//*****************************************************************************
// At least 1 volatile global tIpcController instance is required when using
// IPC API Drivers.
//*****************************************************************************
volatile tIpcController g_sIpcController;


//*****************************************************************************
// Global variables used to read/write data passed between M3 and C28
//*****************************************************************************
uint8_t *pucMtoCMsgRam; // pointer of unsigned chars containing data from M to C
uint8_t *pucCtoMMsgRam; // pointer of unsigned chars containing data from C to M


/*---------------------------------------------------------------------
  Function Name: InitIPC
  Description:   Initialize inter process communications for communicating between cores
  Inputs:        None
  Returns:       None
-----------------------------------------------------------------------*/
void InitIPC(void)
{
    // Initialize M3toC28 message RAM and Sx SARAM and wait until initialized
//    RAMMReqSharedMemAccess (S0_ACCESS, SX_M3MASTER);

    //
    // Allow writes to protected registers.
    HWREG(SYSCTL_MWRALLOW) = 0xA5A5A5A5;

//    HWREG(RAM_CONFIG_BASE + RAM_O_MSXRTESTINIT1) |= 0x1;
//    while((HWREG(RAM_CONFIG_BASE + RAM_O_MSXRINITDONE1)&0x1) != 0x1)
//    {
//    }
//
//    HWREG(RAM_CONFIG_BASE + RAM_O_MTOCCRTESTINIT1) |= 0x1;
//    while((HWREG(RAM_CONFIG_BASE + RAM_O_MTOCRINITDONE)&0x1) != 0x1)
//    {
//    }

    //  Register M3 interrupt handlers
    IntRegister(INT_CTOMPIC3, CtoMIPC3IntHandler);

    //  Disable Clocking to Watchdog Timer1 and Watchdog Timer0
    SysCtlPeripheralDisable(SYSCTL_PERIPH_WDOG0);
    SysCtlPeripheralDisable(SYSCTL_PERIPH_WDOG1);


    //  Enable the IPC interrupts.
    IntEnable(INT_CTOMPIC3);

//    RAMMReqSharedMemAccess (S0_ACCESS, SX_C28MASTER);

    //
    //  Disable writes to protected registers.
    HWREG(SYSCTL_MWRALLOW) = 0;

    // shared RAM messages (2 kbytes)
    pucMtoCMsgRam = (void *)M3_MTOC_PASSMSG;
    pucCtoMMsgRam = (void *)M3_CTOM_PASSMSG;

}

/*---------------------------------------------------------------------
  Function Name: MtoCIPCBlockWrite
  Description:   Send block of data to C28
  Inputs:        id - unique data packet id shared between cores
  	  	  	  	 data - pointer to data
  	  	  	  	 size - size of data to send
  	  	  	  	 blocking - enable blocking to force the core to wait for the other core to process previous data
  Returns:       None
-----------------------------------------------------------------------*/
void MtoCIPCBlockWrite(unsigned char id, BYTE * data, unsigned int size, unsigned int blocking)
{

	if (IPCMtoCFlagBusy(IPC_FLAG2))	// using flag 1 -- could be using 1,2,3, or 4 to generate interrupts
	{
		// C28 hasn't finished processing
		if (blocking)
		{
			while (IPCMtoCFlagBusy(IPC_FLAG2))
			{}
		} else
		{
			return; // todo: something more intelligent here?
		}
	}

    pucMtoCMsgRam[0] = id;	// Packet ID header
    memcpy(pucMtoCMsgRam+2, data, size);  // on C28 all data types are at least 16 bits so make ID 16 bits

    IPCMtoCFlagSet(IPC_FLAG2);	// signal C28 that data is ready

}

/*---------------------------------------------------------------------
  Function Name: CtoMIPC2IntHandler
  Description:   Parse C to M data
  Inputs:        None
  Returns:       None
-----------------------------------------------------------------------*/
void CtoMIPC3IntHandler(void)
{
	GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_1, ~0);
	uint8_t id = pucCtoMMsgRam[0];
	switch (id)
	{
	case PACKETID_STATE:
		memcpy( (void *)&StatePacket.x, pucCtoMMsgRam+2, sizeof(tStatePacket));
		SD_StuffPacket(PACKETID_STATE, sizeof(tStatePacket), (BYTE *)&StatePacket.x);
		toggle_dbg(DBG_PB4);

		break;

	case PACKETID_VOLTAGE:
		memcpy( (void *)&VoltageData.voltage, pucCtoMMsgRam+2, sizeof(tVoltagePacket));

		break;

	default:
		//ErrorFlag = 1;
		break;
	}
	GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_1, 0);

	IPCCtoMFlagAcknowledge(IPC_FLAG3); // acknowledge the data from m3
	//PieCtrlRegs.PIEACK.all = PIEACK_GROUP11;   // Acknowledge interrupt to PIE
}



