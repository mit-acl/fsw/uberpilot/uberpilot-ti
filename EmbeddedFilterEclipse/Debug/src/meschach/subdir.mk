################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/meschach/copy.c \
../src/meschach/hsehldr.c \
../src/meschach/init.c \
../src/meschach/machine.c \
../src/meschach/matop.c \
../src/meschach/meminfo.c \
../src/meschach/memory.c \
../src/meschach/memstat.c \
../src/meschach/norm.c \
../src/meschach/qrfactor.c \
../src/meschach/solve.c \
../src/meschach/submat.c \
../src/meschach/vecop.c 

OBJS += \
./src/meschach/copy.o \
./src/meschach/hsehldr.o \
./src/meschach/init.o \
./src/meschach/machine.o \
./src/meschach/matop.o \
./src/meschach/meminfo.o \
./src/meschach/memory.o \
./src/meschach/memstat.o \
./src/meschach/norm.o \
./src/meschach/qrfactor.o \
./src/meschach/solve.o \
./src/meschach/submat.o \
./src/meschach/vecop.o 

C_DEPS += \
./src/meschach/copy.d \
./src/meschach/hsehldr.d \
./src/meschach/init.d \
./src/meschach/machine.d \
./src/meschach/matop.d \
./src/meschach/meminfo.d \
./src/meschach/memory.d \
./src/meschach/memstat.d \
./src/meschach/norm.d \
./src/meschach/qrfactor.d \
./src/meschach/solve.d \
./src/meschach/submat.d \
./src/meschach/vecop.d 


# Each subdirectory must supply rules for building sources it contributes
src/meschach/%.o: ../src/meschach/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


