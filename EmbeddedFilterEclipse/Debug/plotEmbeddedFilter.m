function plotEmbeddedFilter( )

clc;

%% Set params
global param
param.DT = 1/500;
param.G = 9.81;
param.gyro_scale = pi/14.375/180.0*1.0; % Raw gyro -> rad/sec
param.accel_scale = 1/256*param.G; % Raw accel -> m/s^2
param.nAttMeas = 50; % Do attitude measurement every 10*DT seconds
param.K_AttFilter = param.DT*param.nAttMeas*0.1;
param.K_GyroBias = param.DT*param.nAttMeas*0.05;

param.nPosVelMeas = 100; % Do position/velocity measurement every 100*DT seconds

%% IMPORT DATA
file = 'sensorlog1.txt';
%file = 'sensorlog2.txt';
[sensor, truth] = getData(file);

cdata = importdata('filter_output.txt');
cfilter.x = cdata(:,1);
cfilter.y = cdata(:,2);
cfilter.z = cdata(:,3);
cfilter.dx = cdata(:,4);
cfilter.dy = cdata(:,5);
cfilter.dz = cdata(:,6);
cfilter.ax = cdata(:,7);
cfilter.ay = cdata(:,8);
cfilter.az = cdata(:,9);

iEnd = length(sensor.t);
% iEnd = 1000;

f = figure(1);clf; 
subplot(4,1,1);hold all;
plot(sensor.t(1:iEnd), cfilter.x(1:iEnd),'r',sensor.t(1:iEnd), truth.x(1:iEnd),'r--');
plot(sensor.t(1:iEnd), cfilter.y(1:iEnd),'g',sensor.t(1:iEnd), truth.y(1:iEnd),'g--');
plot(sensor.t(1:iEnd), cfilter.z(1:iEnd),'b',sensor.t(1:iEnd), truth.z(1:iEnd),'b--');
title('Position');legend('xfilt', 'xvicon','yfilt', 'yvicon','zfilt', 'zvicon');
subplot(4,1,2);hold all;
plot(sensor.t(1:iEnd), cfilter.dx(1:iEnd),'r',sensor.t(1:iEnd), truth.dx(1:iEnd),'r--');
plot(sensor.t(1:iEnd), cfilter.dy(1:iEnd),'g',sensor.t(1:iEnd), truth.dy(1:iEnd),'g--');
plot(sensor.t(1:iEnd), cfilter.dz(1:iEnd),'b',sensor.t(1:iEnd), truth.dz(1:iEnd),'b--');
title('Velocity');legend('dxfilt', 'dxvicon','dyfilt', 'dyvicon','dzfilt', 'dzvicon');
subplot(4,1,3);hold all;
plot(sensor.t(1:iEnd), sensor.ax(1:iEnd),'r');
plot(sensor.t(1:iEnd), sensor.ay(1:iEnd),'g');
plot(sensor.t(1:iEnd), sensor.az(1:iEnd),'b');
title('Acceleration');legend('x', 'y','z');
subplot(4,1,4);hold all;
plot(sensor.t(1:iEnd), cfilter.ax(1:iEnd),'r');
plot(sensor.t(1:iEnd), cfilter.ay(1:iEnd),'g');
plot(sensor.t(1:iEnd), cfilter.az(1:iEnd),'b');
title('Estimated Accel Biases');legend('x', 'y','z');

f = figure(2); clf;
plot(sensor.t(1:iEnd), sensor.sonar(1:iEnd));

%% Link x axes
ax = [];
for ii=1:length(f)
    ax = [ax; get(f(ii),'children')];
end
linkaxes(ax,'x');

end





%% Gets sensor/truth data from log file
function [sensor, truth] = getData(file)

global param

log = importdata(file,',',1);

%% Choose data to use
% Time
col = 1;
sensor.rost = log.data(:,col); col = col+1;
sensor.rost = sensor.rost - sensor.rost(1);
sensor.t = 0:1/500:(length(sensor.rost)-1)/500;

% Gyro Data - 500 Hz
sensor.gx = log.data(:,col); col=col+1;
sensor.gy = log.data(:,col); col=col+1;
sensor.gz = log.data(:,col); col=col+1;
% remove initial bias
sensor.gx = sensor.gx - mean(sensor.gx(100:600));
sensor.gy = sensor.gy - mean(sensor.gy(100:600));
sensor.gz = sensor.gz - mean(sensor.gz(100:600));
% Scale to rad/sec in correct orientation
sensor.gx = -sensor.gx*param.gyro_scale;
sensor.gy = -sensor.gy*param.gyro_scale;
sensor.gz =  sensor.gz*param.gyro_scale;

% Accel Data - 500 Hz
sensor.ax = log.data(:,col); col=col+1;
sensor.ay = log.data(:,col); col=col+1;
sensor.az = log.data(:,col); col=col+1;

% Scale to m/s^2 in correct orientation
sensor.ax = -sensor.ax*param.accel_scale;
sensor.ay = -sensor.ay*param.accel_scale;
sensor.az = sensor.az*param.accel_scale;

% Magnetometer Data - 50 Hz
sensor.mx = log.data(:,col); col=col+1;
sensor.my = log.data(:,col); col=col+1;
sensor.mz = log.data(:,col); col=col+1;
% get when magnetometer changes
magx = sensor.mx(1);
i = 1;
while magx == sensor.mx(i)
    i = i+1;
end
index = i:10:length(sensor.mx);
sensor.mx = sensor.mx(index);
sensor.my = sensor.my(index);
sensor.mz = sensor.mz(index);
sensor.tmag = sensor.t(index);

% Pressure Data ~13.2 Hz
sensor.temperature = log.data(:,col); col=col+1;
sensor.pressure = log.data(:,col); col=col+1;
% p = sensor.pressure(1);
% i = 1;
% index = 1;
% while i ~= length(sensor.pressure)
%     p = sensor.pressure(i);
%     while p == sensor.pressure(i)
%         i = i+1;
%     end
%     index = [index i];
% end


sensor.sonar = log.data(:,col); col=col+1;

% Vicon truth data
truth.x = log.data(:,col); col=col+1;
truth.y = log.data(:,col); col=col+1;
truth.z = log.data(:,col); col=col+1;

truth.qw = log.data(:,col); col=col+1;
truth.qx = log.data(:,col); col=col+1;
truth.qy = log.data(:,col); col=col+1;
truth.qz = log.data(:,col); col=col+1;

truth.dx = log.data(:,col); col=col+1;
truth.dy = log.data(:,col); col=col+1;
truth.dz = log.data(:,col); col=col+1;

truth.p = log.data(:,col); col=col+1;
truth.q = log.data(:,col); col=col+1;
truth.r = log.data(:,col);

[truth.yaw truth.pitch truth.roll] = quat2angle([truth.qw truth.qx truth.qy truth.qz]);


end