/**************************************************************************
 **
 ** Copyright (C) 1993 David E. Steward & Zbigniew Leyk, all rights reserved.
 **
 **			     Meschach Library
 **
 ** This Meschach Library is provided "as is" without any express
 ** or implied warranty of any kind with respect to this software.
 ** In particular the authors shall not be liable for any direct,
 ** indirect, special, incidental or consequential damages arising
 ** in any way from use of the software.
 **
 ** Everyone is granted permission to copy, modify and redistribute this
 ** Meschach Library, provided:
 **  1.  All copies contain this copyright notice.
 **  2.  All modified copies shall carry a notice stating who
 **      made the last modification and the date of such modification.
 **  3.  No charge is made for this software or works derived from it.
 **      This clause shall not be construed as constraining other software
 **      distributed on the same medium as this software, nor is a
 **      distribution fee considered a charge.
 **
 ***************************************************************************/

/* matop.c 1.3 11/25/87 */

#include	<stdio.h>
#include	"matrix.h"


/* m_add -- matrix addition -- may be in-situ */
#ifndef ANSI_C
MAT *m_add(mat1,mat2,out)
MAT *mat1,*mat2,*out;
#else
MAT *m_add(const MAT *mat1, const MAT *mat2, MAT *out)
#endif
{
	unsigned int m, n, i;

//	if ( mat1==(MAT *)NULL || mat2==(MAT *)NULL )
//		error(E_NULL,"m_add");
//	if ( mat1->m != mat2->m || mat1->n != mat2->n )
//		error(E_SIZES,"m_add");
	if (out == (MAT *) NULL || out->m != mat1->m || out->n != mat1->n)
		out = m_resize(out, mat1->m, mat1->n);
	m = mat1->m;
	n = mat1->n;
	for (i = 0; i < m; i++) {
		__add__(mat1->me[i], mat2->me[i], out->me[i], (int) n);
		/**************************************************
		 for ( j=0; j<n; j++ )
		 out->me[i][j] = mat1->me[i][j]+mat2->me[i][j];
		 **************************************************/
	}

	return (out);
}

/* m_sub -- matrix subtraction -- may be in-situ */
#ifndef ANSI_C
MAT *m_sub(mat1,mat2,out)
MAT *mat1,*mat2,*out;
#else
MAT *m_sub(const MAT *mat1, const MAT *mat2, MAT *out)
#endif
{
	unsigned int m, n, i;

//	if (mat1 == (MAT *) NULL || mat2 == (MAT *) NULL )
//		error(E_NULL, "m_sub");
//	if (mat1->m != mat2->m || mat1->n != mat2->n)
//		error(E_SIZES, "m_sub");
	if (out == (MAT *) NULL || out->m != mat1->m || out->n != mat1->n)
		out = m_resize(out, mat1->m, mat1->n);
	m = mat1->m;
	n = mat1->n;
	for (i = 0; i < m; i++) {
		__sub__(mat1->me[i], mat2->me[i], out->me[i], (int) n);
		/**************************************************
		 for ( j=0; j<n; j++ )
		 out->me[i][j] = mat1->me[i][j]-mat2->me[i][j];
		 **************************************************/
	}

	return (out);
}

/* m_mlt -- matrix-matrix multiplication */
#ifndef ANSI_C
MAT *m_mlt(A,B,OUT)
MAT *A,*B,*OUT;
#else
MAT *m_mlt(const MAT *A, const MAT *B, MAT *OUT)
#endif
{
	unsigned int i, /* j, */k, m, n, p;
	Real **A_v, **B_v /*, *B_row, *OUT_row, sum, tmp */;

//	if (A == (MAT *) NULL || B == (MAT *) NULL )
//		error(E_NULL, "m_mlt");
//	if (A->n != B->m)
//		error(E_SIZES, "m_mlt");
//	if (A == OUT || B == OUT)
//		error(E_INSITU, "m_mlt");
	m = A->m;
	n = A->n;
	p = B->n;
	A_v = A->me;
	B_v = B->me;

	if (OUT == (MAT *) NULL || OUT->m != A->m || OUT->n != B->n)
		OUT = m_resize(OUT, A->m, B->n);

	/****************************************************************
	 for ( i=0; i<m; i++ )
	 for  ( j=0; j<p; j++ )
	 {
	 sum = 0.0;
	 for ( k=0; k<n; k++ )
	 sum += A_v[i][k]*B_v[k][j];
	 OUT->me[i][j] = sum;
	 }
	 ****************************************************************/
	m_zero(OUT);
	for (i = 0; i < m; i++)
		for (k = 0; k < n; k++) {
			if (A_v[i][k] != 0.0)
				__mltadd__(OUT->me[i], B_v[k], A_v[i][k], (int) p);
			/**************************************************
			 B_row = B_v[k];	OUT_row = OUT->me[i];
			 for ( j=0; j<p; j++ )
			 (*OUT_row++) += tmp*(*B_row++);
			 **************************************************/
		}

	return OUT;
}




/* mv_mlt -- matrix-vector multiplication 
 -- Note: b is treated as a column vector */
#ifndef ANSI_C
VEC *mv_mlt(A,b,out)
MAT *A;
VEC *b,*out;
#else
VEC *mv_mlt(const MAT *A, const VEC *b, VEC *out)
#endif
{
	unsigned int i, m, n;
	Real **A_v, *b_v /*, *A_row */;
	/* register Real	sum; */

//	if (A == (MAT *) NULL || b == (VEC *) NULL )
//		error(E_NULL, "mv_mlt");
//	if (A->n != b->dim)
//		error(E_SIZES, "mv_mlt");
//	if (b == out)
//		error(E_INSITU, "mv_mlt");
	if (out == (VEC *) NULL || out->dim != A->m)
		out = v_resize(out, A->m);

	m = A->m;
	n = A->n;
	A_v = A->me;
	b_v = b->ve;
	for (i = 0; i < m; i++) {
		/* for ( j=0; j<n; j++ )
		 sum += A_v[i][j]*b_v[j]; */
		out->ve[i] = __ip__(A_v[i], b_v, (int) n);
		/**************************************************
		 A_row = A_v[i];		b_v = b->ve;
		 for ( j=0; j<n; j++ )
		 sum += (*A_row++)*(*b_v++);
		 out->ve[i] = sum;
		 **************************************************/
	}

	return out;
}

/* sm_mlt -- scalar-matrix multiply -- may be in-situ */
#ifndef ANSI_C
MAT *sm_mlt(scalar,matrix,out)
double scalar;
MAT *matrix,*out;
#else
MAT *sm_mlt(double scalar, const MAT *matrix, MAT *out)
#endif
{
	unsigned int m, n, i;

//	if (matrix == (MAT *) NULL )
//		error(E_NULL, "sm_mlt");
	if (out == (MAT *) NULL || out->m != matrix->m || out->n != matrix->n)
		out = m_resize(out, matrix->m, matrix->n);
	m = matrix->m;
	n = matrix->n;
	for (i = 0; i < m; i++)
		__smlt__(matrix->me[i], (double) scalar, out->me[i], (int) n);
	/**************************************************
	 for ( j=0; j<n; j++ )
	 out->me[i][j] = scalar*matrix->me[i][j];
	 **************************************************/
	return (out);
}

/* vm_mlt -- vector-matrix multiplication
 -- Note: b is treated as a row vector */
#ifndef ANSI_C
VEC *vm_mlt(A,b,out)
MAT *A;
VEC *b,*out;
#else
VEC *vm_mlt(const MAT *A, const VEC *b, VEC *out)
#endif
{
	unsigned int j, m, n;
	/* Real	sum,**A_v,*b_v; */

//	if (A == (MAT *) NULL || b == (VEC *) NULL )
//		error(E_NULL, "vm_mlt");
//	if (A->m != b->dim)
//		error(E_SIZES, "vm_mlt");
//	if (b == out)
//		error(E_INSITU, "vm_mlt");
	if (out == (VEC *) NULL || out->dim != A->n)
		out = v_resize(out, A->n);

	m = A->m;
	n = A->n;

	v_zero(out);
	for (j = 0; j < m; j++)
		if (b->ve[j] != 0.0)
			__mltadd__(out->ve, A->me[j], b->ve[j], (int) n);
	/**************************************************
	 A_v = A->me;		b_v = b->ve;
	 for ( j=0; j<n; j++ )
	 {
	 sum = 0.0;
	 for ( i=0; i<m; i++ )
	 sum += b_v[i]*A_v[i][j];
	 out->ve[j] = sum;
	 }
	 **************************************************/

	return out;
}




