/**************************************************************************
 **
 ** Copyright (C) 1993 David E. Steward & Zbigniew Leyk, all rights reserved.
 **
 **			     Meschach Library
 **
 ** This Meschach Library is provided "as is" without any express
 ** or implied warranty of any kind with respect to this software.
 ** In particular the authors shall not be liable for any direct,
 ** indirect, special, incidental or consequential damages arising
 ** in any way from use of the software.
 **
 ** Everyone is granted permission to copy, modify and redistribute this
 ** Meschach Library, provided:
 **  1.  All copies contain this copyright notice.
 **  2.  All modified copies shall carry a notice stating who
 **      made the last modification and the date of such modification.
 **  3.  No charge is made for this software or works derived from it.
 **      This clause shall not be construed as constraining other software
 **      distributed on the same medium as this software, nor is a
 **      distribution fee considered a charge.
 **
 ***************************************************************************/

/*  mem_stat.c    6/09/93  */

/* Deallocation of static arrays */

#include <stdio.h>
#include  "matrix.h"
#include  "meminfo.h"
#ifdef COMPLEX   
#include  "zmatrix.h"
#endif
#ifdef SPARSE
#include  "sparse.h"
#include  "iter.h"
#endif

static char rcsid[] = "$Id: memstat.c,v 1.1 1994/01/13 05:32:44 des Exp $";

/* global variable */

extern MEM_CONNECT mem_connect[MEM_CONNECT_MAX_LISTS];

/* local type */

typedef struct {
	void **var; /* for &A, where A is a pointer */
	int type; /* type of A */
	int mark; /* what mark is chosen */
	char *fname; /* source file name where last registered */
	int line; /* line # of file where last registered */
} MEM_STAT_STRUCT;

/* local variables */

/* how many marks are used */
static int mem_stat_mark_many = 0;

/* current mark */
static int mem_stat_mark_curr = 0;

static MEM_STAT_STRUCT mem_stat_var[MEM_HASHSIZE];

/* array of indices (+1) to mem_stat_var */
static unsigned int mem_hash_idx[MEM_HASHSIZE];

/* points to the first unused element in mem_hash_idx */
static unsigned int mem_hash_idx_end = 0;

/* hashing function */
#ifndef ANSI_C
static unsigned int mem_hash(ptr)
void **ptr;
#else
static unsigned int mem_hash(void **ptr)
#endif
{
	unsigned long lp = (unsigned long) ptr;

	return (lp % MEM_HASHSIZE);
}

/* look for a place in mem_stat_var */
#ifndef ANSI_C
static int mem_lookup(var)
void **var;
#else
static int mem_lookup(void **var)
#endif
{
	int k, j;

	k = mem_hash(var);

	if (mem_stat_var[k].var == var) {
		return -1;
	} else if (mem_stat_var[k].var == NULL) {
		return k;
	} else { /* look for an empty place */
		j = k;
		while (mem_stat_var[j].var != var && j < MEM_HASHSIZE
				&& mem_stat_var[j].var != NULL)
			j++;

		if (mem_stat_var[j].var == NULL)
			return j;
		else if (mem_stat_var[j].var == var)
			return -1;
		else { /* if (j == MEM_HASHSIZE) */
			j = 0;
			while (mem_stat_var[j].var != var && j < k
					&& mem_stat_var[j].var != NULL)
				j++;
			if (mem_stat_var[j].var == NULL)
				return j;
			else if (mem_stat_var[j].var == var)
				return -1;
			else { /* if (j == k) */
//	    fprintf(stderr,
//              "\n WARNING !!! static memory: mem_stat_var is too small\n");
//	    fprintf(stderr,
//	      " Increase MEM_HASHSIZE in file: %s (currently = %d)\n\n",
//		    MEM_HASHSIZE_FILE, MEM_HASHSIZE);
//	    if ( !isatty(fileno(stdout)) ) {
//	       fprintf(stdout,
//                "\n WARNING !!! static memory: mem_stat_var is too small\n");
//	       fprintf(stdout,
//	        " Increase MEM_HASHSIZE in file: %s (currently = %d)\n\n",
//		    MEM_HASHSIZE_FILE, MEM_HASHSIZE);
//	    }
//	    error(E_MEM,"mem_lookup");
			}
		}
	}

	return -1;
}

/* register static variables;
 Input arguments:
 var - variable to be registered,
 type - type of this variable;
 list - list of types
 fname - source file name where last registered
 line - line number of source file

 returned value < 0  --> error,
 returned value == 0 --> not registered,
 returned value >= 0 --> registered with this mark;
 */
#ifndef ANSI_C
int mem_stat_reg_list(var,type,list,fname,line)
void **var;
int type,list;
char *fname;
int line;
#else
int mem_stat_reg_list(void **var, int type, int list, char *fname, int line)
#endif
{
	int n;

	if (list < 0 || list >= MEM_CONNECT_MAX_LISTS)
		return -1;

	if (mem_stat_mark_curr == 0)
		return 0; /* not registered */
	if (var == NULL)
		return -1; /* error */

	if (type < 0 || type >= mem_connect[list].ntypes
			|| mem_connect[list].free_funcs[type] == NULL) {
//      warning(WARN_WRONG_TYPE,"mem_stat_reg_list");
		return -1;
	}

	if ((n = mem_lookup(var)) >= 0) {
		mem_stat_var[n].var = var;
		mem_stat_var[n].mark = mem_stat_mark_curr;
		mem_stat_var[n].type = type;
		mem_stat_var[n].fname = fname;
		mem_stat_var[n].line = line;
		/* save n+1, not n */
		mem_hash_idx[mem_hash_idx_end++] = n + 1;
	}

	return mem_stat_mark_curr;
}

