
/**************************************************************************
**
** Copyright (C) 1993 David E. Steward & Zbigniew Leyk, all rights reserved.
**
**			     Meschach Library
** 
** This Meschach Library is provided "as is" without any express 
** or implied warranty of any kind with respect to this software. 
** In particular the authors shall not be liable for any direct, 
** indirect, special, incidental or consequential damages arising 
** in any way from use of the software.
** 
** Everyone is granted permission to copy, modify and redistribute this
** Meschach Library, provided:
**  1.  All copies contain this copyright notice.
**  2.  All modified copies shall carry a notice stating who
**      made the last modification and the date of such modification.
**  3.  No charge is made for this software or works derived from it.  
**      This clause shall not be construed as constraining other software
**      distributed on the same medium as this software, nor is a
**      distribution fee considered a charge.
**
***************************************************************************/


/* meminfo.c  revised  22/11/93 */

/* 
  contains basic functions, types and arrays 
  to keep track of memory allocation/deallocation
*/

#include <stdio.h>
#include  "matrix.h"
#include  "meminfo.h"
#ifdef COMPLEX   
#include  "zmatrix.h"
#endif
#ifdef SPARSE
#include  "sparse.h"
#include  "iter.h"
#endif

//static char rcsid[] = "$Id: meminfo.c,v 1.1 1994/01/13 05:31:39 des Exp $";

/* this array is defined further in this file */
extern MEM_CONNECT mem_connect[MEM_CONNECT_MAX_LISTS];


/* names of types */
static char *mem_type_names[] = {
   "MAT",
   "BAND",
   "PERM",
   "VEC",
   "IVEC"
#ifdef SPARSE
     ,"ITER",
     "SPROW",
     "SPMAT"
#endif
#ifdef COMPLEX   
       ,"ZVEC",
       "ZMAT"
#endif
      };


#define MEM_NUM_STD_TYPES  (sizeof(mem_type_names)/sizeof(mem_type_names[0]))


/* local array for keeping track of memory */
static MEM_ARRAY   mem_info_sum[MEM_NUM_STD_TYPES];  


/* for freeing various types */
static int (*mem_free_funcs[MEM_NUM_STD_TYPES])() = {
   m_free,
   v_free
#ifdef SPARSE
     ,iter_free,	
     sprow_free, 
     sp_free
#endif
#ifdef COMPLEX
       ,zv_free,	
       zm_free
#endif
      };



/* it is a global variable for passing 
   pointers to local arrays defined here */
MEM_CONNECT mem_connect[MEM_CONNECT_MAX_LISTS] = {
 { mem_type_names, mem_free_funcs, MEM_NUM_STD_TYPES, 
     mem_info_sum } 
};







/*=============================================================*/


/* local variables */

static int	mem_switched_on = MEM_SWITCH_ON_DEF;  /* on/off */


/* switch on/off memory info */
#ifndef ANSI_C
int mem_info_on(sw)
int sw;
#else
int mem_info_on(int sw)
#endif
{
   int old = mem_switched_on;

   mem_switched_on = sw;
   return old;
}

#ifdef ANSI_C
int mem_info_is_on(void)
#else
int mem_info_is_on()
#endif
{
   return mem_switched_on;
}


/* return the number of allocated variables for type 'type' */
#ifndef ANSI_C
int mem_info_numvar(type,list)
int type,list;
#else
int mem_info_numvar(int type, int list)
#endif
{
   if ( list < 0 || list >= MEM_CONNECT_MAX_LISTS )
     return 0l;
   if ( !mem_switched_on || type < 0
       || type >= mem_connect[list].ntypes
       || mem_connect[list].free_funcs[type] == NULL )
     return 0l;

   return mem_connect[list].info_sum[type].numvar;
}





/* function for memory information */


/* mem_bytes_list

   Arguments:
   type - the number of type;
   old_size - old size of allocated memory (in bytes);
   new_size - new size of allocated memory (in bytes);
   list - list of types
   */
#ifndef ANSI_C
void mem_bytes_list(type,old_size,new_size,list)
int type,list;
int old_size,new_size;
#else
void mem_bytes_list(int type, int old_size, int new_size, int list)
#endif
{
   MEM_CONNECT *mlist;

   if ( list < 0 || list >= MEM_CONNECT_MAX_LISTS )
     return;

   mlist = &mem_connect[list];
   if (  type < 0 || type >= mlist->ntypes
       || mlist->free_funcs[type] == NULL )
     return;

//   if ( old_size < 0 || new_size < 0 )
//     error(E_NEG,"mem_bytes_list");

   mlist->info_sum[type].bytes += new_size - old_size;

}



/* mem_numvar_list

   Arguments:
   type - the number of type;
   num - # of variables allocated (> 0) or deallocated ( < 0)
   list - list of types
   */

#ifndef ANSI_C
void mem_numvar_list(type,num,list)
int type,list,num;
#else
void mem_numvar_list(int type, int num, int list)
#endif
{
   MEM_CONNECT *mlist;

   if ( list < 0 || list >= MEM_CONNECT_MAX_LISTS )
     return;

   mlist = &mem_connect[list];
   if (  type < 0 || type >= mlist->ntypes
       || mlist->free_funcs[type] == NULL )
     return;

   mlist->info_sum[type].numvar += num;


}

