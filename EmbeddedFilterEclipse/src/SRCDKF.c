/*
 * SRCDKF.c
 *
 *  Created on: Jan 18, 2013
 *      Author: mark
 */


#include "SRCDKF.h"
#include "defines_c28.h"

extern enum state_identifiers;

// ##### Matrix pointers ####### //
// These are global to avoid re- and de-allocating them every time
// If this were C++ these would be member variables

VEC * x; // State vector

unsigned dimX; // Dimension of the state vector
unsigned dimV; // Dimension of the process noise vector

MAT * Sx; // Square-root of state estimate covariance
MAT * Svx; // Square-root of state+process covariance matrix

MAT * W1, *W2; // Weights for sigma points

// Other parameters
float32 h; //Sigma point spread parameter

MAT * C_b2n;
VEC * acc_corr, *acc_rot;

void f_kinematic(VEC* x_in, VEC* v, VEC * u, VEC* x_out, float32* dt) {
	// x = [pos; vel; accbias]
	// v = [accnoise]
	// u = [accel, attitude]

	// Position update
	x_out->ve[XPOS] = x_in->ve[XPOS] + *dt * x_in->ve[XVEL];
	x_out->ve[YPOS] = x_in->ve[YPOS] + *dt * x_in->ve[YVEL];
	x_out->ve[ZPOS] = x_in->ve[ZPOS] + *dt * x_in->ve[ZVEL];

	// Velocity update
	//static MAT * C_b2n;
	C_b2n = m_resize(C_b2n, 3, 3);
	C_b2n->me[0][0] = 0.5 - u->ve[E2] * u->ve[E2] - u->ve[E3] * u->ve[E3];
	C_b2n->me[0][1] = u->ve[E1] * u->ve[E2] - u->ve[E0] * u->ve[E3];
	C_b2n->me[0][2] = u->ve[E1] * u->ve[E3] + u->ve[E0] * u->ve[E2];
	C_b2n->me[1][0] = u->ve[E1] * u->ve[E2] + u->ve[E0] * u->ve[E3];
	C_b2n->me[1][1] = 0.5 - u->ve[E1] * u->ve[E1] - u->ve[E3] * u->ve[E3];
	C_b2n->me[1][2] = u->ve[E2] * u->ve[E3] - u->ve[E0] * u->ve[E1];
	C_b2n->me[2][0] = u->ve[E1] * u->ve[E3] - u->ve[E0] * u->ve[E2];
	C_b2n->me[2][1] = u->ve[E2] * u->ve[E3] + u->ve[E0] * u->ve[E1];
	C_b2n->me[2][2] = 0.5 - u->ve[E1] * u->ve[E1] - u->ve[E2] * u->ve[E2];
	C_b2n = sm_mlt(2.0, C_b2n, C_b2n);

	//static VEC * acc_corr, *acc_rot;
	acc_corr = v_resize(acc_corr, 3);
	acc_rot = v_resize(acc_rot, 3);
	acc_corr->ve[0] = u->ve[AX] - x_in->ve[AXBIAS] - v->ve[0];
	acc_corr->ve[1] = u->ve[AY] - x_in->ve[AYBIAS] - v->ve[1];
	acc_corr->ve[2] = u->ve[AZ] - x_in->ve[AZBIAS] - v->ve[2];

	acc_rot = mv_mlt(C_b2n, acc_corr, acc_rot);
	acc_rot->ve[2] -= GRAVITY;

	x_out->ve[XVEL] = x_in->ve[XVEL] + *dt * acc_rot->ve[0];
	x_out->ve[YVEL] = x_in->ve[YVEL] + *dt * acc_rot->ve[1];
	x_out->ve[ZVEL] = x_in->ve[ZVEL] + *dt * acc_rot->ve[2];

	// Bias update
	x_out->ve[AXBIAS] = x_in->ve[AXBIAS]; // TODO: Add random walk
	x_out->ve[AYBIAS] = x_in->ve[AYBIAS]; // TODO: Add random walk
	x_out->ve[AZBIAS] = x_in->ve[AZBIAS]; // TODO: Add random walk
}

void h_posvel(VEC* x_in, VEC* n_in, VEC* y_out) {
	// Pos meas = pos est + pos noise
	// Vel meas = vel est + vel noise
	unsigned i;
	for (i = 0; i < 6; i++) {
		y_out->ve[i] = x_in->ve[i] + n_in->ve[i];
	}
}

void h_pos(VEC* x_in, VEC* n_in, VEC* y_out) {
	// Pos meas = pos est + pos noise
	unsigned i;
	for (i = 0; i < 3; i++) {
		y_out->ve[i] = x_in->ve[i] + n_in->ve[i];
	}
}

void h_pos_xy(VEC* x_in, VEC* n_in, VEC* y_out) {
	// Pos meas = pos est + pos noise (just x and y position)
	unsigned i;
	for (i = 0; i < 2; i++) {
		y_out->ve[i] = x_in->ve[i] + n_in->ve[i];
	}
}

void h_pos_z(VEC* x_in, VEC* n_in, VEC* y_out) {
	// Pos meas = pos est + pos noise (just z position)
	y_out->ve[0] = x_in->ve[ZPOS] + n_in->ve[ZPOS];
}

/*
 * Initializes matrices, must be called before update functions
 * x0: Initial state vector
 * Rx: Vector representing the diagonal of the initial state covariance matrix
 * Rv: Vector representing the diagonal of the process noise covariance matrix
 * RWvar: Variances of the Gaussian random walk noises to be added in the time update
 *        (use zero to indicate that no noise should be added)
 *        (adding noise to bias states tends to help convergence)
 * h_width: Sigma point spread parameter. Use sqrt(3) for Gaussian prior RV's
 */
void SRCDKF_initialize(VEC * x0, VEC * Rx, VEC * Rv, VEC * RWvar, double h_width){
	unsigned i;
	h = h_width;

	x = v_copy(x0,x);

	// Store dimensions of state and process noise vectors for convenience
	dimX = x0->dim;
	dimV = Rv->dim;

	// Allocate and initialize state covariance matrix
	Sx = m_get(dimX, dimX);
	for(i=0; i<dimX; i++){
		Sx->me[i][i] = sqrt(Rx->ve[i]);
	}

	// Allocate and initialize state+process covariance matrix
	Svx = m_get(dimX+dimV, dimX+dimV);
	// Set process covariance diagonal elements (these don't change between iterations)
	for(i=0; i<dimV; i++){
		Svx->me[dimX+i][dimX+i] = sqrt(Rv->ve[i]); // TODO: Pre-mult by h here
	}

	// Construct the sigma point weight matrices
	W1 = m_get(2,2);
	W1->me[0][0] = (h*h - dimX - dimV)/(h*h);	W1->me[0][1] = 1.0/(2.0*h*h);
	W1->me[1][0] = 1.0/(2.0*h);					W1->me[1][1] = sqrt(h*h-1.0)/(2.0*h*h);
	W2 = m_get(2,2);
	W2 = m_copy(W1, W2);
}


/*
 * Performs a time (process) update, propagating both the state and state covariance
 * u: Inputs needed for process update (i.e. sensor readings, etc)
 * h: Pointer to the observation function, which takes a state vector x and noise
 *    vector n, and input vector u. f should update the vector x.
 */
void SRCDKF_timeUpdate( VEC * u, float* dt, void(*F)(VEC* x_in, VEC* v, VEC * u, VEC* x_out, float* dt) ){
	unsigned i,j;

	/********* Build the sigma points **********/
	// Allocate the sigma point matrix (will only allocate memory the first time)
	static MAT * Xv;
	Xv = m_resize(Xv, dimX+dimV, 1+2*(dimX+dimV));

	// Build the augmented state vector
	static VEC * xv;
	xv = v_resize(xv, dimX+dimV);
	xv = v_move(x, 0, dimX,   xv, 0);

	// Build the state+process covariance matrix
	Svx = m_move(Sx,0,0,dimX,dimX,    Svx, 0, 0);

	/********* Propagate sigma points through process dynamics **********/
	static VEC * x_in, *v_in, *x_out;
	x_in = v_resize(x_in, dimX);
	v_in = v_resize(v_in, dimV); v_in = v_zero(v_in);
	x_out = v_resize(x_out, dimX);
	static MAT * QR;
	QR = m_resize(QR, 2*(dimX+dimV), dimX);
	QR = m_zero(QR);
	unsigned L = dimX+dimV;

	// 0 sigma point
	F(x, v_in, u, x_out, dt); // Propagate through dynamics
	for(i=0;i<dimX; i++){
		x->ve[i] = W1->me[0][0]*x_out->ve[i];
	}
	x_out = sv_mlt(-2.0, x_out, x_out);
	for(i = L; i < QR->m; i++){
		QR = set_row(QR,i,x_out); // Add -2*X0 to QR matrix
	}

	// 1 to L sigma points
	for(i = 0; i < L; i++){
		for(j=0; j<dimX; j++){
			x_in->ve[j] = xv->ve[j] + h*Svx->me[j][i]; // Make input state vector
		}
		for(j=0; j<dimV; j++){
			v_in->ve[j] = xv->ve[dimX+j] + h*Svx->me[dimX+j][i]; // Make input noise vector
		}
		F(x_in, v_in, u, x_out, dt); // Propagate through dynamics
		for(j=0;j<dimX; j++){
			x->ve[j] += W1->me[0][1]*x_out->ve[j];
			QR->me[i][j] = x_out->ve[j];
			QR->me[i+L][j] += x_out->ve[j];
		}
	}

	// L+1 to 2L sigma points
	for(i = L; i < 2*L; i++){
		for(j=0; j<dimX; j++){
			x_in->ve[j] = xv->ve[j] - h*Svx->me[j][i-L]; // Make input state vector
		}
		for(j=0; j<dimV; j++){
			v_in->ve[j] = xv->ve[dimX+j] - h*Svx->me[dimX+j][i-L]; // Make input noise vector
		}
		F(x_in, v_in, u, x_out, dt); // Propagate through dynamics
		for(j=0;j<dimX; j++){
			x->ve[j] += W1->me[0][1]*x_out->ve[j];
			QR->me[i-L][j] -= x_out->ve[j];
			QR->me[i][j] += x_out->ve[j];
		}
	}

	// Multiply by weights
	for(j = 0; j < dimX; j++){
		for(i=0; i<L; i++){
			QR->me[i][j] *= W1->me[1][0];
			QR->me[i+L][j] *= W1->me[1][1];
		}
	}

	// Do QR and update state covariance: Sx = R'
	static VEC * diag;
	diag = v_resize(diag, QR->m);
	QRfactor(QR,diag);
	for(i=0;i<dimX; i++){
		for(j=i; j<dimX; j++){
			Sx->me[j][i] = -QR->me[i][j];
		}
	}
}

/*
 * Performs a measurement update
 * y: Observed measurement
 * h: Pointer to the observation function, which takes a state vector x and noise
 *    vector n, and returns corresponding measurement vector y.
 * Sn:Vector representing the sqrt of the diagonal of the measurement noise covariance matrix
 */
void SRCDKF_measUpdate( VEC * y, void(*H)(VEC* x_in, VEC* n_in, VEC* y_out), VEC * Sn )
{
	unsigned i,j;
	unsigned dimY = Sn->dim;
	unsigned L = dimX+dimY;

	// Calculate weight
	W2->me[0][0] = (h*h-L)/(h*h);

	/********* Build the sigma points **********/
	// Allocate the sigma point matrix (will only allocate memory the first time)
	static MAT * Xn;
	Xn = m_resize(Xn, L, 1+2*L);

	// Build the augmented state vector
	static VEC * xn;
	xn = v_resize(xn, L);
	xn = v_move(x, 0, dimX,   xn, 0);

	// Build the state+process covariance matrix
	static MAT * Sxy;
	Sxy = m_resize(Sxy, L,L);
	Sxy = m_move(Sx,0,0,dimX,dimX,    Sxy, 0, 0);
	for(i=0;i<dimY;i++){
		Sxy->me[dimX+i][dimX+i] = Sn->ve[i];
	}

	/********* Propagate sigma points through observation function **********/
	static VEC * x_in, *n_in, *y_out, *y_hat;
	x_in = v_resize(x_in, dimX);
	n_in = v_resize(n_in, dimY); n_in = v_zero(n_in);
	y_out = v_resize(y_out, dimY);
	y_hat = v_resize(y_hat, dimY);

	static MAT * QR;
	QR = m_resize(QR, 2*L, dimY);
	QR = m_zero(QR);

	// 0 sigma point
	H(x, n_in, y_out); // Propagate through observation function
	for(i=0;i<dimY; i++){
		y_hat->ve[i] = W2->me[0][0]*y_out->ve[i];
	}
	y_out = sv_mlt(-2.0, y_out, y_out);
	for(i = L; i < QR->m; i++){
		QR = set_row(QR,i,y_out); // Add -2*Y0 to QR matrix
	}

	// 1 to L sigma points
	for(i = 0; i < L; i++){
		for(j=0; j<dimX; j++){
			x_in->ve[j] = xn->ve[j] + h*Sxy->me[j][i]; // Make input state vector
		}
		for(j=0; j<dimY; j++){
			n_in->ve[j] = xn->ve[dimX+j] + h*Sxy->me[dimX+j][i]; // Make input noise vector
		}
		H(x_in, n_in, y_out); // Propagate through observation function
		for(j=0;j<dimY; j++){
			y_hat->ve[j] += W2->me[0][1]*y_out->ve[j];
			QR->me[i][j] = y_out->ve[j];
			QR->me[i+L][j] += y_out->ve[j];
		}
	}

	// L+1 to 2L sigma points
	for(i = L; i < 2*L; i++){
		for(j=0; j<dimX; j++){
			x_in->ve[j] = xn->ve[j] - h*Sxy->me[j][i-L]; // Make input state vector
		}
		for(j=0; j<dimY; j++){
			n_in->ve[j] = xn->ve[dimX+j] - h*Sxy->me[dimX+j][i-L]; // Make input noise vector
		}
		H(x_in, n_in, y_out); // Propagate through observation function
		for(j=0;j<dimY; j++){
			y_hat->ve[j] += W2->me[0][1]*y_out->ve[j];
			QR->me[i-L][j] -= y_out->ve[j];
			QR->me[i][j] += y_out->ve[j];
		}
	}

	// Multiply by weights
	for(j = 0; j < dimY; j++){
		for(i=0; i<L; i++){
			QR->me[i][j] *= W2->me[1][0];
			QR->me[i+L][j] *= W2->me[1][1];
		}
	}

	// Make a copy of the QR matrix to be used later
	static MAT * QR_copy;
	if(QR_copy == MNULL || QR_copy->m > QR->m || QR_copy->n > QR->n ){ // other case is handled in the _m_copy function
		m_resize(QR_copy, QR->m, QR->n);
	}
	QR_copy = _m_copy(QR, QR_copy, 0, 0);

	// Do QR and update state covariance: Sy = R'
	static MAT * Sy;
	Sy = m_resize(Sy, dimY, dimY);
	static VEC * diag;
	diag = v_resize(diag, QR->m);
	QRfactor(QR,diag);
	for(i=0;i<dimY; i++){
		for(j=i; j<dimY; j++){
			Sy->me[j][i] = -QR->me[i][j];
		}
	}


	/********* Calculate gain and apply **********/
	static MAT * Syx1, *Syw1, *D;
	Syx1 = sub_mat(QR_copy, 0,0,dimX-1,QR_copy->n-1,Syx1);
	Syw1 = sub_mat(QR_copy, dimX,0,L-1,QR_copy->n-1,Syw1);
	D = sub_mat(QR_copy, L,0,QR_copy->m-1,QR_copy->n-1,D);

	static MAT * Pxy;
	Pxy = m_resize(Pxy, dimX, dimY);
	Pxy = m_mlt(Sx, Syx1, Pxy);

	static MAT * QR_K;
	static VEC * diag2;
	QR_K = m_resize(QR_K, dimY, dimY);
	QR_K = m_copy(Sy, QR_K);
	diag2 = v_resize(diag2, QR_K->m);
	QRfactor(QR_K, diag2);

	static MAT * K; K = m_resize(K,dimY,dimX);
	static VEC * b; b = v_resize(b,dimY);
	static VEC * tmp; tmp = v_resize(tmp,dimY);
	for(i=0; i<dimX; i++){
		b = get_row(Pxy,i,b);
		tmp = QRsolve(QR_K,diag2,b,tmp);
		K = set_col(K,i,tmp);
	}

	for(i=0;i<Sy->m;i++){
		for(j=0;j<Sy->n;j++){
			QR_K->me[j][i] = Sy->me[i][j]; // Put Sy' into QR_K
		}
	}
	QRfactor(QR_K, diag2);
	for(i=0; i<dimX; i++){
			b = get_col(K,i,b);
			tmp = QRsolve(QR_K,diag2,b,tmp);
			K = set_col(K,i,tmp);
	}

	// Make the innovation
	for(i=0;i<dimY;i++){
		y->ve[i] -= y_hat->ve[i];
	}

	// Apply gain
	x_in = vm_mlt(K,y,x_in);
	for(i=0;i<dimX;i++){
		x->ve[i] += x_in->ve[i];
	}

	/********* Update state covariance **********/
	static MAT * QR_Sx, *submat1, *submat2, *submat3;
	QR_Sx = m_resize(QR_Sx, 2*L, dimX);
	submat1 = sub_mat(QR_Sx, 0,0,dimX-1,dimX-1, submat1);
	submat2 = sub_mat(QR_Sx, dimX, 0, L-1, dimX-1, submat2);
	submat3 = sub_mat(QR_Sx, L, 0, 2*L-1, dimX-1, submat3);

	submat1 = m_mlt(Syx1,K,submat1);
	for(i=0;i<dimX;i++){
		for(j=0;j<dimX;j++){
			submat1->me[i][j] = Sx->me[j][i] - submat1->me[i][j];
		}
	}
	submat2 = m_mlt(Syw1,K,submat2);
	submat3 = m_mlt(D,K,submat3);

	static VEC * diag3;
	diag3 = v_resize(diag3, QR_Sx->m);
	QRfactor(QR_Sx,diag3);
	for(i=0;i<dimX; i++){
		for(j=i; j<dimX; j++){
			Sx->me[j][i] = -QR_Sx->me[i][j];
		}
	}
}
