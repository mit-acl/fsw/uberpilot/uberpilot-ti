/*
 * main.c
 *
 *  Created on: Oct 11, 2012
 *      Author: Buddy
 */

#include<stdio.h>
#include <math.h>
#include "meschach/matrix.h"
#include "meschach/matrix2.h"

#include "SRCDKF.h"

//#define USE_VELOCITY_MEASURMENTS

extern enum state_identifiers;

extern VEC * x; // State vector

int main() {

	/* Open test data */
	char *inname = "modifiedSensors1.txt";
	FILE *infile;
	char line_buffer[BUFSIZ]; /* BUFSIZ is defined if you include stdio.h */
	int line_number;

	infile = fopen(inname, "r");
	if (!infile) {
		printf("Couldn't open file %s for reading.\n", inname);
		return 0;
	}
	printf("Opened file %s for reading.\n", inname);

	// Open file for human readable format textfile
	FILE * writeFile;
	writeFile = fopen("filter_output.txt", "w");
	char outStr[512];

	line_number = 0;
	// get first line
	fgets(line_buffer, sizeof(line_buffer), infile);
	line_number++;
	float xpos, ypos, zpos, dx, dy, dz, ax, ay, az, qw, qx, qy, qz;
	sscanf(line_buffer, "%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f\n", &xpos,
			&ypos, &zpos, &dx, &dy, &dz, &ax, &ay, &az, &qw, &qx, &qy, &qz);

	/* Filter Initialization */
	VEC * x0 = v_get(9);
	x0->ve[0] = xpos; //-1.7882;	// x
	x0->ve[1] = ypos; //-3.387;		// y
	x0->ve[2] = zpos; //-0.0166;	// z
	x0->ve[3] = dx; //0.0001;		// dx
	x0->ve[4] = dy; //0.0;		// dy
	x0->ve[5] = dz; //-0.0002;	// dz
	x0->ve[6] = 0;			// ax_bias
	x0->ve[7] = 0;			// ay_bias
	x0->ve[8] = 0;			// az_bias
	VEC * Rx = v_get(9);
	Rx->ve[0] = 0.1;
	Rx->ve[1] = 0.1;
	Rx->ve[2] = 0.1;
	Rx->ve[3] = 0.1;
	Rx->ve[4] = 0.1;
	Rx->ve[5] = 0.1;
	Rx->ve[6] = 0.1;
	Rx->ve[7] = 0.1;
	Rx->ve[8] = 0.1;
	float rv = 1.0;
	float sn = 0.001;
	VEC * Rv = v_get(3);
	Rv->ve[0] = rv;
	Rv->ve[1] = rv;
	Rv->ve[2] = rv;
#ifdef USE_VELOCITY_MEASURMENTS
	VEC * Sn = v_get(6);
	Sn->ve[0] = sqrt(sn);
	Sn->ve[1] = sqrt(sn);
	Sn->ve[2] = sqrt(sn);
	Sn->ve[3] = sqrt(sn);
	Sn->ve[4] = sqrt(sn);
	Sn->ve[5] = sqrt(sn);
#else
	VEC * Sn_xy = v_get(2);
	VEC * Sn_z = v_get(1);
	Sn_xy->ve[0] = sqrt(sn);
	Sn_xy->ve[1] = sqrt(sn);
	Sn_z->ve[0] = sqrt(sn);
#endif
	VEC * RWvar;
	double h_width = sqrt(3);

	// initialize
	SRCDKF_initialize(x0, Rx, Rv, RWvar, h_width);

	// write to file
	sprintf(outStr, "%f,%f,%f,%f,%f,%f,%f,%f,%f\r\n", x->ve[XPOS], x->ve[YPOS],
			x->ve[ZPOS], x->ve[XVEL], x->ve[YVEL], x->ve[ZVEL], x->ve[AXBIAS],
			x->ve[AYBIAS], x->ve[AZBIAS]);
	fputs(outStr, writeFile);

	static VEC *u = NULL, *Y = NULL;

	line_number = 0;
	float timef = 0;
	float dt = 1.0 / 500.0;
	int time, time_prev;
	time = 0;
	time_prev = 0;

	u = v_resize(u, 7);
	u->ve[0] = -0.0766;	// ax
	u->ve[1] = 0.0766;	// ay
	u->ve[2] = 9.9633;	// az
	u->ve[3] = 0.9999;	// qw
	u->ve[4] = 0.0021;	// qx
	u->ve[5] = -0.0033;	// qy
	u->ve[6] = 0.0142;	// qz

	while (fgets(line_buffer, sizeof(line_buffer), infile)) {
		line_number++;
		time++;
		timef += dt;

		sscanf(line_buffer, "%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f\n", &xpos,
				&ypos, &zpos, &dx, &dy, &dz, &ax, &ay, &az, &qw, &qx, &qy, &qz);

//    if (line_number % 1 == 0)	// 100 Hz
//    {
//
//      // time update
//      u = v_resize(u, 6);
//      u->ve[0] = ax; //-0.0766;	// ax
//      u->ve[1] = ay; //0.0766;	// ay
//      u->ve[2] = az; //9.9633;	// az
//      u->ve[3] = qw; //0.9999;	// qw
//      u->ve[4] = qx; //0.0021;	// qx
//      u->ve[5] = qy; //-0.0033;	// qy
//      u->ve[6] = qz; //0.0142;	// qz
//      dt = (time - time_prev) / 500.0;
//      time_prev = time;
//      SRCDKF_timeUpdate(u, &dt, &f_kinematic);
//    }
//
//    if (timef < 8 || timef > 13)
//    {
//      if (line_number % 10 == 0) // 5 Hz
//      {
//        // measurement update
//#ifdef USE_VELOCITY_MEASURMENTS
//        Y = v_resize(Y,6);
//        Y->ve[0] = xpos; //-1.7882;	// x
//        Y->ve[1] = ypos;//-3.387;	// y
//        Y->ve[2] = zpos;//-0.0166;	// z
//        Y->ve[3] = dx;//-0.0001;	// dx
//        Y->ve[4] = dy;//0.0001;	// dy
//        Y->ve[5] = dz;//-0.0005;	// dz
//        SRCDKF_measUpdate(Y, &h_posvel, Sn);
//#else
//        Y = v_resize(Y, 3);
//        Y->ve[0] = xpos; //-1.7882;     // x
//        Y->ve[1] = ypos; //-3.387;      // y
//        Y->ve[2] = zpos; //-0.0166;     // z
//        SRCDKF_measUpdate(Y, &h_pos, Sn);
//#endif
//
//      }
//    }

		if (line_number % 1 == 0)
				{

			// time update
			u = v_resize(u, 6);
			u->ve[0] = ax; //-0.0766;	// ax
			u->ve[1] = ay; //0.0766;	// ay
			u->ve[2] = az; //9.9633;	// az
			u->ve[3] = qw; //0.9999;	// qw
			u->ve[4] = qx; //0.0021;	// qx
			u->ve[5] = qy; //-0.0033;	// qy
			u->ve[6] = qz; //0.0142;	// qz
			dt = (time - time_prev) / 500.0;
			time_prev = time;
			SRCDKF_timeUpdate(u, &dt, &f_kinematic);
		}

		if (timef < 15){// || timef > 8) {
			if (line_number % 10 == 0)
					{
				// measurement update
#ifdef USE_VELOCITY_MEASURMENTS
				Y = v_resize(Y, 6);
				Y->ve[0] = xpos; //-1.7882;	// x
				Y->ve[1] = ypos; //-3.387;	// y
				Y->ve[2] = zpos; //-0.0166;	// z
				Y->ve[3] = dx; //-0.0001;	// dx
				Y->ve[4] = dy; //0.0001;	// dy
				Y->ve[5] = dz; //-0.0005;	// dz
				SRCDKF_measUpdate(Y, &h_posvel, Sn);
#else
//				Y = v_resize(Y, 3);
//				Y->ve[0] = xpos; //-1.7882;     // x
//				Y->ve[1] = ypos;//-3.387;      // y
//				Y->ve[2] = zpos;//-0.0166;     // z
//				SRCDKF_measUpdate(Y, &h_pos, Sn);

				Y = v_resize(Y, 2);
				Y->ve[0] = xpos; //-1.7882;     // x
				Y->ve[1] = ypos;//-3.387;      // y
				SRCDKF_measUpdate(Y, &h_pos_xy, Sn_xy);
#endif

			}
		}

		if (x->ve[XPOS]>5.1)
		{
			__asm__(" NOP");
		}

		// use separate z position update at 10 Hz
		if (line_number % 50 == 0){
			Y = v_resize(Y, 1);
			Y->ve[0] = zpos;//-0.0166;     // z
			SRCDKF_measUpdate(Y, &h_pos_z, Sn_z);
		}

		// write to file
		sprintf(outStr, "%f,%f,%f,%f,%f,%f,%f,%f,%f\r\n", x->ve[XPOS],
				x->ve[YPOS], x->ve[ZPOS], x->ve[XVEL], x->ve[YVEL], x->ve[ZVEL],
				x->ve[AXBIAS], x->ve[AYBIAS], x->ve[AZBIAS]);
		fputs(outStr, writeFile);
	}

	printf("Filter done \n");

	return 0;
}

///*
// * main.c
// *
// *  Created on: Oct 11, 2012
// *      Author: Buddy
// */
//
//#include<stdio.h>
//#include <math.h>
//#include "meschach/matrix.h"
//#include "meschach/matrix2.h"
//
//#include "SRCDKF.h"
//
//#define XPOS 0
//#define YPOS 1
//#define ZPOS 2
//#define XVEL 3
//#define YVEL 4
//#define ZVEL 5
//#define AXBIAS 6
//#define AYBIAS 7
//#define AZBIAS 8
//#define AX 0
//#define AY 1
//#define AZ 2
//#define E0 3
//#define E1 4
//#define E2 5
//#define E3 6
//
//extern VEC * x; // State vector
//
//void f_kinematic(VEC* x_in, VEC* v, VEC * u, VEC* x_out, float* dt) {
//	// x = [pos; vel; accbias]
//	// v = [accnoise]
//	// u = [accel, attitude]
//
//	static double G = 9.81;
//	//static double dt = 20.0 / 500.0;
//
//	// Position update
//	x_out->ve[XPOS] = x_in->ve[XPOS] + *dt * x_in->ve[XVEL];
//	x_out->ve[YPOS] = x_in->ve[YPOS] + *dt * x_in->ve[YVEL];
//	x_out->ve[ZPOS] = x_in->ve[ZPOS] + *dt * x_in->ve[ZVEL];
//
//	// Velocity update
//	static MAT * C_b2n;
//	C_b2n = m_resize(C_b2n, 3, 3);
//	C_b2n->me[0][0] = 0.5 - u->ve[E2] * u->ve[E2] - u->ve[E3] * u->ve[E3];
//	C_b2n->me[0][1] = u->ve[E1] * u->ve[E2] - u->ve[E0] * u->ve[E3];
//	C_b2n->me[0][2] = u->ve[E1] * u->ve[E3] + u->ve[E0] * u->ve[E2];
//	C_b2n->me[1][0] = u->ve[E1] * u->ve[E2] + u->ve[E0] * u->ve[E3];
//	C_b2n->me[1][1] = 0.5 - u->ve[E1] * u->ve[E1] - u->ve[E3] * u->ve[E3];
//	C_b2n->me[1][2] = u->ve[E2] * u->ve[E3] - u->ve[E0] * u->ve[E1];
//	C_b2n->me[2][0] = u->ve[E1] * u->ve[E3] - u->ve[E0] * u->ve[E2];
//	C_b2n->me[2][1] = u->ve[E2] * u->ve[E3] + u->ve[E0] * u->ve[E1];
//	C_b2n->me[2][2] = 0.5 - u->ve[E1] * u->ve[E1] - u->ve[E2] * u->ve[E2];
//	C_b2n = sm_mlt(2.0, C_b2n, C_b2n);
//
//	static VEC * acc_corr, *acc_rot;
//	acc_corr = v_resize(acc_corr, 3);
//	acc_rot = v_resize(acc_rot, 3);
//	acc_corr->ve[0] = u->ve[AX] - x_in->ve[AXBIAS] - v->ve[0];
//	acc_corr->ve[1] = u->ve[AY] - x_in->ve[AYBIAS] - v->ve[1];
//	acc_corr->ve[2] = u->ve[AZ] - x_in->ve[AZBIAS] - v->ve[2];
//
//	acc_rot = mv_mlt(C_b2n, acc_corr, acc_rot);
//	acc_rot->ve[2] -= G;
//
//	x_out->ve[XVEL] = x_in->ve[XVEL] + *dt * acc_rot->ve[0];
//	x_out->ve[YVEL] = x_in->ve[YVEL] + *dt * acc_rot->ve[1];
//	x_out->ve[ZVEL] = x_in->ve[ZVEL] + *dt * acc_rot->ve[2];
//
//	// Bias update
//	x_out->ve[AXBIAS] = x_in->ve[AXBIAS]; // TODO: Add random walk
//	x_out->ve[AYBIAS] = x_in->ve[AYBIAS]; // TODO: Add random walk
//	x_out->ve[AZBIAS] = x_in->ve[AZBIAS]; // TODO: Add random walk
//}
//
//void h_posvel(VEC* x_in, VEC* n_in, VEC* y_out) {
//	// Pos meas = pos est + pos noise
//	// Vel meas = vel est + vel noise
//	unsigned i;
//	for (i = 0; i < 6; i++) {
//		y_out->ve[i] = x_in->ve[i] + n_in->ve[i];
//	}
//}
//
//// TODO:
//// 1. Write attitude prop/correction/bias estimation function
//// 2. Sample from Gaussian for RW and sensor noise (Box-Muller transform)
//// 2. Read in sensor/truth data line by line
//// 3. Do time update/measurement updates as appropriate
//
//int main() {
//
//	/* Open test data */
//	char *inname = "modifiedSensors1.txt";
//	FILE *infile;
//	char line_buffer[BUFSIZ]; /* BUFSIZ is defined if you include stdio.h */
//	int line_number;
//
//	infile = fopen(inname, "r");
//	if (!infile) {
//		printf("Couldn't open file %s for reading.\n", inname);
//		return 0;
//	}
//	printf("Opened file %s for reading.\n", inname);
//
//	// Open file for human readable format textfile
//	FILE * writeFile;
//	writeFile = fopen("filter_output.txt", "w");
//	char outStr[512];
//
//	line_number = 0;
//	// get first line
//	fgets(line_buffer, sizeof(line_buffer), infile);
//	line_number++;
//	float xpos,ypos,zpos,dx,dy,dz,ax,ay,az,qw,qx,qy,qz;
//	sscanf(line_buffer,"%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f\n",&xpos,&ypos,&zpos,&dx,&dy,&dz,&ax,&ay,&az,&qw,&qx,&qy,&qz);
//
//	/* Filter Initialization */
//	VEC * x0 = v_get(9);
//	x0->ve[0] = 0.0;	// x
//	x0->ve[1] = 0.0;		// y
//	x0->ve[2] = 0.0;	// z
//	x0->ve[3] = 0.0;		// dx
//	x0->ve[4] = 0.0;		// dy
//	x0->ve[5] = 0.0;	// dz
//	x0->ve[6] = 0;			// ax_bias
//	x0->ve[7] = 0;			// ay_bias
//	x0->ve[8] = 0;			// az_bias
//	VEC * Rx = v_get(9);
//	Rx->ve[0] = 0.1;
//	Rx->ve[1] = 0.1;
//	Rx->ve[2] = 0.1;
//	Rx->ve[3] = 0.1;
//	Rx->ve[4] = 0.1;
//	Rx->ve[5] = 0.1;
//	Rx->ve[6] = 0.1;
//	Rx->ve[7] = 0.1;
//	Rx->ve[8] = 0.1;
//	VEC * Rv = v_get(3);
//	Rv->ve[0] = 1.0;
//	Rv->ve[1] = 1.0;
//	Rv->ve[2] = 1.0;
//	VEC * Sn = v_get(6);
//	Sn->ve[0] = sqrt(0.001);
//	Sn->ve[1] = sqrt(0.001);
//	Sn->ve[2] = sqrt(0.001);
//	Sn->ve[3] = sqrt(0.001);
//	Sn->ve[4] = sqrt(0.001);
//	Sn->ve[5] = sqrt(0.001);
//	VEC * RWvar;
//	double h_width = sqrt(3);
//
//	// initialize
//	SRCDKF_initialize(x0, Rx, Rv, RWvar, h_width);
//
//	// write to file
//	sprintf(outStr, "%f,%f,%f,%f,%f,%f,%f,%f,%f\r\n", x->ve[XPOS],x->ve[YPOS],x->ve[ZPOS],
//			x->ve[XVEL],x->ve[YVEL],x->ve[ZVEL],x->ve[AXBIAS], x->ve[AYBIAS],x->ve[AZBIAS]);
//	fputs(outStr, writeFile);
//
//	static VEC *u=NULL, *Y=NULL;
//
//	line_number = 0;
//	float dt = 1.0/1000.0;
//	int time, time_prev;
//	time = 0; time_prev = 0;
//
//
//	u = v_resize(u,7);
//	u->ve[0] = -0.1;	// ax
//	u->ve[1] = 0.1;	// ay
//	u->ve[2] = 9.9;	// az
//	u->ve[3] = 0.9999;	// qw
//	u->ve[4] = 0.0021;	// qx
//	u->ve[5] = -0.0033;	// qy
//	u->ve[6] = 0.0142;	// qz
//
//	float timef = 0;
//	int i;
//	for (i=0; i<1000; i++){
//		line_number++;
//		time++;
//
//		sscanf(line_buffer,"%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f\n",&xpos,&ypos,&zpos,&dx,&dy,&dz,&ax,&ay,&az,&qw,&qx,&qy,&qz);
//
////		i f( line_number % 5 == 0)// 100 Hz
////		{
//
//			// time update
//			u = v_resize(u,6);
//			u->ve[0] = 0.5;	// ax
//			u->ve[1] = 0.0;	// ay
//			u->ve[2] = 9.81;	// az
//			u->ve[3] = 1;	// qw
//			u->ve[4] = 0;	// qx
//			u->ve[5] = 0;	// qy
//			u->ve[6] = 0;	// qz
//			dt = 1/500.0;
//			timef += dt;
//			time_prev = time;
//			SRCDKF_timeUpdate(u, &dt, &f_kinematic);
////		}
//
//		if( line_number % 100 == 0)// 5 Hz
//		{
//			// measurement update
//			Y = v_resize(Y,6);
//			Y->ve[0] = -2.0;	// x
//			Y->ve[1] = -4.0;	// y
//			Y->ve[2] = -1.0;	// z
//			Y->ve[3] = 0.0;	// dx
//			Y->ve[4] = 0.0;	// dy
//			Y->ve[5] = 0.0;	// dz
////			SRCDKF_measUpdate(Y, &h_posvel, Sn);
//		}
//
//		// write to file
//		sprintf(outStr, "%f,%f,%f,%f,%f,%f,%f,%f,%f\r\n", x->ve[XPOS],x->ve[YPOS],x->ve[ZPOS],
//				x->ve[XVEL],x->ve[YVEL],x->ve[ZVEL],x->ve[AXBIAS], x->ve[AYBIAS],x->ve[AZBIAS]);
//		fputs(outStr, writeFile);
//	}
//
//
//	printf("Filter done \n");
//
//	return 0;
//}

