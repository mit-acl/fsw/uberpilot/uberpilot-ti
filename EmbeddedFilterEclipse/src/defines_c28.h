/*
 * defines_c28.h
 *
 *  Created on: Jan 18, 2013
 *      Author: mark
 */

#ifndef DEFINES_C28_H_
#define DEFINES_C28_H_

typedef float float32;

#define PI	3.14159265359
#define GRAVITY 9.81

enum state_identifiers {
	XPOS,
	YPOS,
	ZPOS,
	XVEL,
	YVEL,
	ZVEL,
	AXBIAS,
	AYBIAS,
	AZBIAS,
	AX = 0,
	AY,
	AZ,
	E0,
	E1,
	E2,
	E3
};


#endif /* DEFINES_C28_H_ */
