/*
 * SRCDKF.h
 *
 *  Created on: Oct 12, 2012
 *      Author: B. Michini
 */

#ifndef SRCDKF_H_
#define SRCDKF_H_

#include "meschach/matrix.h"
#include "meschach/matrix2.h"

#include "defines_c28.h"

void f_kinematic(VEC* x_in, VEC* v, VEC * u, VEC* x_out, float32* dt);
void h_posvel(VEC* x_in, VEC* n_in, VEC* y_out);
void h_pos(VEC* x_in, VEC* n_in, VEC* y_out);
void h_pos_xy(VEC* x_in, VEC* n_in, VEC* y_out);
void h_pos_z(VEC* x_in, VEC* n_in, VEC* y_out);
void SRCDKF_initialize(VEC * x0, VEC * Rx, VEC * Rv, VEC * RWvar, double h_width);
void SRCDKF_timeUpdate( VEC * u, float* dt, void(*F)(VEC* x_in, VEC* v, VEC * u, VEC* x_out, float* dt) );
void SRCDKF_measUpdate( VEC * y, void(*H)(VEC* x_in, VEC* n_in, VEC* y_out), VEC * Sn );

#endif /* SRCDKF_H_ */
