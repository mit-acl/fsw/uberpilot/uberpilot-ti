% TI_uberpilot_voltage_scaling.m

clc; clear all;

mV = 1000*[4.084 5.012 5.949 7.01 8.12 9.09 10.05 11.51 12.03 12.44 12.99];
adc = [1182 1453 1725 2034 2357 2636 2914 3337 3486 3603 3765];

coeffs = polyfit(adc,mV,1);

figure(1); clf;
plot(adc,mV,adc,adc*coeffs(1)+coeffs(2));
legend('data','fit');